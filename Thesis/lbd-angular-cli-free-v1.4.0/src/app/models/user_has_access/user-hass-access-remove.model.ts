import { Resource } from "../resource.model";

export class UserHassAccessRemove extends Resource {
    entryId: number;
    userId: number;
}
