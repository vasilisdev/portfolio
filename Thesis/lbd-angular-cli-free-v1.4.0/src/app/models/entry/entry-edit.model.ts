import { Resource } from "../resource.model";
import { Entry } from "./entry.model";
import { UserHassAccess } from "app/models/user_has_access/user-hass-access.model";

export class EntryEdit extends Resource {
    entry: Entry;
    list: UserHassAccess[];
}
