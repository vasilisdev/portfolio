import { Resource } from "../resource.model";
import { Schema } from "./schema.model";



export class SchemaObject extends Resource {
    objectName: string;
    schema: Schema;
}
