import { Resource } from "app/models/resource.model";
import { ColumnName } from "./column-name";
import { Rows } from "app/models/schema/select/rows.model";

export class SelectAll extends Resource{
    columnsName :ColumnName;
    rows: Rows;
}

