import { Resource } from "../../resource.model";

export class Element extends Resource{
    isNull : boolean;
    value : string;
}
