import { Resource } from "../../resource.model";
import {Element} from "app/models/schema/select-foreign-key/element.model"


export class SelectForeignKey extends Resource{
    constrainName:string;
    elements:Element[]; 
}
