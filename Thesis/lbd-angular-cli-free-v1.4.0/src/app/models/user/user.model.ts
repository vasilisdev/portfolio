import { Resource } from "../resource.model";

export class User extends Resource{
    username: string;
    email: string;
    name: string;
    surname: string;
    phoneNumber: string;
    password?: string;
    active : boolean;
}
