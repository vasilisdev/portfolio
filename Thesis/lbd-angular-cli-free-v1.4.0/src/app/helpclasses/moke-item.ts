import { Item } from './item';

export const ITEMS: Item[] = [
    {
        name: 'Private',
        value: 'private'
    },
    {
        name: 'Public',
        value: 'public'
    },
    {
        name: 'Public View',
        value: 'public view'
    },

];