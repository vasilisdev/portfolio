import { Component, OnInit } from '@angular/core';
import { User } from '../models/user/user.model';
import { UserService } from '../service/user.service';
import { AdminService } from '../service/admin.service';

declare interface TableData {
    headerRow: string[];
    users: User[];
}

@Component({
    selector: 'app-tables',
    templateUrl: './tables.component.html',
    styleUrls: ['./tables.component.css']
})
export class TablesComponent implements OnInit {
    public tableData1: TableData;

    constructor(private userService: UserService, private adminService: AdminService) {

        this.tableData1 = {
            headerRow: ['ID', 'USERNAME', 'NAME', 'SURNAME', 'EMAIL', 'PHONE', 'STATUS'],
            users: []
        };

    }

    ngOnInit() {
        this.userService.list().subscribe(
            (data: User[]) => {
                data.forEach((o) => this.tableData1.users.push(o));
            }
        );
    }


    onActivate(id: number) {
        const user: User = this.tableData1.users.filter((item: User) => item.id === id)[0];
        user.active = true;
        let index = this.tableData1.users.indexOf(user);
        console.log(index);

        this.adminService.update(user).subscribe((data : User) => {
            const user: User = this.tableData1.users.filter((item: User) => item.id === id)[0];
        });

    }



}
