import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from '../service/authentication.service';
import { Router } from '@angular/router';
import { Authentication } from '../models/authentication.model';
import { UserWithToken } from '../models/user/user-with-token.model';
import { User } from '../models/user/user.model';
import { ITEMS } from 'app/helpclasses/moke-login-register';
import { Item } from 'app/helpclasses/item';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  authentication: Authentication;

  constructor(private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService
  ) {
    this.authentication = new Authentication();
  }

  ngOnInit() {

  }

  onSubmit(form: NgForm) {

    console.log(form);

    // valid form
    if (form.valid) {
      this.authentication.password = form.value.password;
      this.authentication.username = form.value.username;

      form.reset();

      //sub in service
      this.authenticationService.login(this.authentication).subscribe(
        (user: UserWithToken) => {
          if (user && user.token) {
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.router.navigate(['./home']);
          }
        }
      );
    }
  }

  onSubmitSignUp(form: NgForm) {
    // console.log(form);
    if (form.valid) {
   
      const user : User = this.userService.formRegister(form);
      form.reset();

      this.userService.create(user).subscribe(
        (response) => { console.log(response) }
      );
    }
  }
}
