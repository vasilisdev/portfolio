import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { TablesComponent } from './tables/tables.component';
import { IconsComponent } from './icons/icons.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { AppComponent } from './app.component';
import { EntryComponent } from './entry/entry.component';
import { WelcomeComponent } from 'app/welcome/welcome.component';
import { DirectoryComponent } from './entry/directory/directory.component';
import { EditComponent } from './entry/edit/edit.component';
import { DatabaseComponent } from './database/database.component';
import { ProcessesComponent } from './processes/processes.component';
import { FileComponent } from './entry/file/file.component';
import { TableComponent } from './database/table/table.component';
import { SelectAllComponent } from './database/select-all/select-all.component';
import { SelectStructureComponent } from 'app/database/select-structure/select-structure.component';
import { InsertComponent } from './database/insert/insert.component';
import { UpdateComponent } from './database/update/update.component';
import { CreateTableComponent } from './database/create-table/create-table.component';
import { ProcessLogComponent } from 'app/processes/process-log/process-log.component';
import { ProcessLogContentComponent } from 'app/processes/process-log-content/process-log-content.component';
import { AddColumnComponent } from 'app/database/add-column/add-column.component';
import { AlertColumnComponent } from './database/alert-column/alert-column.component';
import { AddKeyComponent } from './database/add-key/add-key.component';

const routes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'user', component: UserComponent },
  { path: 'home', component: HomeComponent },
  // { path: 'dashboard', component: HomeComponent },
  // { path: 'welcome', component: WelcomeComponent },
  { path: 'entry', component: EntryComponent },
  { path: 'entry/:id', component: DirectoryComponent },
  { path: 'entry/:id/edit', component: EditComponent },
  { path: 'entry/file/:id', component: FileComponent },
  { path: 'database', component: DatabaseComponent },
  { path: 'database/:id', component: TableComponent },
  { path: 'database/:schemaName/create', component: CreateTableComponent },
  { path: 'database/:schemaName/table/:tableName', component: InsertComponent },
  { path: 'database/:schemaName/table/:tableName/update', component: UpdateComponent },
  { path: 'database/:schemaName/table/:tableName/add', component: AddColumnComponent },
  { path: 'database/:schemaName/table/:tableName/key', component: AddKeyComponent },
  { path: 'database/:schemaName/table/:tableName/alert', component: AlertColumnComponent },
  { path: 'database/:schemaName/:tableName', component: SelectAllComponent },
  { path: 'database/:schemaName/structure/:tableName', component: SelectStructureComponent },

  { path: 'processes', component: ProcessesComponent },
  { path: 'processes/log', component: ProcessLogContentComponent },
  { path: 'processes/:id/:name', component: ProcessLogComponent },

  // { path: 'table', component: TablesComponent },
  // { path: 'icons', component: IconsComponent },
  // { path: 'notifications', component: NotificationsComponent },
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
