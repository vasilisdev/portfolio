import { Serializer } from "../../serializer";
import { Resource } from "app/models/resource.model";
import { Loges } from "app/models/process/log/loges";
import { Log } from "../../../models/process/log/log";
import { LogSerializer } from "./log-serializer";

export class LogesSerializer implements Serializer {
    public fromJson(json: any): Resource {
        const loges = new Loges();
        const logSerializer = new LogSerializer();
        const list: Log[] = [];

        for(let o in  json){ 
            let log = <Log> logSerializer.fromJson(json[o]);
            list.push(log);
        }

        loges.loges = list;

        return loges;
    }
}
