import { Serializer } from "../serializer";
import { Resource } from "app/models/resource.model";
import { Processes } from "app/models/process/processes.model";
import { Process } from "app/models/process/process.model";
import { ProcessSerializer } from "app/serializer/process/process-serializer";

export class ProcessesSerializer implements Serializer {
    public fromJson(json: any): Resource {
        const processes = new Processes();
        const processSerializer = new ProcessSerializer();
        const list: Process[] = [];

        console.log(json)

        for (let o in json) {
            console.log(json[o].id)
            let p = <Process>processSerializer.fromJson(json[o]);
            list.push(p);
        }

        processes.processes = list;

        return processes;
    }
}
