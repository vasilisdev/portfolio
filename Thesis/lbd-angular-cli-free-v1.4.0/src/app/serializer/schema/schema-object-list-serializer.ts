import { Serializer } from "../serializer";
import { Resource } from "app/models/resource.model";
import { SchemaObject } from "../../models/schema/schema-object.model";
import { SchemaObjectList } from "app/models/schema/schema-object-list.model";
import { SchemaObjectSerializer } from "app/serializer/schema/schema-object-serializer";

export class SchemaObjectListSerializer implements Serializer {
    public fromJson(json: any): Resource {
        const schemaObjectListObject = new SchemaObjectList();
        const schemaObjectSerializer = new SchemaObjectSerializer();
        const schemaObjectList: SchemaObject [] =[];
  

        for (let o in json) {
            const schemaObject = <SchemaObject>schemaObjectSerializer.fromJson(json[o]);
            schemaObjectList.push(schemaObject);
        }
        schemaObjectListObject.list = schemaObjectList;

        return schemaObjectListObject;
    }
}
