import { Serializer } from "../../serializer";
import { Resource } from "app/models/resource.model";
import{Element} from "app/models/schema/select/element.model"

export class ElementSerializer extends Serializer {
    public fromJson(json: any): Resource {
        const element :Element = new Element();
        element.isNull = json.isNull;
        element.value = json.value;
        return element;
    }
}
