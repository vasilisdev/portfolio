import { Serializer } from "../../serializer";
import { Resource } from "app/models/resource.model";
import { SelectAll } from "app/models/schema/select/select-all.model";
import { ColumnName } from "../../../models/schema/select/column-name";
import { Rows } from "../../../models/schema/select/rows.model";
import { Elements } from "../../../models/schema/select/elements.model";
import { Element } from "app/models/schema/select/element.model"
import { ElementsSerializer } from "app/serializer/schema/schemaSelectAll/elements-serializer";


export class SelectAllSerializer implements Serializer {
    public fromJson(json: any): Resource {
        const columnsName: string[] = [];
        const columName = new ColumnName();
        const selectAll = new SelectAll();
        const rows = new Rows();
        const elementsSerializer = new ElementsSerializer();
        const list: Elements[] = [];

        console.log(json);

        for (let o in json.rows) {
            let e = <Elements>elementsSerializer.fromJson(json.rows[o]);
            list.push(e);
        }


        rows.row = list;
        selectAll.rows = rows;

        for (let o in json.columnsName) {
            let columName = <string>json.columnsName[o];
            columnsName.push(columName);
        }
        columName.columnsName = columnsName;
        selectAll.columnsName = columName;


        console.log(rows);

        return selectAll;
    }
}

