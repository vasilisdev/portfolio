import { Serializer } from "../../serializer";
import { Resource } from "app/models/resource.model";
import { SelectForeignKey } from "app/models/schema/select-foreign-key/select-foreign-key.model";
import { Element } from "app/models/schema/select-foreign-key/element.model"
import { ElementSerializer } from "app/serializer/schema/tableForeignKey/element-serializer";

export class ForeignKeySerializer implements Serializer {
    public fromJson(json: any): Resource {
        const selectForeignKey = new SelectForeignKey();
        const elements: Element[] = [];
        const elementSerializer = new ElementSerializer();

        for (let o in json.elements) {
            let element = <Element>elementSerializer.fromJson(json.elements[o]);
            elements.push(element);
        }

        selectForeignKey.constrainName = <string> json.constrainName;
        selectForeignKey.elements = elements;

        return selectForeignKey;
    }
}
