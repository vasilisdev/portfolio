import { Serializer } from "../../serializer";
import { Resource } from "app/models/resource.model";
import {Element} from "app/models/schema/select-foreign-key/element.model"

export class ElementSerializer implements Serializer {
    public fromJson(json: any): Resource {
        const element = new Element();
        element.columnName = json.columnName;
        element.referencedColumn = json.referencedColumn;
        element.refererencedTable = json.refererencedTable;
        return element;
    }
}
