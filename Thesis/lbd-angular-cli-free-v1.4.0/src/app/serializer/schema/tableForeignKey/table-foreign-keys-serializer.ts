import { Serializer } from "../../serializer";
import { Resource } from "app/models/resource.model";
import { SelectTableForeignKeys } from "app/models/schema/select-foreign-key/select-table-foreign-keys.model";
import { ForeignKeySerializer } from "app/serializer/schema/tableForeignKey/foreign-key-serializer";
import { SelectForeignKey } from "app/models/schema/select-foreign-key/select-foreign-key.model";

export class TableForeignKeysSerializer implements Serializer {
    public fromJson(json: any): Resource {
       const  selectTableForeignKeys = new SelectTableForeignKeys();
       const foreignKeySerializer = new ForeignKeySerializer();
       const  foreignKeysList: SelectForeignKey[]= [];

       for(let o in json.foreignKeysList){
           let foreignKeys = <SelectForeignKey> foreignKeySerializer.fromJson(json.foreignKeysList[o]);
           foreignKeysList.push(foreignKeys);
       }

       selectTableForeignKeys.foreignKeysList = foreignKeysList;
       return selectTableForeignKeys;
    }
}
