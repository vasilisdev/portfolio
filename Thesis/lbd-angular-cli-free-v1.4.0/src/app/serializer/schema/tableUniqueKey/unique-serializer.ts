import { Serializer } from "../../serializer";
import { Resource } from "app/models/resource.model";
import { SelectUniqueKey } from "app/models/schema/select-unique-key/select-unique-key.model";

export class UniqueSerializer implements Serializer {
    public fromJson(json: any): Resource {
        const selectUniqueKey = new SelectUniqueKey();
        const list: string[] = [];

        for (let o in json.columnsName) {
            let columName = <string>json.columnsName[o];
            list.push(columName);
        }

        selectUniqueKey.uniqueKeyName = json.uniqueKeyName;
        selectUniqueKey.columnsName = list;
        return selectUniqueKey;
    }
}
