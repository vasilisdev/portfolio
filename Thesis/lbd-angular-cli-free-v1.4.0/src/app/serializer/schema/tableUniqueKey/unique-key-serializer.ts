import { Serializer } from "../../serializer";
import { Resource } from "app/models/resource.model";
import { SelectTableUniqueKeys } from "app/models/schema/select-unique-key/select-table-unique-keys.model";
import { SelectUniqueKey } from "app/models/schema/select-unique-key/select-unique-key.model";
import { UniqueSerializer } from "app/serializer/schema/tableUniqueKey/unique-serializer";

export class UniqueKeySerializer implements Serializer {
    public fromJson(json: any): Resource {
        const selectTableUniqueKeys = new SelectTableUniqueKeys();
        const uniqueSerializer = new UniqueSerializer();
        const list: SelectUniqueKey[] = []

        for (let o in json.uniqueKey) {
            let e: SelectUniqueKey = <SelectUniqueKey> uniqueSerializer.fromJson(json.uniqueKey[o]);
            list.push(e);
        }
        selectTableUniqueKeys.uniqueKey = list;
        return selectTableUniqueKeys;
    }
}
