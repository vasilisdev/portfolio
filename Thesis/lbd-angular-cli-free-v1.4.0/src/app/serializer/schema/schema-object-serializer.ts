import { Serializer } from "../serializer";
import { SchemaObject } from "../../models/schema/schema-object.model";
import { Resource } from "app/models/resource.model";
import { Schema } from "../../models/schema/schema.model";
import { SchemaSerializer } from "./schema-serializer";

export class SchemaObjectSerializer implements Serializer {
    public fromJson(json: any): Resource {
        const scheamObject = new SchemaObject();
        const schemaSerializer: SchemaSerializer = new SchemaSerializer();
        scheamObject.id = json.id;
        scheamObject.objectName = json.objectName;
        scheamObject.schema = <Schema>schemaSerializer.fromJson(json.schemaModel);
        return scheamObject;
    }
}
