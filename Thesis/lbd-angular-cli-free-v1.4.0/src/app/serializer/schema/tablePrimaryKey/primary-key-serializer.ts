import { Serializer } from "../../serializer";
import { Resource } from "app/models/resource.model";
import { SelectPrimaryKeyList } from "app/models/schema/select-primary-key/select-primary-key-list.model";
import { PrimaryKeyColumns } from "app/models/schema/select-primary-key/primary-key-columns.model";

export class PrimaryKeySerializer implements Serializer {
    public fromJson(json: any): Resource {

        console.log(json)
        const selectPrimaryKeyList  = new SelectPrimaryKeyList();
        const primaryKeyColumns =  new PrimaryKeyColumns();
        const list:string[] =[];

        for(let o in json.listOfColumnName){
          let  element = <string> json.listOfColumnName[o];
          list.push(element);
        }
        
        primaryKeyColumns.listOfColumnName = list;
        selectPrimaryKeyList.primaryKeyColumns = primaryKeyColumns;

        return selectPrimaryKeyList;
    }
}
