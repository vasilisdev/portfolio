
import { User } from "app/models/user/user.model";
import { UserSerializer } from "app/serializer/user/user-serializer";
import { Serializer } from "app/serializer/serializer";
import { Resource } from "app/models/resource.model";
import { Entry } from "app/models/entry/entry.model";

export class EntrySerializer extends Serializer {
    public fromJson(json: any): Resource {
        const entry: Entry = new Entry();
        const userSerializer: UserSerializer = new UserSerializer();

        console.log(json);

        entry.id = json.id;
        entry.fullpath = json.fullpath;
        entry.isDirectory = json.isDirectory;
        entry.isExecutable = json.isExecutable;
        entry.otherCanExecute = json.otherCanExecute;
        entry.otherCanRead = json.otherCanRead;
        entry.otherCanWrite = json.otherCanWrite;
        entry.ownerId = json.ownerId;
        entry.paretnId = json.paretnId;

        entry.ownerId = <User>userSerializer.fromJson(json.ownerId);
        return entry;
    }
}
