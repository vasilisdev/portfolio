import { Resource } from "../models/resource.model";

export abstract class Serializer {
   public abstract fromJson(json: any): Resource;
}

