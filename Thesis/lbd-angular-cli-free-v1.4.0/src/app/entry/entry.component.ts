import { Component, OnInit } from '@angular/core';
import { Entry } from '../models/entry/entry.model';
import { EntryService } from '../service/entry.service';
import { NgForm } from '@angular/forms';
import { User } from '../models/user/user.model';
import { AuthenticationService } from '../service/authentication.service';
import { Router } from '@angular/router';
import { ITEMS } from 'app/helpclasses/moke-item';
import { Item } from 'app/helpclasses/item';
import fileSaver = require("file-saver");

declare interface TableData {
  headerRow: string[];
  entries: Entry[];
}



@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.css']
})
export class EntryComponent implements OnInit {

  public tableData1: TableData;
  currentUserId: number;

  //----------------------------------------------------------------------------------------
  radioSel: any;
  radioSelected: string;
  itemsList: Item[] = ITEMS;
  //---------------------------------------------------------------------------------------

  constructor(private entryService: EntryService,
    private authenticationService: AuthenticationService,
    private router: Router
  ) {

    this.itemsList = ITEMS;
    this.radioSelected = "private";
    this.getSelecteditem();

    this.tableData1 = {
      headerRow: ['Index', 'TYPE', 'NAME', 'OWNER', 'OPERATION'],
      entries: []
    };
  }

  ngOnInit() {
    this.currentUserId = this.authenticationService.getLoggedUserId();
    this.entryService.list().subscribe(
      (data: Entry[]) => {
        data.forEach((o) => this.tableData1.entries.push(o));
        console.log(data);
      }
    )
  }


  onDownload(id: number) {
    this.entryService.downloadFile(id)
      .subscribe(data => this.entryService.getZipFile(data)),
      error => console.log("Error downloading the file."),
      () => console.log('Completed file download.');
  }


   

  //To do Valid form
  onSubmit(form: NgForm) {
    // console.log(form);
    const newEntry = this.entryService.formCreateBucket(form);
    this.entryService.create(newEntry).subscribe(
      (data: Entry) => {
        // console.log(data);
        this.tableData1.entries.push(data);
      }
    );
  }

  onDelete(id: number) {
    console.log(id);
    this.entryService.delete(id).subscribe(
      data => {
        const index = this.tableData1.entries.findIndex(entry => entry.id === id);
        this.tableData1.entries.splice(index, 1);
      }
    );
  }


  //--------------------Select Item-------------------------------------
  getSelecteditem() {
    this.radioSel = ITEMS.find(Item => Item.value === this.radioSelected);

  }

  onItemChange(item) {
    this.getSelecteditem();
  }
  //--------------------------------------------------------------------

}
