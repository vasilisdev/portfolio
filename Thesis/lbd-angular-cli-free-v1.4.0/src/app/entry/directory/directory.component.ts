import { Component, OnInit, OnDestroy } from '@angular/core';
import { Entry } from 'app/models/entry/entry.model';
import { EntryService } from 'app/service/entry.service';
import { AuthenticationService } from '../../service/authentication.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Item } from 'app/helpclasses/item';
import { ITEMS } from 'app/helpclasses/moke-item';
import { NgForm } from '@angular/forms';
import { Route } from '@angular/compiler/src/core';
import { EntryCopyCut } from 'app/models/entry/entry-copy-cut.model';
import { encodeUriQuery } from '@angular/router/src/url_tree';


declare interface TableData {
  headerRow: string[];
  entries: Entry[];
}

@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.css']
})
export class DirectoryComponent implements OnInit {


  tableData1: TableData;
  currentUserId: number;
  parentEntryId: number;
  parent: Entry;
  selectedFile: File = null;

  canCopyCanCut: boolean = false;
  entryCopyCut: Entry;
  //----------------------------------------------------------
  radioSel: any;
  radioSelected: string;
  itemsList: Item[] = ITEMS;
  checkBox: boolean = false;
  //---------------------------------------------------------

  constructor(
    private route: Router,
    private router: ActivatedRoute,
    private entryService: EntryService,
    private authenticationService: AuthenticationService
  ) {
    this.itemsList = ITEMS;
    this.radioSelected = "private";
    this.getSelecteditem();
  }

  ngOnInit() {
    this.currentUserId = this.authenticationService.getLoggedUserId();
    this.tableData1 = {
      headerRow: ['Index', 'TYPE', 'EXECUTABLE', 'NAME', 'OWNER', 'OPERATION'],
      entries: []
    };



    this.router.params.subscribe(
      (params: Params) => {
        this.parentEntryId = +params['id'];

        //Ferni to entry me ton patera tou
        this.entryService.read(this.parentEntryId).subscribe(
          (e: Entry) => {
            // console.log(e);
            this.parent = e;
            // console.log('parent : ' + this.parent);

            let entryFromStorage = JSON.parse(localStorage.getItem('copy_cut')) as Entry;
            // console.log(entryFromStorage);
            if (entryFromStorage !== null) {
              this.canCopyCanCut = true;
              this.entryCopyCut = entryFromStorage;
            }

            if (this.parent.paretnId !== undefined) {
              this.entryService.listWithId(this.parentEntryId).subscribe(
                (data: Entry[]) => {
                  this.tableData1.entries = data;
                }
              );
            } else {
              this.route.navigate(['./entry']);
            }
          }
        );
      }
    );
  }




  onDownload(id: number) {
    this.entryService.downloadFile(id)
      .subscribe(data => this.entryService.getZipFile(data)),
      error => console.log("Error downloading the file."),
      () => console.log('Completed file download.');
  }

  onUpload(form: NgForm) {
    const e = new Entry();
    e.isDirectory = false;
    e.isExecutable = false;
    e.otherCanExecute = false;
    e.otherCanRead = false;
    e.otherCanWrite = false;
    e.fullpath = this.selectedFile.name;
    e.paretnId = this.parentEntryId;

    this.entryService.uploadFile(e, this.selectedFile).subscribe(

      (data: Entry) => {
        console.log(data);
        this.tableData1.entries.push(data);
      }
    )
  }

  onFileSelect(event) {
    this.selectedFile = event.target.files[0];
  }

  onCut(id: number, index: number) {
    this.canCopyCanCut = !this.canCopyCanCut;
    const entry: Entry = this.tableData1.entries.find(x => x.id === id);
    this.entryCopyCut = entry;
    localStorage.setItem('copy_cut', JSON.stringify(entry));
    localStorage.setItem('copy_or_cut', JSON.stringify(true));
  }

  onCopy(id: number, index: number) {
    this.canCopyCanCut = !this.canCopyCanCut;
    const entry: Entry = this.tableData1.entries.find(x => x.id === id);
    this.entryCopyCut = entry;
    localStorage.setItem('copy_cut', JSON.stringify(entry));
    localStorage.setItem('copy_or_cut', JSON.stringify(false));
  }

  onPaste(id: number) {
    const entryCopyCut = new EntryCopyCut();
    entryCopyCut.destinationId = id;
    const entry: Entry = JSON.parse(localStorage.getItem('copy_cut'));
    entryCopyCut.sourceId = entry.id;
    //to do send to the server

    let flag: boolean = JSON.parse(localStorage.getItem('copy_or_cut'));
    console.log(flag);

    if (flag) {
      this.entryService.moveEntry(entryCopyCut).subscribe(
        (data: Entry) => {
          this.tableData1.entries.push(data);
          localStorage.removeItem('copy_cut');
          localStorage.removeItem('copy_or_cut');
          this.canCopyCanCut = false;
        }
      );
    } else {
      this.entryService.copyEntry(entryCopyCut).subscribe(
        (data: Entry) => {
          this.tableData1.entries.push(data);
          localStorage.removeItem('copy_cut');
          localStorage.removeItem('copy_or_cut');
          this.canCopyCanCut = false;
        }
      );

    }
  }


  //--------------------Select Item-------------------------------------
  getSelecteditem() {
    this.radioSel = ITEMS.find(Item => Item.value === this.radioSelected);

  }

  onItemChange(item) {
    this.getSelecteditem();
  }

  onChangeCheckBox() {
    this.checkBox = !this.checkBox;
  }

  //--------------------------------------------------------------------

}

        // let entryFromStorage = JSON.parse(localStorage.getItem('copy_cat')) as Entry;
        // console.log(entryFromStorage);
        // if (entryFromStorage !== null) {
        //   this.canCopyCanCut = true;
        // }

        // console.log(this.canCopyCanCut);