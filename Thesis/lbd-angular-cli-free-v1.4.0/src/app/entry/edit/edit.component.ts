import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'app/service/authentication.service';
import { EntryService } from 'app/service/entry.service';
import { ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { EntryRename } from '../../models/entry/entry-rename.model';
import { Entry } from '../../models/entry/entry.model';
import { Item } from 'app/helpclasses/item';
import { ITEMS } from 'app/helpclasses/moke-item';

import { UserHassAccess } from 'app/models/user_has_access/user-hass-access.model';
import { EntryEdit } from '../../models/entry/entry-edit.model';
import { UserHassAccessRemove } from '../../models/user_has_access/user-hass-access-remove.model';
import { EntryPremissionService } from '../../service/entry-premission.service';
import { User } from '../../models/user/user.model';
import { ITEMS_PREMISSION_TO_FOLDER } from 'app/helpclasses/moke-item-premission-to-folder';

declare interface TableData {
  headerRow: string[];
  userHasAccess: UserHassAccess[];
}

declare interface AddSpecific {
  headerRow: string[];
  canRead: string;
  canExecute: string;
  canWrite: string;
}

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  entryId: number;
  entry: Entry;
  public tableData1: TableData;

  //-----------------------Entry others premission----------------------------------------
  radioSel: any;
  radioSelected: string;
  itemsList: Item[] = ITEMS;
  //--------------------------------------------------------------------------------------

  //-----------------------Create access to user------------------------------------------
  radioSelUser: any;
  readioSelcectedUser: string;
  intemListFolderPremission: Item[] = ITEMS_PREMISSION_TO_FOLDER;

  //--------------------------------------------------------------------------------------

  constructor(
    private router: ActivatedRoute,
    private entryService: EntryService,
    private authenticationService: AuthenticationService,
    private premissionService: EntryPremissionService
  ) {

    this.itemsList = ITEMS;
    this.radioSelected = 'private';
    this.getSelecteditem();

    this.intemListFolderPremission = ITEMS_PREMISSION_TO_FOLDER;
    this.readioSelcectedUser = 'view'
    this.getSelecteditemFolderAccess();

    this.tableData1 = {
      headerRow: ['Index', 'User', 'Premission', 'Filename', 'OPERATION'],
      userHasAccess: []
    };

  }

  ngOnInit() {
    this.router.params.subscribe(
      (params: Params) => {
        this.entryId = +params['id'];
      }
    );




    this.entryService.getEntryWithAllPremision(this.entryId).subscribe(
      (data: EntryEdit) => {
        this.tableData1.userHasAccess = data.list;
        this.entry = data.entry;
        console.log(this.entry);
        if (this.entry.otherCanRead && this.entry.otherCanExecute && this.entry.otherCanWrite) {
          this.radioSelected = 'public';
        } else if (this.entry.otherCanExecute && this.entry.otherCanRead && !this.entry.otherCanWrite) {
          this.radioSelected = 'public view'
        } else if (!this.entry.otherCanExecute && !this.entry.otherCanRead && !this.entry.otherCanWrite) {
          this.radioSelected = 'private';
        } else {
          console.log('Somthing gose wrong');
        }
      }
    );

  }


  onRename(form: NgForm) {
    console.log(form);
    const rename = new EntryRename();
    rename.fullpath = form.value.fullpath;
    rename.id = this.entryId;

    this.entryService.renameEntry(rename).subscribe(
      (data: Entry) => {
        this.entry = data;
        form.reset();
      }
    )
  }

  onAccess(form: NgForm) {
    if (form.valid) {
      let userHassAccess = new UserHassAccess();
      let userModel = new User();
      userModel.username = form.value.username;
      userHassAccess.userModel = userModel;
      userHassAccess.id = this.entryId;
      if (form.value.radiobuttonFolder === 'block') {
        userHassAccess.canExecute = false;
        userHassAccess.canRead = false;
        userHassAccess.canWrite = false;
      } else if (form.value.radiobuttonFolder === 'view') {
        userHassAccess.canExecute = true;
        userHassAccess.canRead = true;
        userHassAccess.canWrite = false;
      } else if (form.value.radiobuttonFolder === 'view create') {
        userHassAccess.canExecute = true;
        userHassAccess.canRead = true;
        userHassAccess.canWrite = true;
      }

      this.premissionService.create(userHassAccess).subscribe(
        (data: UserHassAccess) => {
          console.log(data);
          this.tableData1.userHasAccess.push(data)
        }
      );
    }
  }


  onDelete(id: number, index: number) {
    let userHasAccessRemove = new UserHassAccessRemove();
    userHasAccessRemove.entryId = this.entryId;
    userHasAccessRemove.userId = id;
    this.premissionService.deleteUserPremission(userHasAccessRemove).subscribe(
      (data: UserHassAccessRemove) => {

        this.tableData1.userHasAccess.splice(index ,1);
        // let us: UserHassAccess = this.tableData1.userHasAccess.find(x => data.userId === id);
        // console.log(us);
        // let index = this.tableData1.userHasAccess.indexOf(us);
        // console.log(index)
        // this.tableData1.userHasAccess.splice(index, 1);
      }
    );
  }


  onBlock(id: number) {
    const userHassAccess = new UserHassAccess();
    userHassAccess.id = this.entryId;
    userHassAccess.canExecute = false;
    userHassAccess.canRead = false;
    userHassAccess.canWrite = false;
    let userHas = this.tableData1.userHasAccess.find(userHas => userHas.userModel.id === id);
    const userModel = new User();
    userModel.id = userHas.userModel.id;
    userModel.username = userHas.userModel.username;
    userHassAccess.userModel = userModel;
    this.premissionService.update(userHassAccess).subscribe(
      (data: UserHassAccess) => {
        const user = this.tableData1.userHasAccess.find(userHas => userHas.userModel.id === data.userModel.id);
        user.canExecute = data.canExecute;
        user.canRead = data.canRead;
        user.canWrite = data.canWrite;
        console.log(user);
      }
    );
  }

  onView(id: number) {
    const userHassAccess = new UserHassAccess();
    userHassAccess.id = this.entryId;
    userHassAccess.canExecute = true;
    userHassAccess.canRead = true;
    userHassAccess.canWrite = false;
    let userHas = this.tableData1.userHasAccess.find(userHas => userHas.userModel.id === id);
    const userModel = new User();
    userModel.id = userHas.userModel.id;
    userModel.username = userHas.userModel.username;
    userHassAccess.userModel = userModel;
    this.premissionService.update(userHassAccess).subscribe(
      (data: UserHassAccess) => {
        const user = this.tableData1.userHasAccess.find(userHas => userHas.userModel.id === data.userModel.id);
        user.canExecute = data.canExecute;
        user.canRead = data.canRead;
        user.canWrite = data.canWrite;
        console.log(user);
      }
    );
  }

  onViewCreate(id: number) {
    const userHassAccess = new UserHassAccess();
    userHassAccess.id = this.entryId;
    userHassAccess.canExecute = true;
    userHassAccess.canRead = true;
    userHassAccess.canWrite = true;
    let userHas = this.tableData1.userHasAccess.find(userHas => userHas.userModel.id === id);
    const userModel = new User();
    userModel.id = userHas.userModel.id;
    userModel.username = userHas.userModel.username;
    userHassAccess.userModel = userModel;
    this.premissionService.update(userHassAccess).subscribe(
      (data: UserHassAccess) => {
        const user = this.tableData1.userHasAccess.find(userHas => userHas.userModel.id === data.userModel.id);
        user.canExecute = data.canExecute;
        user.canRead = data.canRead;
        user.canWrite = data.canWrite;
        console.log(user);
      }
    );
  }

  //--------------------Select Item-------------------------------------
  getSelecteditem() {
    this.radioSel = ITEMS.find(Item => Item.value === this.radioSelected);
    console.log(this.radioSel);
  }

  onItemChange(item) {
    this.getSelecteditem();
  }
  //--------------------------------------------------------------------

  getSelecteditemFolderAccess() {
    this.radioSelUser = ITEMS.find(Item => Item.value === this.readioSelcectedUser);
    console.log(this.radioSelUser);
  }

  onItemChangeFolderAccess(item) {
    this.getSelecteditem();
  }
}
