import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { NavbarModule } from './shared/navbar/navbar.module';
import { FooterModule } from './shared/footer/footer.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { LbdModule } from './lbd/lbd.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { TablesComponent } from './tables/tables.component';
import { IconsComponent } from './icons/icons.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { AuthenticationService } from 'app/service/authentication.service';
import { UserService } from './service/user.service';
import { SharedService } from './service/shared.service';
import { HttpClientModule } from '@angular/common/http';
import { AdminService } from './service/admin.service';
import { EntryComponent } from './entry/entry.component';
import { EntryService } from './service/entry.service';
import { WelcomeComponent } from './welcome/welcome.component';
import { DirectoryComponent } from './entry/directory/directory.component';
import { EditComponent } from './entry/edit/edit.component';
import { DatabaseComponent } from './database/database.component';
import { ProcessesComponent } from './processes/processes.component';
import { UserWithTokenSerializer } from 'app/serializer/user/user-with-token-serializer';
import { FileComponent } from './entry/file/file.component';
import { SchemaService } from 'app/service/schema.service';
import { TableComponent } from './database/table/table.component';
import { SchemaObjectService } from 'app/service/schema-object.service';
import { SelectAllComponent } from './database/select-all/select-all.component';
import { SelectStructureComponent } from './database/select-structure/select-structure.component';
import { InsertComponent } from './database/insert/insert.component';
import { UpdateComponent } from './database/update/update.component';
import { CreateTableComponent } from './database/create-table/create-table.component';
import { ProcessesService } from 'app/service/processes.service';
import { ProcessLogComponent } from './processes/process-log/process-log.component';
import { ProcessLogContentComponent } from './processes/process-log-content/process-log-content.component';
import { AddColumnComponent } from './database/add-column/add-column.component';
import { AlertColumnComponent } from './database/alert-column/alert-column.component';
import { AddKeyComponent } from './database/add-key/add-key.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserComponent,
    TablesComponent,
    IconsComponent,
    NotificationsComponent,
    EntryComponent,
    WelcomeComponent,
    DirectoryComponent,
    EditComponent,
    DatabaseComponent,
    ProcessesComponent,
    FileComponent,
    TableComponent,
    SelectAllComponent,
    SelectStructureComponent,
    InsertComponent,
    UpdateComponent,
    CreateTableComponent,
    ProcessLogComponent,
    ProcessLogContentComponent,
    AddColumnComponent,
    AlertColumnComponent,
    AddKeyComponent

  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    NavbarModule,
    FooterModule,
    SidebarModule,
    RouterModule,
    AppRoutingModule,
    LbdModule,
    ReactiveFormsModule
  ],
  providers: [
    AuthenticationService,
    ProcessesService,
    SharedService,
    UserService,
    AdminService,
    EntryService,
    SchemaService,
    UserWithTokenSerializer,
    SchemaObjectService],
  bootstrap: [AppComponent]
})
export class AppModule { }
