import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Log } from '../../models/process/log/log';
import { ProcessesService } from '../../service/processes.service';

@Component({
  selector: 'app-process-log-content',
  templateUrl: './process-log-content.component.html',
  styleUrls: ['./process-log-content.component.css']
})
export class ProcessLogContentComponent implements OnInit {

  proccesName: string;
  log: Log;
  is_edit: boolean =true; 

  constructor(
    private router: ActivatedRoute,
    private processService : ProcessesService
  ) { }

  ngOnInit() {

    this.router.queryParams.subscribe(
      (params: Params) => {
        console.log(params)
        this.log = <Log> JSON.parse(params.record) as Log;
       
        this.processService.postLogView(this.log.id).subscribe(
          (data)=> {console.log(data)}
        );
      }
    );
  }

}
