import { Injectable } from '@angular/core';
import { UserHassAccess } from 'app/models/user_has_access/user-hass-access.model';
import { SharedService } from 'app/service/shared.service';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from 'app/service/authentication.service';
import { UserHassAccessRemove } from 'app/models/user_has_access/user-hass-access-remove.model';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs-compat/Observable';
import { UserHasAccessSerilalizer } from 'app/serializer/user-has-access/user-has-access-serilalizer';
import { UserHasAccessRemoveSerializer } from 'app/serializer/user-has-access/user-has-access-remove-serializer';

@Injectable({
  providedIn: 'root'
})
export class EntryPremissionService extends SharedService<UserHassAccess>{

  private pdelete: string = 'delete';

  constructor(httpClient: HttpClient, authenticate: AuthenticationService, http: Http) {
    super(httpClient,
      'http://localhost:8084/SuperCloud/webresources',
      'entry/premission',
      authenticate,
      new UserHasAccessSerilalizer())
  }


  deleteUserPremission(userHasAccessRemove: UserHassAccessRemove): Observable<UserHassAccessRemove> {
    let pserializer = new UserHasAccessRemoveSerializer();
    return this.httpClient.put(`${this.url}/${this.endpoint}/${this.pdelete}`, JSON.stringify(userHasAccessRemove),{ headers: this.addHeaders() }).pipe(
        map((data => pserializer.fromJson(data) as UserHassAccessRemove))
      )
  }

}
