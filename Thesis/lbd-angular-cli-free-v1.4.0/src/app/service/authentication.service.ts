import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Authentication } from '../models/authentication.model';
import { Observable } from 'rxjs/Observable';

import { UserWithToken } from '../models/user/user-with-token.model';
import { User } from '../models/user/user.model';
import { UserWithTokenSerializer } from 'app/serializer/user/user-with-token-serializer';

@Injectable()
export class AuthenticationService {

  user: UserWithToken;

  url: string = 'http://localhost:8084/SuperCloud/webresources/account/login';

  constructor(private http: HttpClient, private serializer: UserWithTokenSerializer) { }

  login(authentication: Authentication) {

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');

    return this.http.post<any>(this.url, JSON.stringify(authentication), { headers: headers })
      .map((data: UserWithToken) => { return this.serializer.fromJson(data) });
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }

  saveTestToken() {
    localStorage.setItem('currentUser', '{"token":"eyJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJtZSIsInN1YiI6InciLCJhdWQiOiJ5b3UiLCJleHAiOjE1MzU2NTQ1NjcsImlhdCI6MTUzNTY1NDI2NywianRpIjoiNmNkY2U5YjEtNGUwMC00NjdjLTkyNTktNjgxYmNhZmY1NDE3In0.g-H5lj5-PgDxSzuRuk9SZfFsxHIz5l8piZr0wmoqDHRt6j_HnMchedMarlf942BKKhy161Wn8Nr9mHRYxpFbSg","user":{"id":2,"name":"w","surname":"w","email":"w","phoneNumber":"w","password":"w"}}')
  }


  getLoggedUser() : User{
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    return this.user.user;
  }

  getToken(): String {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    return this.user.token;
  }

  getLoggedUserId() :number {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    return this.user.user.id;
  }

}
