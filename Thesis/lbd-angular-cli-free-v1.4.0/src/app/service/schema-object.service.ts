import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from 'app/service/authentication.service';
import { SharedService } from './shared.service';
import { SchemaObject } from '../models/schema/schema-object.model';
import { SchemaObjectSerializer } from 'app/serializer/schema/schema-object-serializer';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { SchemaObjectListSerializer } from 'app/serializer/schema/schema-object-list-serializer';
import { DropSchemObject } from 'app/models/schema/drop-schem-object.model';
import { DropSchemaObjectSerilalizer } from 'app/serializer/schema/drop-schema-object-serilalizer';
import { SelectAll } from 'app/models/schema/select/select-all.model';
import { SelectAllSerializer } from 'app/serializer/schema/schemaSelectAll/select-all-serializer';
import { Select } from 'app/models/schema/select.model';
import { SelectPrimaryKeyList } from 'app/models/schema/select-primary-key/select-primary-key-list.model';
import { PrimaryKeySerializer } from 'app/serializer/schema/tablePrimaryKey/primary-key-serializer';
import { SelectTableUniqueKeys } from 'app/models/schema/select-unique-key/select-table-unique-keys.model';
import { UniqueKeySerializer } from 'app/serializer/schema/tableUniqueKey/unique-key-serializer';
import { SelectTableForeignKeys } from 'app/models/schema/select-foreign-key/select-table-foreign-keys.model';
import { TableForeignKeysSerializer } from 'app/serializer/schema/tableForeignKey/table-foreign-keys-serializer';

@Injectable()
export class SchemaObjectService extends SharedService<SchemaObject>{

  private drop: string = 'drop';
  private selectall: string = 'selectall';
  private structure: string = 'structure';
  private primaryKey: string = 'primarykey';
  private unique: string = 'unique';
  private foreign:string= 'foreign';

  constructor(
    httpClient: HttpClient,
    authenticate: AuthenticationService
  ) {

    super(
      httpClient,
      'http://localhost:8084/SuperCloud/webresources/database',
      'table',
      authenticate,
      new SchemaObjectSerializer()
    );
  }

  dropTable(dropSchemObject: DropSchemObject): Observable<DropSchemObject> {
    let pserializer = new DropSchemaObjectSerilalizer();
    return this.httpClient.put(`${this.url}/${this.endpoint}/${this.drop}`, JSON.stringify(dropSchemObject), { headers: this.addHeaders() }).pipe(
      map(data => pserializer.fromJson(data) as DropSchemObject)
    )
  }

  getAllTableContent(select: Select): Observable<SelectAll> {
    let pserializer = new SelectAllSerializer();
    return this.httpClient.post(`${this.url}/${this.endpoint}/${this.selectall}`, JSON.stringify(select), { headers: this.addHeaders() }).pipe(
      map(data =>
        pserializer.fromJson(data) as SelectAll)
    )
  }

  getTableStructure(select: Select): Observable<SelectAll> {
    let pserializer = new SelectAllSerializer();
    return this.httpClient.post(`${this.url}/${this.endpoint}/${this.structure}`, JSON.stringify(select), { headers: this.addHeaders() }).pipe(
      map(data => pserializer.fromJson(data) as SelectAll)
    )
  }

  getTablePrimaryKey(select: Select): Observable<SelectPrimaryKeyList> {
    let pserializer = new PrimaryKeySerializer();
    return this.httpClient.post(`${this.url}/${this.endpoint}/${this.primaryKey}`, JSON.stringify(select), { headers: this.addHeaders() }).pipe(
      map(data => pserializer.fromJson(data) as SelectPrimaryKeyList)
    )
  }

  getTableUniqueKey(select: Select): Observable<SelectTableUniqueKeys> {
    let pserializer = new UniqueKeySerializer();
    return this.httpClient.post(`${this.url}/${this.endpoint}/${this.unique}`, JSON.stringify(select), { headers: this.addHeaders() }).pipe(
      map(data => pserializer.fromJson(data) as SelectTableUniqueKeys)
    )
  }

  getTableForeignKey(select:Select): Observable<SelectTableForeignKeys>{
    let pserializer = new TableForeignKeysSerializer();
    return this.httpClient.post(`${this.url}/${this.endpoint}/${this.foreign}`, JSON.stringify(select), { headers: this.addHeaders() }).pipe(
      map(data => pserializer.fromJson(data) as SelectTableForeignKeys)
    )
  }

}
