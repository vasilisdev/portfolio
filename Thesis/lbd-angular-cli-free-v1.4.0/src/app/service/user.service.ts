import { Injectable } from '@angular/core';
import { SharedService } from './shared.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user/user.model';
import { AuthenticationService } from 'app/service/authentication.service';

import { NgForm } from '@angular/forms';
import { UserSerializer } from 'app/serializer/user/user-serializer';

@Injectable()
export class UserService extends SharedService<User>{

  constructor(httpClient: HttpClient, authenticate: AuthenticationService) {
    super(httpClient,
      'http://localhost:8084/SuperCloud/webresources',
      'user',
      authenticate,
      new UserSerializer()
    );
  }

  formRegister(form: NgForm): User {
    const newUser: User = new User();
    newUser.email = form.value.email;
    newUser.username = form.value.username;
    newUser.name = form.value.name;
    newUser.surname = form.value.surname;
    newUser.phoneNumber = form.value.phoneNumber;
    newUser.password = form.value.password;
    return newUser;
  }

  fromFormToUser(form: NgForm, user: User): User {
    console.log(form);
    const newUser: User = new User();
    newUser.id = user.id;
    newUser.active = user.active;
    newUser.username = form.value.username;
    newUser.email = form.value.email;
    newUser.password = form.value.password;
    newUser.name = form.value.name;
    newUser.surname = form.value.surname;
    newUser.phoneNumber = form.value.phoneNumber;
    return newUser;
  }



}
