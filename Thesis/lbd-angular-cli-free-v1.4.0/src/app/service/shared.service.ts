import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Resource } from '../models/resource.model';
import { Serializer } from '../serializer/serializer';
import { AuthenticationService } from './authentication.service';
import { Observable } from 'rxjs/Observable';
import { tap, map } from 'rxjs/operators';
import 'rxjs/add/operator/map'


@Injectable()
export class SharedService<T extends Resource> {
  constructor(
    protected httpClient: HttpClient,
    protected url: string,
    protected endpoint: string,
    protected authentication: AuthenticationService,
    protected serializer: Serializer
  ) { }

  //Post
  create(resource: T) {
    return this.httpClient.post(`${this.url}/${this.endpoint}`, JSON.stringify(resource), { headers: this.addHeaders() }).pipe(
      map(data => this.serializer.fromJson(data) as T)
    )
  }

  //PUT
  update(item: T): Observable<T> {
    return this.httpClient.put<T>(`${this.url}/${this.endpoint}`, JSON.stringify(item), { headers: this.addHeaders() }).pipe(
      map(data => this.serializer.fromJson(data) as T)
    )
  }

  //GET
  read(id: number): Observable<T> {
    console.log(id);
    return this.httpClient.get<T>(`${this.url}/${this.endpoint}/${id}`, { headers: this.addHeaders() }).pipe(
      map(data => this.serializer.fromJson(data) as T)
    )
  }

  //GET ALL
  list(): Observable<T[]> {
    return this.httpClient.get(`${this.url}/${this.endpoint}`, { headers: this.addHeaders() }).pipe(
      map((data: T[]) => (this.convertData(data)))
    )
  }

  //Delete
  delete(id: number) {
    return this.httpClient.delete(`${this.url}/${this.endpoint}/${id}` , { headers: this.addHeaders()});
  }


  protected convertData(data: any): T[] {
    return data.map(item => { return this.serializer.fromJson(item) as T });
  }

  //edo bazw to token
  protected addHeaders() {
    let token = ('Bearer ' + this.authentication.getToken()).valueOf();
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8').set('Authorization', token);
    return headers;
  }

}
