import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ColumnName } from 'app/models/schema/select/column-name';

@Component({
  selector: 'app-insert',
  templateUrl: './insert.component.html',
  styleUrls: ['./insert.component.css']
})
export class InsertComponent implements OnInit {

  schemaName: string;
  tableName: string;
  columnsName: ColumnName;
  columns: string[];
  constructor(
    private router: ActivatedRoute
  ) { }

  ngOnInit() {

    this.router.params.subscribe(

      (params: Params) => {
        this.schemaName = params['schemaName'];
        this.tableName = params['tableName'];
      }

    );

    this.router.queryParams.subscribe(
      (params: Params) => {
        this.columnsName = JSON.parse(params.record) as ColumnName;
        this.columns = this.columnsName.columnsName;
      }
    );
  }

}
