import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SelectAll } from '../../models/schema/select/select-all.model';
import { ColumnName } from 'app/models/schema/select/column-name';
import { Rows } from 'app/models/schema/select/rows.model';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  schemaName: string;
  tableName: string;
  selectAll: SelectAll;
  columnsName: ColumnName;
  col: string[];
  rows :Rows;


  constructor(
    private router: ActivatedRoute
  ) { }

  ngOnInit() {

    this.router.params.subscribe(
      (params: Params) => {
        this.schemaName = params['schemaName'];
        this.tableName = params['tableName'];
      }
    );

    this.router.queryParams.subscribe(
      (params: Params) => {
        this.selectAll =<SelectAll> JSON.parse(params.record) as SelectAll;
        this.columnsName = <ColumnName> this.selectAll.columnsName;
        this.rows = <Rows> this.selectAll.rows;
      }
    );
  }

}
