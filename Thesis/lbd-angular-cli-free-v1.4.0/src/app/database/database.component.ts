import { Component, OnInit } from '@angular/core';
import { Schema } from 'app/models/schema/schema.model';
import { SchemaService } from '../service/schema.service';
import { AuthenticationService } from 'app/service/authentication.service';
import { NgForm } from '@angular/forms';
import { User } from '../models/user/user.model';

declare interface TableData {
  headerRow: string[];
  schemas: Schema[];
}


@Component({
  selector: 'app-database',
  templateUrl: './database.component.html',
  styleUrls: ['./database.component.css']
})
export class DatabaseComponent implements OnInit {
  tableData1: TableData;

  currentUserId: number;

  constructor(
    private schemaService: SchemaService,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.currentUserId = this.authenticationService.getLoggedUserId();
    this.tableData1 = {
      headerRow: ['ID', 'Database Name', 'OWNER', 'OPERATION'],
      schemas: []
    };

    this.schemaService.list().subscribe(
      (data: Schema[]) => {
        this.tableData1.schemas = data;
      }
    );


  }

  onSubmit(form: NgForm) {
    let schema = new Schema();
    schema.dataBaseName = form.value.dataBaseName;
    let user = new User();
    user.id = this.currentUserId;
    schema.ownerId = user;

    this.schemaService.create(schema).subscribe(
      (data: Schema) => this.tableData1.schemas.push(data)
    );
  }

  onDelete(id: number, index: number) {
    // console.log(id);
    // console.log(index);


    let schema = this.tableData1.schemas.find(x => x.id === id);

    this.schemaService.update(schema).subscribe(
      () => this.tableData1.schemas.splice(index, 1)
    );
  }

}
