import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ColumnName } from 'app/models/schema/select/column-name';
import { Foreign } from 'app/models/schema/foreign/foreign.model';

@Component({
  selector: 'app-add-key',
  templateUrl: './add-key.component.html',
  styleUrls: ['./add-key.component.css']
})
export class AddKeyComponent implements OnInit {

  schemaName: string;
  tableName: string;
  columnsName: ColumnName;

  saveForeingKey: boolean = false;

  //-----------Foreign
  forignKeyList: Foreign[] = [];
  forignKey1Src: Foreign;
  forignKey1ScrList: string[] = ['City'];
  //-----------------

  constructor(private router: ActivatedRoute) {

    this.forignKey1Src = new Foreign();
    this.forignKey1Src.foreign = this.forignKey1ScrList;

    this.forignKeyList.push(this.forignKey1Src);
   }

  ngOnInit() {
    this.router.params.subscribe(
      (params: Params) => {
        this.schemaName = params['schemaName'];
        this.tableName = params['tableName'];
      }
    );

    this.router.queryParams.subscribe(
      (params: Params) => {
        this.columnsName = JSON.parse(params['record']) as ColumnName;
      }
    );
  }

  onAddForeignKey(){
    this.saveForeingKey = !this.saveForeingKey;
    console.log(this.saveForeingKey);
  }

}
