import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'app/service/authentication.service';
import { ActivatedRoute, Params } from '@angular/router';
import { SchemaService } from 'app/service/schema.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms'
import { config } from 'rxjs';
import { Unique } from 'app/models/schema/unique/unique.model';
import { Foreign } from 'app/models/schema/foreign/foreign.model';



@Component({
  selector: 'app-create-table',
  templateUrl: './create-table.component.html',
  styleUrls: ['./create-table.component.css']
})
export class CreateTableComponent implements OnInit {

  schemaName: string;
  tableHeader: string[] = ['Column', 'Type', 'Size', 'Is Null', 'Default', 'Auto Increment', 'Operation'];
  tablePrimaryKey: string[] = ['id', 'name', 'surname' ,'cityId' ,'cityName'];
  tablePrimaryKeyValue: string[] = ['id'];
  numberOfColumns: number = 2;

  //------------UNIQUE
  unique1: Unique;
  unique1Key: string[] = ['name'];
  unique2:Unique;
  unique2Key: string[]= ['name', 'surname'];
  uniqueKeyList:Unique[] =[];
  //------------

  //-----------Foreign
  forignKeyList :Foreign[]=[];
  forignKey1Src : Foreign;
  forignKey1ScrList :string[] = ['cityId','cityName'];
  //-----------------



  save: boolean = false;
  savePrimaryKey: boolean = false;
  saveUniqueKey: boolean = false;
  saveForeingKey:boolean = false;

  constructor(
    private authenticationService: AuthenticationService,
    private router: ActivatedRoute,
    private schemaService: SchemaService
  ) {
    this.unique1 = new Unique();
    this.unique1.uniqueKey = this.unique1Key;

    this.unique2 = new Unique();
    this.unique2.uniqueKey = this.unique2Key;

    this.uniqueKeyList.push(this.unique1);
    this.uniqueKeyList.push(this.unique2);

    this.forignKey1Src = new Foreign();
    this.forignKey1Src.foreign = this.forignKey1ScrList;

    this.forignKeyList.push(this.forignKey1Src);
  }

  ngOnInit() {
    this.router.paramMap.subscribe(
      (params) => {
        console.log(params)
        this.schemaName = params.get('schemaName');
        console.log(this.schemaName);
      }
    );
  }

  onSave() {
    this.save = !this.save;
  }

  onSavePrimaryKey() {
    this.savePrimaryKey = !this.savePrimaryKey;
  }

  onSaveUniqueKey() {
    this.saveUniqueKey = !this.saveUniqueKey;
  }

  onSaveForeignKey(){
    this.saveForeingKey = !this.saveForeingKey;
  }


}
