import { Component, OnInit } from '@angular/core';
import { SelectAll } from 'app/models/schema/select/select-all.model';
import { ColumnName } from 'app/models/schema/select/column-name';
import { Rows } from 'app/models/schema/select/rows.model';
import { SchemaObjectService } from 'app/service/schema-object.service';
import { ActivatedRoute, Params } from '@angular/router';
import { AuthenticationService } from 'app/service/authentication.service';
import { Select } from 'app/models/schema/select.model';
import { SelectPrimaryKeyList } from 'app/models/schema/select-primary-key/select-primary-key-list.model';
import { PrimaryKeyColumns } from 'app/models/schema/select-primary-key/primary-key-columns.model';
import { SelectTableUniqueKeys } from 'app/models/schema/select-unique-key/select-table-unique-keys.model';
import { SelectTableForeignKeys } from 'app/models/schema/select-foreign-key/select-table-foreign-keys.model';

@Component({
  selector: 'app-select-structure',
  templateUrl: './select-structure.component.html',
  styleUrls: ['./select-structure.component.css']
})
export class SelectStructureComponent implements OnInit {

  schemaName: string;
  tableName: string;
  selectAll: SelectAll;
  columnsName: ColumnName;
  rows: Rows;

  colName: string[] = [];

  selectPrimaryKeyList: SelectPrimaryKeyList;
  primaryKeyColumns: PrimaryKeyColumns;

  selectTableUniqueKeys: SelectTableUniqueKeys;
  selectTableForeignKeys: SelectTableForeignKeys;

  foreignKeyHeader: string[];

  constructor(
    private authenticationService: AuthenticationService,
    private router: ActivatedRoute,
    private schemaObjectService: SchemaObjectService
  ) { }

  ngOnInit() {

    this.foreignKeyHeader = ['Column Name', 'Referenced Column', 'Refererenced Table', 'Constraint', 'Operation']

    this.router.params.subscribe(
      (params: Params) => {
        this.schemaName = params['schemaName'];

        this.tableName = params['tableName'];
        const select = new Select();
        select.tableName = this.tableName;
        select.schemaName = this.schemaName;

        this.schemaObjectService.getTableStructure(select).subscribe(
          (data: SelectAll) => {
            this.selectAll = data;
            this.columnsName = data.columnsName;
            this.rows = data.rows

            for (let i in this.rows.row) {
              for (let o in this.rows.row[i].elements) {
                if (this.rows.row[i].elements.indexOf(this.rows.row[i].elements[o]) === 1) {
                  this.colName.push(this.rows.row[i].elements[o].value);
                  break;
                }
              }
            }
          }
        );

        this.schemaObjectService.getTablePrimaryKey(select).subscribe(
          (data: SelectPrimaryKeyList) => {
            this.selectPrimaryKeyList = data;
            this.primaryKeyColumns = this.selectPrimaryKeyList.primaryKeyColumns;
          }
        );

        this.schemaObjectService.getTableUniqueKey(select).subscribe(
          (data: SelectTableUniqueKeys) => {
            console.log(data)
            this.selectTableUniqueKeys = data;
          }
        );

        this.schemaObjectService.getTableForeignKey(select).subscribe(
          (data: SelectTableForeignKeys) => {
            this.selectTableForeignKeys = data;
          }
        );

      }
    );
  }

}
