package utils;

import java.util.List;

public class SelectForeignKey {
    private String constrainName;
    private List<Element> elements;

    public SelectForeignKey() {
    }

    public String getConstrainName() {
        return constrainName;
    }

    public void setConstrainName(String constrainName) {
        this.constrainName = constrainName;
    }

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }
}
