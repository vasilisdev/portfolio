package utils;

import java.util.List;

public class SelectTableForeignKeys {

    private List<SelectForeignKey> foreignKeysList;

    public SelectTableForeignKeys() {
    }

    public List<SelectForeignKey> getForeignKeysList() {
        return foreignKeysList;
    }

    public void setForeignKeysList(List<SelectForeignKey> foreignKeysList) {
        this.foreignKeysList = foreignKeysList;
    }
}
