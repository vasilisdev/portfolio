package utils;

public class ColumnCaracteristic {

    private String columnName;

    public ColumnCaracteristic() {
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

}
