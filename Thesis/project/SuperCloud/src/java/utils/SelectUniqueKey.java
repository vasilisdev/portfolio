package utils;

import java.util.List;

public class SelectUniqueKey {

    private String uniqueKeyName;
    private List<String> columnsName;

    public SelectUniqueKey() {
    }

    public String getUniqueKeyName() {
        return uniqueKeyName;
    }

    public void setUniqueKeyName(String uniqueKeyName) {
        this.uniqueKeyName = uniqueKeyName;
    }

    public List<String> getColumnsName() {
        return columnsName;
    }

    public void setColumnsName(List<String> columnsName) {
        this.columnsName = columnsName;
    }

  

}
