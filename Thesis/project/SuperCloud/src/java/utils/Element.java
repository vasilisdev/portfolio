package utils;

public class Element {
    private String columnName;
    private String refererencedTable;
    private String referencedColumn;
    
    public Element() {
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getRefererencedTable() {
        return refererencedTable;
    }

    public void setRefererencedTable(String refererencedTable) {
        this.refererencedTable = refererencedTable;
    }

    public String getReferencedColumn() {
        return referencedColumn;
    }

    public void setReferencedColumn(String referencedColumn) {
        this.referencedColumn = referencedColumn;
    }

   
}
