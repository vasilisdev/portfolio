/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author psilos
 */
@Embeddable
public class UserHasAccessPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private int userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "entry_id")
    private int entryId;

    public UserHasAccessPK() {
    }

    public UserHasAccessPK(int userId, int entryId) {
        this.userId = userId;
        this.entryId = entryId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (int) entryId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserHasAccessPK)) {
            return false;
        }
        UserHasAccessPK other = (UserHasAccessPK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.entryId != other.entryId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.UserHasAccessPK[ userId=" + userId + ", entryId=" + entryId + " ]";
    }
    
}
