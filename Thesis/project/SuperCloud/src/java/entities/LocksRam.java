/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author psilos
 */
@Entity
@Table(name = "locks_ram")
@NamedQueries({
    @NamedQuery(name = "LocksRam.findAll", query = "SELECT l FROM LocksRam l")
    , @NamedQuery(name = "LocksRam.findByLockId", query = "SELECT l FROM LocksRam l WHERE l.lockId = :lockId")
    , @NamedQuery(name = "LocksRam.findByEntryId", query = "SELECT l FROM LocksRam l WHERE l.entryId = :entryId")
    , @NamedQuery(name = "LocksRam.findByEntryIdTarget", query = "SELECT l FROM LocksRam l WHERE l.entryIdTarget = :entryIdTarget")
    , @NamedQuery(name = "LocksRam.findByActiveLock", query = "SELECT l FROM LocksRam l WHERE l.activeLock = :activeLock")})
public class LocksRam implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "lock_id")
    private String lockId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "entry_id")
    private int entryId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "entry_id_target")
    private int entryIdTarget;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active_lock")
    private boolean activeLock;

    public LocksRam() {
    }

    public LocksRam(String lockId) {
        this.lockId = lockId;
    }

    public LocksRam(String lockId, int entryId, int entryIdTarget, boolean activeLock) {
        this.lockId = lockId;
        this.entryId = entryId;
        this.entryIdTarget = entryIdTarget;
        this.activeLock = activeLock;
    }

    public String getLockId() {
        return lockId;
    }

    public void setLockId(String lockId) {
        this.lockId = lockId;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public int getEntryIdTarget() {
        return entryIdTarget;
    }

    public void setEntryIdTarget(int entryIdTarget) {
        this.entryIdTarget = entryIdTarget;
    }

    public boolean getActiveLock() {
        return activeLock;
    }

    public void setActiveLock(boolean activeLock) {
        this.activeLock = activeLock;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lockId != null ? lockId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocksRam)) {
            return false;
        }
        LocksRam other = (LocksRam) object;
        if ((this.lockId == null && other.lockId != null) || (this.lockId != null && !this.lockId.equals(other.lockId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.LocksRam[ lockId=" + lockId + " ]";
    }
    
}
