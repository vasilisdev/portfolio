/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author psilos
 */
@Entity
@Table(name = "user_has_access_to_schema")
@NamedQueries({
    @NamedQuery(name = "UserHasAccessToSchema.findAll", query = "SELECT u FROM UserHasAccessToSchema u")
    , @NamedQuery(name = "UserHasAccessToSchema.findByUserId", query = "SELECT u FROM UserHasAccessToSchema u WHERE u.userHasAccessToSchemaPK.userId = :userId")
    , @NamedQuery(name = "UserHasAccessToSchema.findBySchemaId", query = "SELECT u FROM UserHasAccessToSchema u WHERE u.userHasAccessToSchemaPK.schemaId = :schemaId")
    , @NamedQuery(name = "UserHasAccessToSchema.findByCanModify", query = "SELECT u FROM UserHasAccessToSchema u WHERE u.canModify = :canModify")})
public class UserHasAccessToSchema implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserHasAccessToSchemaPK userHasAccessToSchemaPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_modify")
    private boolean canModify;
    @JoinColumn(name = "schema_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Schema schema;
    @JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private User user;

    public UserHasAccessToSchema() {
    }

    public UserHasAccessToSchema(UserHasAccessToSchemaPK userHasAccessToSchemaPK) {
        this.userHasAccessToSchemaPK = userHasAccessToSchemaPK;
    }

    public UserHasAccessToSchema(UserHasAccessToSchemaPK userHasAccessToSchemaPK, boolean canModify) {
        this.userHasAccessToSchemaPK = userHasAccessToSchemaPK;
        this.canModify = canModify;
    }

    public UserHasAccessToSchema(int userId, int schemaId) {
        this.userHasAccessToSchemaPK = new UserHasAccessToSchemaPK(userId, schemaId);
    }

    public UserHasAccessToSchemaPK getUserHasAccessToSchemaPK() {
        return userHasAccessToSchemaPK;
    }

    public void setUserHasAccessToSchemaPK(UserHasAccessToSchemaPK userHasAccessToSchemaPK) {
        this.userHasAccessToSchemaPK = userHasAccessToSchemaPK;
    }

    public boolean getCanModify() {
        return canModify;
    }

    public void setCanModify(boolean canModify) {
        this.canModify = canModify;
    }

    public Schema getSchema() {
        return schema;
    }

    public void setSchema(Schema schema) {
        this.schema = schema;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userHasAccessToSchemaPK != null ? userHasAccessToSchemaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserHasAccessToSchema)) {
            return false;
        }
        UserHasAccessToSchema other = (UserHasAccessToSchema) object;
        if ((this.userHasAccessToSchemaPK == null && other.userHasAccessToSchemaPK != null) || (this.userHasAccessToSchemaPK != null && !this.userHasAccessToSchemaPK.equals(other.userHasAccessToSchemaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.UserHasAccessToSchema[ userHasAccessToSchemaPK=" + userHasAccessToSchemaPK + " ]";
    }
    
}
