/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author psilos
 */
@Entity
@Table(name = "user_has_access_to_object")
@NamedQueries({
    @NamedQuery(name = "UserHasAccessToObject.findAll", query = "SELECT u FROM UserHasAccessToObject u")
    , @NamedQuery(name = "UserHasAccessToObject.findByUserId", query = "SELECT u FROM UserHasAccessToObject u WHERE u.userHasAccessToObjectPK.userId = :userId")
    , @NamedQuery(name = "UserHasAccessToObject.findBySchemaObjectsId", query = "SELECT u FROM UserHasAccessToObject u WHERE u.userHasAccessToObjectPK.schemaObjectsId = :schemaObjectsId")
    , @NamedQuery(name = "UserHasAccessToObject.findByCanSelect", query = "SELECT u FROM UserHasAccessToObject u WHERE u.canSelect = :canSelect")
    , @NamedQuery(name = "UserHasAccessToObject.findByCanInsert", query = "SELECT u FROM UserHasAccessToObject u WHERE u.canInsert = :canInsert")
    , @NamedQuery(name = "UserHasAccessToObject.findByCanUpdate", query = "SELECT u FROM UserHasAccessToObject u WHERE u.canUpdate = :canUpdate")
    , @NamedQuery(name = "UserHasAccessToObject.findByCanDelete", query = "SELECT u FROM UserHasAccessToObject u WHERE u.canDelete = :canDelete")
    , @NamedQuery(name = "UserHasAccessToObject.findByCanAlter", query = "SELECT u FROM UserHasAccessToObject u WHERE u.canAlter = :canAlter")
    , @NamedQuery(name = "UserHasAccessToObject.findByCanDrop", query = "SELECT u FROM UserHasAccessToObject u WHERE u.canDrop = :canDrop")})
public class UserHasAccessToObject implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserHasAccessToObjectPK userHasAccessToObjectPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_select")
    private boolean canSelect;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_insert")
    private boolean canInsert;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_update")
    private boolean canUpdate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_delete")
    private boolean canDelete;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_alter")
    private boolean canAlter;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_drop")
    private boolean canDrop;
    @JoinColumn(name = "schema_objects_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private SchemaObject schemaObject;
    @JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private User user;

    public UserHasAccessToObject() {
    }

    public UserHasAccessToObject(UserHasAccessToObjectPK userHasAccessToObjectPK) {
        this.userHasAccessToObjectPK = userHasAccessToObjectPK;
    }

    public UserHasAccessToObject(UserHasAccessToObjectPK userHasAccessToObjectPK, boolean canSelect, boolean canInsert, boolean canUpdate, boolean canDelete, boolean canAlter, boolean canDrop) {
        this.userHasAccessToObjectPK = userHasAccessToObjectPK;
        this.canSelect = canSelect;
        this.canInsert = canInsert;
        this.canUpdate = canUpdate;
        this.canDelete = canDelete;
        this.canAlter = canAlter;
        this.canDrop = canDrop;
    }

    public UserHasAccessToObject(int userId, int schemaObjectsId) {
        this.userHasAccessToObjectPK = new UserHasAccessToObjectPK(userId, schemaObjectsId);
    }

    public UserHasAccessToObjectPK getUserHasAccessToObjectPK() {
        return userHasAccessToObjectPK;
    }

    public void setUserHasAccessToObjectPK(UserHasAccessToObjectPK userHasAccessToObjectPK) {
        this.userHasAccessToObjectPK = userHasAccessToObjectPK;
    }

    public boolean getCanSelect() {
        return canSelect;
    }

    public void setCanSelect(boolean canSelect) {
        this.canSelect = canSelect;
    }

    public boolean getCanInsert() {
        return canInsert;
    }

    public void setCanInsert(boolean canInsert) {
        this.canInsert = canInsert;
    }

    public boolean getCanUpdate() {
        return canUpdate;
    }

    public void setCanUpdate(boolean canUpdate) {
        this.canUpdate = canUpdate;
    }

    public boolean getCanDelete() {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    public boolean getCanAlter() {
        return canAlter;
    }

    public void setCanAlter(boolean canAlter) {
        this.canAlter = canAlter;
    }

    public boolean getCanDrop() {
        return canDrop;
    }

    public void setCanDrop(boolean canDrop) {
        this.canDrop = canDrop;
    }

    public SchemaObject getSchemaObject() {
        return schemaObject;
    }

    public void setSchemaObject(SchemaObject schemaObject) {
        this.schemaObject = schemaObject;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userHasAccessToObjectPK != null ? userHasAccessToObjectPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserHasAccessToObject)) {
            return false;
        }
        UserHasAccessToObject other = (UserHasAccessToObject) object;
        if ((this.userHasAccessToObjectPK == null && other.userHasAccessToObjectPK != null) || (this.userHasAccessToObjectPK != null && !this.userHasAccessToObjectPK.equals(other.userHasAccessToObjectPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UserHasAccessToObject{" + "userHasAccessToObjectPK=" + userHasAccessToObjectPK + ", canSelect=" + canSelect + ", canInsert=" + canInsert + ", canUpdate=" + canUpdate + ", canDelete=" + canDelete + ", canAlter=" + canAlter + ", canDrop=" + canDrop + ", schemaObject=" + schemaObject + ", user=" + user + '}';
    }
}
