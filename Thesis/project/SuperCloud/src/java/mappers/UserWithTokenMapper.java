package mappers;

import entities.User;

public class UserWithTokenMapper {
    
    private String token;
    private User user;

    public UserWithTokenMapper() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "UserWithTokenMapper{" + "token=" + token + ", user=" + user + '}';
    } 
    
}
