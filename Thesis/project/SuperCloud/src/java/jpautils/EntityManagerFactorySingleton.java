package jpautils;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

// context listener
public class EntityManagerFactorySingleton {

    public static EntityManagerFactory entityManagerFactory;

    static {
        entityManagerFactory = Persistence.createEntityManagerFactory("SuperCloudPU");
    }

    public static synchronized EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    public static synchronized void closeEntityManagerFactory() {
        entityManagerFactory.close();
        entityManagerFactory = null;
    }

    public static synchronized void createEntityManagerFactory() {
        entityManagerFactory = Persistence.createEntityManagerFactory("SuperCloudPU");
    }

    public static synchronized EntityManager getRuntimeEntityManager(String schemaName) {
        Map<String, String> addedOrOverridenProperties = new HashMap<>();

        String url = String.format("jdbc:mysql://localhost:3306/%s?zeroDateTimeBehavior=convertToNull", schemaName);
        
        addedOrOverridenProperties.put("javax.persistence.jdbc.url", url);

        EntityManagerFactory fact = Persistence.createEntityManagerFactory("RuntimePU", addedOrOverridenProperties);
        return fact.createEntityManager();
    }
}
