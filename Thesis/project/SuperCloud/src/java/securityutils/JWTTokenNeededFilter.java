package securityutils;

import angularsession.SessionMap;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import java.io.IOException;
import java.security.Key;
import java.util.Date;
import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@JWTTokenNeeded
@Provider
@Priority(Priorities.AUTHENTICATION)


public class JWTTokenNeededFilter implements ContainerRequestFilter {
    @Context
    private transient HttpServletRequest servletRequest;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        System.out.println(authorizationHeader);

        // Check if the HTTP Authorization header is present and formatted correctly 
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            throw new NotAuthorizedException("Authorization header must be provided");
        }

        try {
            String token = authorizationHeader.substring("Bearer".length()).trim();
            validateToken(token);
            
            Integer id = SessionMap.map.get(token);
            
            this.servletRequest.setAttribute("session_user_id", id);
            
        } catch (Exception e) {
            System.out.println("error in filter");
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        }
    }

    private void validateToken(String token) throws Exception {
// Validate the token
        Key key = KeyGeneratorFactory.getPRIVATEKEY();
        Claims claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody();
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        if (now.after(claims.getExpiration())) {
            throw new Exception();
        }
        
        if (!SessionMap.map.containsKey(token)) {
            throw new Exception();
        }
    }
}
