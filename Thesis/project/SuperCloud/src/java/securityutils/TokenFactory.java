
package securityutils;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.security.Key;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @author psilos
 */
public class TokenFactory {

//    private static final String ISSUER = "BATMAN";  // uriInfo.getAbsolutePath().toString()
    public static String issueToken(String username) {
        Key key = KeyGeneratorFactory.getPRIVATEKEY();

        long nowMillis = System.currentTimeMillis();
        long expMillis = nowMillis + 300000L;

        Date now = new Date(nowMillis);
        Date exp = new Date(expMillis);
        String id = String.valueOf(UUID.randomUUID());
        
        String jwtToken = Jwts.builder()
                .setIssuer("me")
                .setSubject(username)
                .setAudience("you")
                .setExpiration(exp)
                .setIssuedAt(now)
                .signWith(SignatureAlgorithm.HS512, key)
                .setId(id)
                .compact();

        return jwtToken;
    }
}
