package dto;

import entities.SchemaObject;
import java.util.ArrayList;
import java.util.List;
import model.SchemaModel;
import model.SchemaObjectModel;

public class SchemaObjectDTO {

    public static SchemaObjectModel toModel(SchemaObject entity) {
        SchemaObjectModel model = new SchemaObjectModel();
        if (entity.getId() != null) {
            model.setId(entity.getId());
        }
        if (entity.getObjectName() != null) {
            model.setObjectName(entity.getObjectName());
        }
        if (entity.getSchemaId() != null) {
            SchemaModel schemaModel;
            schemaModel = SchemaDTO.toModel(entity.getSchemaId());
            model.setSchemaModel(schemaModel);
        }
        return model;
    }

    public static List<SchemaObjectModel> toModel(List<SchemaObject> entites) {

        List<SchemaObjectModel> model = new ArrayList<>();
        for (SchemaObject o : entites) {
            SchemaObjectModel em;
            em = SchemaObjectDTO.toModel(o);
            model.add(em);
        }
        return model;

    }

}
