package dto;

import java.util.ArrayList;
import java.util.List;
import model.EntryModel;
import model.ProcessHelperModel;
import model.ProcessHepler;

public class ProcessHelperDTO {

    public static ProcessHelperModel toModel(ProcessHepler entity) {
        ProcessHelperModel model = new ProcessHelperModel();

        if (entity.getEntry() != null) {
            EntryModel entry = EntryDTO.toModel(entity.getEntry());
            model.setEntryModel(entry);
        }

        if(entity.isIsRunning() != null){
            model.setIsRunning(entity.isIsRunning());
        }

        return model;
    }

    public static List<ProcessHelperModel> toModel(List<ProcessHepler> entryes) {
        List<ProcessHelperModel> model = new ArrayList<>();
        for (ProcessHepler e : entryes) {
            ProcessHelperModel em = ProcessHelperDTO.toModel(e);
            model.add(em);
        }
        return model;
    }
}
