//package dto;
//
//import entities.Database;
//import entities.User;
//import entities.UserHasAccessToDatabase;
//import entities.UserHasAccessToDatabasePK;
//import model.UserHasAccessToDatabaseModel;
//
//public class UserHasAccessToDatabaseDTO {
//
//    public static UserHasAccessToDatabase toEntry(UserHasAccessToDatabaseModel model) {
//        UserHasAccessToDatabase entity = new UserHasAccessToDatabase();
//        //set Details;
//        if (model.getCanSelect() != null) {
//            entity.setCanSelect(model.getCanSelect());
//        }
//        if (model.getCanInsert() != null) {
//            entity.setCanInsert(model.getCanInsert());
//        }
//        if (model.getCanUpdate() != null) {
//            entity.setCanUpdate(model.getCanUpdate());
//        }
//        if (model.getCanAlert() != null) {
//            entity.setCanAlter(model.getCanAlert());
//        }
//        if (model.getCanDelete() != null) {
//            entity.setCanDelete(model.getCanDelete());
//        }
//        if (model.getCanDrop() != null) {
//            entity.setCanDrop(model.getCanDrop());
//        }
//        
//        if(model.getDatabaseModel() != null){
//            Database db = DatabaseDTO.toEntry(model.getDatabaseModel());
//            entity.setDatabase(db);
//        }
//        if(model.getUserModel() != null){
//            User user = UserDTO.toEntity(model.getUserModel());
//            entity.setUser(user);
//        }
//        
//        if(entity.getUserHasAccessToDatabasePK() == null){
//            UserHasAccessToDatabasePK pk = new UserHasAccessToDatabasePK();
//            entity.setUserHasAccessToDatabasePK(pk);
//        }
//        return entity;
//    }
//}
