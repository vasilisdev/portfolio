package dto;

import entities.ExecutionLog;
import java.util.ArrayList;
import java.util.List;
import model.EntryModel;
import model.ExecutionLogModel;

public class ExecutionLogDTO {

    public static ExecutionLogModel toModel(ExecutionLog entity) {

        ExecutionLogModel model = new ExecutionLogModel();
        model.setNowRunning(entity.getNowRunning());
        model.setRunAt(entity.getRunAt());
        model.setStderror(entity.getStderr());
        model.setStdout(entity.getStdout());
        model.setViewed(entity.getViewed());
        model.setId(entity.getId());
        model.setPid(entity.getPid());
        if (entity.getEntryId() != null) {
            EntryModel entry = EntryDTO.toModel(entity.getEntryId());
            model.setEntryModel(entry);
        }
        return model;
    }

    public static List<ExecutionLogModel> toModel(List<ExecutionLog> entryes) {
        List<ExecutionLogModel> model = new ArrayList<>();
        for (ExecutionLog e : entryes) {
            ExecutionLogModel em = ExecutionLogDTO.toModel(e);
            model.add(em);
        }
        return model;
    }

}
