package dto;

import entities.Entry;
import entities.User;
import java.util.ArrayList;
import java.util.List;
import model.EntryModel;
import model.UserModel;

public class EntryDTO {

    public static EntryModel toModel(Entry entry) {
        EntryModel model = new EntryModel();

        //Detail
        
        model.setId(entry.getId());
        model.setFullpath(entry.getFullpath());
        model.setIsDirectory(entry.getIsDirectory());
        model.setOtherCanRead(entry.getOthersCanRead());
        model.setOtherCanWrite(entry.getOthersCanWrite());
        model.setOtherCanExecute(entry.getOthersCanExecute());
//        model.setModifiedDate(entry.getModifiedDate());
//        model.setCreatedDate(entry.getCreatedDate());
        model.setIsDeleted(entry.getIsDeleted());
        model.setIsExecutable(entry.getIsExecutable());

        //Owner
        if (entry.getOwnerId() != null) {
            UserModel umodel = UserDTO.toModel(entry.getOwnerId());
            model.setOwnerId(umodel);
        }

//        if (entry.getTagList() != null) {
//            List<TagModel> tm = TagDTO.toModel(entry.getTagList());
//            model.setListTags(tm);
//        }
        //Parent
        if (entry.getParentId() != null) {
            model.setParetnId(entry.getParentId().getId());
        }

        return model;
    }

    public static List<EntryModel> toModel(List<Entry> entryes) {
        List<EntryModel> model = new ArrayList<>();
        for (Entry e : entryes) {
            EntryModel em = EntryDTO.toModel(e);
            model.add(em);
        }
        return model;
    }

    public static Entry toEntry(EntryModel model) {
        Entry entry = new Entry();
        //Details
        if(model.getId() != null){
            entry.setId(model.getId());
        }
        if (model.getFullpath() != null) {
            entry.setFullpath(model.getFullpath());
        }
        if (model.getIsDirectory() != null) {
            entry.setIsDirectory(model.getIsDirectory());
        }
        if (model.getOtherCanExecute() != null) {
            entry.setOthersCanExecute(model.getOtherCanExecute());
        }
        if (model.getOtherCanRead() != null) {
            entry.setOthersCanRead(model.getOtherCanRead());
        }
        if (model.getOtherCanWrite() != null) {
            entry.setOthersCanWrite(model.getOtherCanWrite());
        }
        if (model.getIsDeleted() != null) {
            entry.setIsDeleted(model.getIsDeleted());
        }
        if (model.getIsExecutable() != null) {
            entry.setIsExecutable(model.getIsExecutable());
        }
        if (model.getCreatedDate() != null) {
            entry.setCreatedDate(model.getCreatedDate());
        }
        if (model.getModifiedDate() != null) {
            entry.setModifiedDate(model.getCreatedDate());
        }
        //User
        if (model.getOwnerId() != null) {
            User user = new User();
            user.setId(model.getOwnerId().getId());
            entry.setOwnerId(user);
        }
        //Set Tag
//        if (model.getListTags() != null) {
//            List<Tag> tagList = TagDTO.toEntry(model.getListTags());
//            entry.setTagList(tagList);
//        }
        //Entry Parent
        if (model.getParetnId() != null) {
            Entry parentId = new Entry();
            parentId.setId(model.getParetnId());
            entry.setParentId(parentId);
        }
        return entry;
    }

}
