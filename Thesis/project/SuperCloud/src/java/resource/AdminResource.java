package resource;

import controllers.AdminController;
import dto.UserDTO;
import entities.User;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import model.UserModel;

@Path("admin")
public class AdminResource {

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response activate(UserModel model) {
        try {
            System.out.println("admin");
            System.out.println("Input   :"+model);
            User user = UserDTO.toEntity(model);
            User newUser = AdminController.activate(user);
            UserModel newMode = UserDTO.toModel(newUser);
            System.out.println("Output   :"+newMode);
            return Response.status(Status.OK).entity(newMode).build();
        } catch (Exception e) {
            return Response.status(Status.OK).entity(e.getMessage()).build();
        }
    }

}
