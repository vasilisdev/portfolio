package resource;

import controllers.AccountController;
import dto.UserDTO;
import exeption.UserNotFoundException;
import exeption.UserPasswordInvalidException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import mappers.UserWithTokenMapper;
import model.AuthenticationModel;
import model.UserWithTokenModel;

@Path("account")
public class AccountResource {

     //To do correct in error
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    @Path("/login")
    public Response login(AuthenticationModel model) {
        try {

            UserWithTokenMapper entity = AccountController.login(model);
            UserWithTokenModel usmodel = UserDTO.toModel(entity);

            System.out.println("Model " + usmodel);
            return Response.ok(usmodel, MediaType.APPLICATION_JSON).status(Status.OK).build();
        } catch (UserNotFoundException | UserPasswordInvalidException ex) {
            System.out.println(ex.getMessage());
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity(null).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(null).build();
        }
    }
}
