package resource;

import angularsession.SessionManager;
import controllers.filesystem.DirectoryController;
import controllers.filesystem.EntryController;
import controllers.filesystem.EntrySetAccessController;
import controllers.filesystem.FileController;
import dto.EntryDTO;
import dto.UserHasAccessDTO;
import entities.Entry;
import entities.User;
import entities.UserHasAccess;
import exeption.PermissionException;
import exeption.UserException;
import filesystem.FileManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import model.permission.AccessToEntryModel;
import model.EntryModel;
import model.EntryPremissionModel;
import model.MoveEntryModel;
import model.RenameModel;
import model.permission.RemoveAccessToEntryModel;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;

@Path("entry")
public class EntryResource {

//    @JWTTokenNeeded
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response create(EntryModel model) {

        try {
            int userId = angularsession.SessionManager.getOwnerID();
            System.out.println(model);
            Entry entry = EntryDTO.toEntry(model);
            DirectoryController.createBucket(userId, entry);
            EntryModel entryModel = EntryDTO.toModel(entry);
            return Response.status(Response.Status.CREATED).entity(entryModel).build();
        } catch (PermissionException | FileNotFoundException | UserException | DuplicateName ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        }
    }

    @POST
    @Path("directory")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createDirectory(EntryModel model) {

        try {
            int userId = angularsession.SessionManager.getOwnerID();
            System.out.println(model);
            Entry entry = EntryDTO.toEntry(model);
            DirectoryController.create(userId, entry);
            EntryModel entryModel = EntryDTO.toModel(entry);
            return Response.status(Response.Status.CREATED).entity(entryModel).build();
        } catch (PermissionException | FileNotFoundException | UserException | DuplicateName ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response remove(@PathParam("id") Integer id) {
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            System.out.println("ok boss");
            EntryController.delete(userId, id);
            return Response.status(Response.Status.OK).build();
        } catch (FileNotFoundException | PermissionException ex) {
            System.out.println("here");
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        } catch (IOException ex) {
            System.out.println("foo");
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        }
    }

    @GET
    @Path("{id}")
    public Response find(@PathParam("id") Integer id) {
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            Entry entry = EntryController.find(userId, id);
            EntryModel entryModel = EntryDTO.toModel(entry);

            return Response.status(Response.Status.CREATED).entity(entryModel).build();
        } catch (FileNotFoundException | PermissionException ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        } catch (IOException ex) {
            System.out.println("foo");
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        }
    }

    //upload file
    @POST
    @Path("file")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createFile(@FormDataParam("stream") InputStream in, @FormDataParam("model") FormDataBodyPart model) {
     
          

        try {
            int userId = SessionManager.getOwnerID();
            User user = new User();
            user.setId(userId);

            model.setMediaType(MediaType.APPLICATION_JSON_TYPE);
            EntryModel entryModel = model.getValueAs(EntryModel.class);
            Entry entity = EntryDTO.toEntry(entryModel);
            entity.setOwnerId(user);

            FileController.create(in, entity, userId);
            EntryModel newEnryModel = EntryDTO.toModel(entity);

            return Response.status(Response.Status.CREATED).entity(newEnryModel).build();
        } catch (PermissionException | UserException | DuplicateName | FileNotFoundException ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        } catch (IOException ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }

    }

    //move files and directory
    //rename
    @PUT
    @Path("rename")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response rename(RenameModel model) {

        try {
            int userId = angularsession.SessionManager.getOwnerID();
            Entry entry = EntryController.rename(userId, model.getFullpath(), model.getId());
            EntryModel entryModel = EntryDTO.toModel(entry);
            return Response.status(Response.Status.OK).entity(entryModel).build();
        } catch (FileNotFoundException | PermissionException | DuplicateName ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        } catch (IOException ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        }
    }

    @PUT
    @Path("move")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response move(MoveEntryModel model) {

        try {
            int user_id = SessionManager.getOwnerID();
            Entry entry = EntryController.move(user_id, model.getSourceId(), model.getDestinationId());
            EntryModel newEnryModel = EntryDTO.toModel(entry);
            return Response.status(Response.Status.OK).entity(newEnryModel).build();
        } catch (FileNotFoundException | PermissionException | DuplicateName ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        } catch (IOException ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        }

    }

    //copy
    @PUT
    @Path("copy")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response copy(MoveEntryModel model) {

        try {
            int user_id = SessionManager.getOwnerID();
            int src = model.getSourceId();
            int des = model.getDestinationId();
            Entry entry = EntryController.copy(user_id, src, des);

            EntryModel newEnryModel = EntryDTO.toModel(entry);
            return Response.status(Response.Status.OK).entity(newEnryModel).build();
        } catch (FileNotFoundException | PermissionException | DuplicateName ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        } catch (IOException ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        }

    }

    //find owner bucket
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response findBucket() {
        try {
            int ownerId = angularsession.SessionManager.getOwnerID();
            List<Entry> entres = DirectoryController.retreveBucket(ownerId);
            List<EntryModel> model = EntryDTO.toModel(entres);
            return Response.status(Response.Status.CREATED).entity(model).build();
        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
        }

    }

    @GET
    @Path("directory/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response directoryContent(@PathParam("id") Integer id) {
        try {
            int ownerId = angularsession.SessionManager.getOwnerID();
            List<Entry> entres = DirectoryController.retreveDirectory(ownerId, id);
            List<EntryModel> list = EntryDTO.toModel(entres);

            return Response.status(Response.Status.CREATED).entity(list).build();
        } catch (FileNotFoundException | PermissionException ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        }
    }

    @GET
    @Path("download/{id}")
    @Produces("application/zip")
    public Response download(@PathParam("id") Integer id) {
        ResponseBuilder response;
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            String filepath = EntryController.download(userId, id);
            File file = new File(filepath);
            String filename = FileManager.onlyFileNameFromAbsolutePath(filepath);

            response = Response.ok((Object) file);
            response.header("Content-Disposition", filename);
//            return Response.status(Response.Status.OK).header("Content-Disposition", "attachment; filename=" + filename).entity((Object) file).build();

        } catch (FileNotFoundException | PermissionException ex) {
            response = Response.status(Status.NOT_FOUND);
        } catch (IOException ex) {
            response = Response.status(Status.INTERNAL_SERVER_ERROR);
        }
        return response.build();
    }

    @GET
    @Path("raw/{id}")
    @Produces({MediaType.TEXT_PLAIN})
    public Response downloadRaw(@PathParam("id") Integer id) {
        ResponseBuilder response;
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            String filepath = EntryController.download(userId, id);
            File file = new File(filepath);
            String filename = FileManager.onlyFileNameFromAbsolutePath(filepath);

//            String text = new String(Files.readAllBytes(Paths.get("file")), StandardCharsets.UTF_8);
            response = Response.ok((Object) file);
            response.header("Content-Disposition", "attachment; filename=" + filename);

        } catch (FileNotFoundException | PermissionException ex) {
            response = Response.status(Status.NOT_FOUND);
        } catch (IOException ex) {
            response = Response.status(Status.INTERNAL_SERVER_ERROR);
        }
        return response.build();
    }

//    @PUT
//    @Path("premission")
//    @Consumes({MediaType.APPLICATION_JSON})
//    public String editPremissionForOthers(EntryModel model) {
//        String msg;
//        try {
//            int ownerId = SessionManager.getOwnerID();
//            Entry entity = EntryDTO.toEntry(model);
//            EntrySetAccessController.editPremissionForOthers(entity, ownerId);
//            msg = "ok";
//        } catch (FileNotFoundException | PermissionException ex) {
//            msg = ex.getMessage();
//        }
//        return msg;
//    }
    @PUT
    @Path("premission")
    @Consumes({MediaType.APPLICATION_JSON})
    public AccessToEntryModel editPremission(AccessToEntryModel model) {
        String msg;
        try {
            System.out.println(model);
            int ownerId = SessionManager.getOwnerID();
            UserHasAccess entity = UserHasAccessDTO.toEntry(model);
            EntrySetAccessController.editAccessPermission(entity, ownerId);

            AccessToEntryModel newModel = UserHasAccessDTO.toModel(entity);
            return newModel;

        } catch (Exception ex) {
            msg = ex.getMessage();
            return null;
        }

    }

    @POST
    @Path("premission")
    @Consumes({MediaType.APPLICATION_JSON})
    public AccessToEntryModel registerPremission(AccessToEntryModel model) {
        String msg;
        try {
            System.out.println(model);
            int ownerId = SessionManager.getOwnerID();
            UserHasAccess entity = UserHasAccessDTO.toEntry(model);
            EntrySetAccessController.addAccessPermission(entity, ownerId);
            AccessToEntryModel newModel = UserHasAccessDTO.toModel(entity);
            return newModel;
        } catch (Exception ex) {
            msg = ex.getMessage();
            return null;
        }

    }

    @PUT
    @Path("premission/delete")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response unregisterPremission(RemoveAccessToEntryModel model) {

        System.out.println(model);

        try {
            int userId = SessionManager.getOwnerID();
            EntrySetAccessController.removeAccess(model, userId);
            return Response.status(Response.Status.OK).entity(model).build();
        } catch (PermissionException | FileNotFoundException | UserException ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        }
    }

    @GET
    @Path("premission/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public Response entryPremission(@PathParam("id") Integer id) {
        try {
            int userId = SessionManager.getOwnerID();
            EntryPremissionModel model = EntrySetAccessController.getAllPremissionToSpecifcUser(id, userId);
            return Response.status(Response.Status.OK).entity(model).build();
        } catch (FileNotFoundException ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        } catch (PermissionException ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }
}

//    @GET
//    @Produces({MediaType.APPLICATION_JSON})
//    @Path("bucket/shared")
//    public List<EntryModel> retreveAllPublicAndAccessedBucket() {
//        int ownerId = angularsession.SessionManager.getOwnerID();
//        List<Entry> entres = DirectoryController.retreveAllPublicAndAccessedBucket(ownerId);
//        List<EntryModel> model = EntryDTO.toModel(entres);
//        return model;
//    }
