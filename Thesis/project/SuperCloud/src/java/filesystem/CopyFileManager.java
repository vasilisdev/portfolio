package filesystem;

import dao.EntryDAOImpl;
import entities.Entry;
import entities.Tag;
import entities.User;
import exeption.PermissionException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;

public class CopyFileManager {


    public static LinkedHashMap<String, ArrayList<Entry>> copy(Entry source, Entry destination, EntryDAOImpl entryDAOImpl, User userEntity) throws IOException, PermissionException {

        LinkedHashMap<String, ArrayList<Entry>> map = new LinkedHashMap<>();
        ArrayList<Entry> entryList = new ArrayList<>();

        String fullPathDes = FileManager.appandSlash(destination.getFullpath()) + FileManager.onlyFileNameFromAbsolutePath(source.getFullpath());
        Entry en = copyEntry(source, fullPathDes, userEntity); //copy from src to new Entry that will insert
        entryList.add(en);
        map.put(destination.getFullpath(), entryList);

        if (source.getIsDirectory()) {
            copyDirectory(source, fullPathDes, entryDAOImpl, userEntity, map);
        }
        if (!source.getIsDirectory()) {
            copyFile(source, fullPathDes);
        }
        return map;

    }

    private static void copyDirectory(Entry source, String destination, EntryDAOImpl entryDAOImpl, User userEntity, Map<String, ArrayList<Entry>> map) throws IOException {

//        System.out.println(destination);
        if (source.getIsDirectory()) {
            File file = new File(destination);
            file.mkdir();
        }

        if ((!PermissionManager.canViewDirectory(source, userEntity.getId()))) {
            return;
        }

        List<Entry> children = entryDAOImpl.findchildsWithTags(source.getId());
        if ((children.isEmpty())) {
            return;
        }

        for (Entry entry : children) {
            if (PermissionManager.canRead(entry, userEntity.getId())) {

                String fullPathChild = FileManager.appandSlash(destination) + FileManager.onlyFileNameFromAbsolutePath(entry.getFullpath());
                Entry en = copyEntry(entry, fullPathChild, userEntity); //copy entry from Db

                //add ti map
                if (map.containsKey(destination)) {
                    map.get(destination).add(en);
                } else {
                    ArrayList<Entry> entryList = new ArrayList<>();
                    entryList.add(en);
                    map.put(destination, entryList);
                }

                if (entry.getIsDirectory()) {
                    copyDirectory(entry, fullPathChild, entryDAOImpl, userEntity, map);
                }
                if (!entry.getIsDirectory()) {
                    //add to map
                    copyFile(entry, fullPathChild);
                }
            }
        }
    }

    private static void copyFile(Entry source, String fullPathDes) throws IOException {
        //src path
        File src = new File(source.getFullpath());
        //new File path
        File des = new File(fullPathDes);
        FileUtils.copyFile(src, des);
    }

    private static Entry copyEntry(Entry source, String fullPath, User userEntry) {
        Entry entry = new Entry();
        List<Tag> tagList = new ArrayList<>();

        entry.setCreatedDate(new Date());
        entry.setModifiedDate(new Date());
        entry.setOwnerId(userEntry);
        entry.setIsExecutable(source.getIsExecutable());
        entry.setIsDirectory(source.getIsDirectory());
        entry.setIsDeleted(source.getIsDeleted());
        entry.setFullpath(fullPath);

        //remove premission
        entry.setOthersCanExecute(false);
        entry.setOthersCanRead(false);
        entry.setOthersCanWrite(false);

        //add tag
        tagList.addAll(source.getTagList());
        entry.setTagList(tagList);
        return entry;
    }
}
