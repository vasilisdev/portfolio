package filesystem;

import entities.Entry;
import entities.UserHasAccess;

public class PermissionManager {
    // TODO: UserHasAccess permissions override entry permissions

    // ---------------------------------------------------------------------
    //                    EFFECTIVE PERMISSIONS
    // ---------------------------------------------------------------------
    public static boolean canWriteAndCanExecute(Entry parentDirectory, int user_id) {
        if (parentDirectory == null) {
            return false;
        }
        if (parentDirectory.getOwnerId().getId() == user_id && parentDirectory.getIsDeleted() == false) {
            return true;
        }
        if (parentDirectory.getOthersCanExecute() && parentDirectory.getOthersCanWrite() && parentDirectory.getIsDeleted() == false) {
            return true;
        }
        for (UserHasAccess p : parentDirectory.getUserHasAccessList()) {
            if (p.getUser().getId() == user_id && p.getCanExecute() && p.getCanWrite()) {
                return true;
            }
        }
        return false;
    }

    //premission in parent
    public static boolean canViewDirectory(Entry entry, int userId) {
        if (entry == null) {
            return false;
        }
        if (entry.getOwnerId().getId() == userId && entry.getIsDeleted() == false && entry.getIsDirectory() == true) {
            return true;
        }

        for (UserHasAccess us : entry.getUserHasAccessList()) {
            if (us.getUser().getId() == userId && !entry.getIsDeleted() && entry.getIsDirectory()) { //an prepei na figi to get is directouy
                return us.getCanRead() && us.getCanExecute();
            }
        }

        if (entry.getOthersCanExecute() && entry.getOthersCanRead() && entry.getIsDeleted() == false && entry.getIsDirectory() == true) {
            return true;
        }

        return false;
    }

    //access tou owner main directory
    public static boolean canOwnerViewEntry(Entry entry, int user_id) {
        if (entry == null) {
            return false;
        }
        if (entry.getOwnerId().getId() == user_id && entry.getIsDeleted() == false) {
            return true;
        }
        return false;
    }

    public static boolean canMoveEntry(Entry parentDirectory_from, Entry parentDirectory_to, int user_id) {
        if (parentDirectory_from == null || parentDirectory_to == null) {
            return false;
        }
        if (!canWriteAndCanExecute(parentDirectory_from, user_id)) {
            return false;
        }
        if (!canWriteAndCanExecute(parentDirectory_to, user_id)) {
            return false;
        }
        return true;
    }

    public static boolean canCopyEntry(Entry from, Entry parentDirectory_from, Entry parentDirectory_to, int user_id) {
        if (parentDirectory_from == null || parentDirectory_to == null) {
            return false;
        }
        if (!canViewDirectory(parentDirectory_from, user_id)) {     // source parent r+x
            return false;
        }
        if (!canWriteAndCanExecute(parentDirectory_to, user_id)) { // destination parent w+x
            return false;
        }
        if (!canRead(from, user_id)) {                             // source entry r
            return false;
        }
        return true;
    }

    public static boolean canRenameEntry(Entry parentDirectory_from, int user_id) {
        if (parentDirectory_from == null) {
            return false;
        }
        if (!canWriteAndCanExecute(parentDirectory_from, user_id)) {
            return false;
        }
        return true;
    }

    public static boolean canDeleteEntry(Entry parentDirectory, int user_id) {
        if (parentDirectory == null) {
            return false;
        }
        if (!canWriteAndCanExecute(parentDirectory, user_id)) {
            return false;
        }
        return true;
    }

    public static boolean canRead(Entry entry, int userId) {
        if (entry == null) {
            return false;
        }
        if (entry.getOwnerId().getId() == userId && entry.getIsDeleted() == false) {
            return true;
        }
        for (UserHasAccess us : entry.getUserHasAccessList()) {
            if (us.getUser().getId() == userId) {
                if (us.getCanRead() && entry.getIsDeleted() == false) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        if (entry.getOthersCanRead() == true && entry.getIsDeleted() == false) {
            return true;
        }
        return false;
    }

    public static boolean canExecute(Entry entry, int userId) {
        if (entry == null) {
            return false;
        }
        if (entry.getOwnerId().getId() == userId && entry.getIsDeleted() == false) {
            return true;
        }
        for (UserHasAccess us : entry.getUserHasAccessList()) {
            if (us.getUser().getId() == userId) {
                if (us.getCanExecute() && entry.getIsDeleted() == false) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        if (entry.getOthersCanExecute() == true && entry.getIsDeleted() == false) {
            return true;
        }
        return false;
    }

    public static boolean canReadAndExecute(Entry entry, int userId) {

        return canRead(entry, userId) && canExecute(entry, userId);
    }

}
