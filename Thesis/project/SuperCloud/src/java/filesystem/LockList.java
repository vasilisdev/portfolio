package filesystem;

import java.util.ArrayList;



public class LockList extends ArrayList<LockEntry> {
    public boolean contains(LockEntry what) {
        for (LockEntry le : this) {
            if (le == what) {
                return true;
            }
        }
        return false;
    }
    public boolean contains(int entry_id) {
        for (LockEntry le : this) {
            if (le.getEntry_id() == entry_id) {
                return true;
            }
        }
        return false;
    }
    
     public LockEntry find(int entry_id) {
        for (LockEntry le : this) {
            if (le.getEntry_id() == entry_id) {
                return le;
            }
        }
        return null;
    }
}
