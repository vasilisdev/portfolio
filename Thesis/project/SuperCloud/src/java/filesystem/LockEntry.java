package filesystem;


public class LockEntry {
    private int owner_id;
    private int entry_id;
    private int counter = 0;

    public LockEntry(int owner_id, int entry_id) {
        this.owner_id = owner_id;
        this.entry_id = entry_id;
    }

    public int getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(int owner_id) {
        this.owner_id = owner_id;
    }

    public int getEntry_id() {
        return entry_id;
    }

    public void setEntry_id(int entry_id) {
        this.entry_id = entry_id;
    }
    
    public void increase() {
        counter++;
    }
    
    public void descrease() {
        counter--;
    }

    public int getCounter() {
        return counter;
    }
}
