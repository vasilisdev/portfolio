package dao;

import entities.UserHasAccessToObject;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class UserHasAccessToObjectImpl extends GenericDAO<UserHasAccessToObject> {

    public UserHasAccessToObjectImpl(EntityManager em) {
        super(em, UserHasAccessToObject.class);
    }

    public List<UserHasAccessToObject> findUserHasAccessToObjectByObjectId(int schemaObjectsId) {
        Query query = em.createNamedQuery("UserHasAccessToObject.findBySchemaObjectsId").setParameter("schemaObjectsId", schemaObjectsId);
        return query.getResultList();
    }

    public List<UserHasAccessToObject> findAccessBySchemaObjectAndUser(int schemaObjectsId, int userId) {
        Query query = em.createQuery("select us from UserHasAccessToObject us \n"
                + "where  us.userHasAccessToObjectPK.schemaObjectsId =:schemaObjectsId \n"
                + "and  us.userHasAccessToObjectPK.userId =:userId")
                .setParameter("schemaObjectsId", schemaObjectsId)
                .setParameter("userId", userId);
        return query.getResultList();
    }

}
