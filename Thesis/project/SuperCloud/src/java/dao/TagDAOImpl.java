package dao;

import entities.Tag;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class TagDAOImpl extends GenericDAO<Tag>  {
     public TagDAOImpl(EntityManager em) {
        super(em, Tag.class);
    }
     
     public List<Tag> findTagByTagNameForSchema(String tagname){
        List<Tag> list;
        Query query = em.createQuery("select distinct t from Tag t left join fetch t.schemaList d where t.tagname = :tagname").setParameter("tagname", tagname);
        list = query.getResultList();
        return list;    
     }
     
     public List<Tag> findTagByNameForEntry(String tagname){
         List<Tag>list;
         Query query = em.createQuery("select distinct t from Tag t left join fetch t.entryList e where t.tagname = :tagname").setParameter("tagname", tagname);
         list = query.getResultList();
         return list;
     }
}
