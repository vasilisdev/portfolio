package dao;

import entities.Schema;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class SchemaDAOImpl extends GenericDAO<Schema> {

    public SchemaDAOImpl(EntityManager em) {
        super(em, Schema.class);
    }

    public List<Schema> findSchemaForSetAccess(int id) {
        Query query = em.createQuery("select distinct s from Schema s \n"
                + "join s.userId u \n"
                + "left join fetch UserHasAccessToSchema us \n"
                + "on us.userHasAccessToSchemaPK.schemaId = s.id \n"
                + "where s.id =:id").setParameter("id", id);
        return query.getResultList();
    }

    public List<Schema> findSchemaByName(String schemaName) {
        Query query = em.createNamedQuery("Schema.findBySchemaName").setParameter("schemaName", schemaName);
        return query.getResultList();
    }

    public List<Schema> findSchemaAll(String schemaName) {
        Query query = em.createQuery("select distinct s from Schema s "
                + "left join s.userHasAccessToSchemaList us "
                + "left join s.userId u "
                + "where s.schemaName =:schemaName").setParameter("schemaName", schemaName);
        return query.getResultList();
    }

    public List<Schema> findSchemaAll() {
        Query query = em.createQuery("select distinct s from Schema s "
                + "left join fetch s.userHasAccessToSchemaList us "
                + "left join s.userId u ");
        return query.getResultList();
    }
    
 
}
