package dao;

import entities.Entry;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class EntryDAOImpl extends GenericDAO<Entry> {

    public EntryDAOImpl(EntityManager em) {
        super(em, Entry.class);
    }

    public List<Entry> findEntryWithPermissions(int id) {
        Query query = em.createQuery("select distinct e from Entry e join fetch e.ownerId o\n"
                + "left join fetch e.userHasAccessList us \n"
                + "where e.id =:id").setParameter("id", id);
        return query.getResultList();
    }

    public List<Entry> findEntryWithTag(int id) {
        Query query = em.createQuery("select distinct e from Entry e left join fetch e.tagList t where e.id =:id").setParameter("id", id);
        return query.getResultList();
    }

    public List<Entry> findEntryForSetAccess(int id, int ownerId) {
        List<Entry> entrys;
        Query query = em.createQuery("select distinct e from Entry e \n"
                + "join e.ownerId o \n"
                + "left join fetch e.userHasAccessList us \n"
                + "where e.id =:id and o.id =:ownerId").setParameter("id", id).setParameter("ownerId", ownerId);
        entrys = query.getResultList();
        return entrys;
    }

    public List<Entry> findEntryByIdAndTags(int id) {
        List<Entry> entres;
        Query query = em.createQuery("select distinct e from Entry e left join e.executable as ex left join fetch e.tagList where ex is not null and e.id = :id").setParameter("id", id);
        entres = query.getResultList();
        return entres;
    }

    public List<Entry> findMainDirectores() {
        Query query = em.createQuery("select distinct e from Entry e left join fetch e.parentId p where p.id is null");
        return query.getResultList();
    }

    public List<Entry> findAllPublicAndAccessedBucket() {
        Query query = em.createQuery("select distinct e from Entry e\n"
                + "left join e.parentId p \n"
                + "join e.ownerId ownerParentDirectory \n"
                + "left join fetch e.entryList listEntry \n"
                + "left join listEntry.ownerId listOwner \n"
                + "left join fetch listEntry.userHasAccessList  listEntryUserHasAccessList \n"
                + "where p is null \n");
        return query.getResultList();
    }

    public List<Entry> findEntryForSetVisability(int id, int ownerId) {
        List<Entry> entres;
        Query query = em.createQuery("Select e from Entry e \n"
                + "join e.ownerId owner\n"
                + "where owner.id =:ownerId\n"
                + "and e.id =:id\n"
                + "and e.isDeleted = false")
                .setParameter("id", id)
                .setParameter("ownerId", ownerId);
        entres = query.getResultList();
        return entres;
    }

    public List<Entry> findifHadAccessToInsert(int parentId, int userId) {
        List<Entry> entres;
        // add if owner ...
        Query query = em.createQuery("select e from Entry e\n"
                + "join fetch e.ownerId o\n"
                + "left join fetch e.userHasAccessList us left join User u on (us.userHasAccessPK.userId = u.id)\n"
                + "where (\n"
                + "(\n"
                + "  (us.userHasAccessPK.userId =:userId\n"
                + "   and us.userHasAccessPK.entryId =:entryId\n"
                + "   and us.canWrite = true)\n"
                + "   or(o.id =:userId and e.id =:entryId)\n"
                + "   or(e.id =:entryId and e.othersCanWrite = true)) \n"
                + "   and e.isDeleted = false\n"
                + ")")
                .setParameter("entryId", parentId)
                .setParameter("userId", userId);

        entres = query.getResultList();
        return entres;
    }

    public List<Entry> findEntryDetails(int id) {
        Query query = em.createQuery("select distinct e from Entry e \n"
                + "left join fetch e.ownerId owner \n"
                + "left join fetch e.userHasAccessList userlist \n"
                + "left join e.entryList entrylist \n"
                //                + "join fetch entrylist.parentId entrylistparent \n"
                + "left join entrylist.ownerId ownerlist \n"
                + "left join fetch entrylist.userHasAccessList entrylistuserlist \n"
                + "left join fetch entrylist.tagList entrylisttaglist \n"
                + "where e.id =:id")
                .setParameter("id", id);
        return query.getResultList();
    }

    public List<Entry> findchilds(int id) {
        Query query = em.createQuery("select distinct e from Entry e left join fetch e.parentId p where p.id =:id")
                .setParameter("id", id);
        return query.getResultList();
    }

    public List<Entry> findchildsWithTags(int id) {
        Query query = em.createQuery("select distinct e from Entry e left join fetch e.parentId p left join fetch e.tagList t where p.id =:id")
                .setParameter("id", id);
        return query.getResultList();
    }

    public List<Entry> findEntryWithAccess(int id) {
        Query query = em.createQuery("select distinct e from Entry e\n"
                + "join e.ownerId o \n"
                + "left join fetch UserHasAccess us \n"
                + "on us.userHasAccessPK.entryId = e.id \n"
                + "where e.id =:id").setParameter("id", id);
        return query.getResultList();
    }

    public List<Entry> findByFullpath(String fullpath) {
        Query query = em.createNamedQuery("Entry.findByFullpath", Entry.class).setParameter("fullpath", fullpath);
        return query.getResultList();
    }

    public List<Entry> findAllProcessesWithThereAccess() {
        Query query = em.createQuery("select distinct e from Entry e \n"
                + "left join fetch  e.ownerId o \n"
                + "left join fetch  e.userHasAccessList us \n"
                + "where e.isExecutable =:value").setParameter("value", true);
        return query.getResultList();
    }
}
