package dao;

import entities.UserHasAccess;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class UserHasAccessDAOImpl extends GenericDAO<UserHasAccess> {

    public UserHasAccessDAOImpl(EntityManager em) {
        super(em, UserHasAccess.class);
    }

    public List<UserHasAccess> findByUserid(int id) {
        List<UserHasAccess> list;
        Query query = em.createQuery("select us from UserHasAccess us where us.userHasAccessPK.userId =:id")
                .setParameter("id", id);
        list = query.getResultList();
        return list;
    }

    public List<UserHasAccess> findPremission(int entryId, int userId) {
        Query query = em.createQuery("select distinct us from UserHasAccess us \n"
                + "where us.userHasAccessPK.entryId =:entryId \n"
                + "and us.userHasAccessPK.userId =:userId")
                .setParameter("entryId", entryId)
                .setParameter("userId", userId);
        return query.getResultList();
    }

    public List<UserHasAccess> findEntryPremission(int entryId) {
        Query query = em.createQuery("select distinct us from UserHasAccess us \n"
                + "where  us.userHasAccessPK.entryId =:entryId \n")
                .setParameter("entryId", entryId);
        return query.getResultList();
    }

    public int removePermmissionAccess(int entryId, int userId) {
        Query query = em.createQuery("delete from UserHasAccess us\n"
                + "where\n"
                + "us.userHasAccessPK.entryId =:entryId\n"
                + "and us.userHasAccessPK.userId =:userId")
                .setParameter("entryId", entryId)
                .setParameter("userId", userId);
        return query.executeUpdate();
    }
}
