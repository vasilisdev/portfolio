package dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public abstract class GenericDAO<T> {

    protected final Class<T> persistentClass;        
    protected final EntityManager em;

    public GenericDAO(EntityManager em, Class<T> persistencClass) {
        this.persistentClass = persistencClass;
        this.em = em;
    }

    public List<T> find(int id) {
        List<T> list;
        Query query = em.createQuery("select e from " + persistentClass.getSimpleName() + " e" + " where e.id =:id").setParameter("id", id);
        list = query.getResultList();
        return list;
    }

    public List<T> findAll() {
        List<T> list;
        Query query = em.createQuery("select e from " + persistentClass.getSimpleName() + " e");
        list = query.getResultList();
        return list;
    }

    public void create(T entity) {
        em.persist(entity);
    }

    public void edit(T entity) {
        em.merge(entity);
    }

    public void remove(T entity) {
        em.remove(em.merge(entity));
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(persistentClass));
        javax.persistence.Query q = em.createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {        
        Query query = em.createQuery("select e from " + persistentClass.getSimpleName() + " e");
        List<T> list = query.getResultList();
        return list.size();
    }
}
