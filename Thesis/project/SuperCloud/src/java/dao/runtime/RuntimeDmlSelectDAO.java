package dao.runtime;

import database.MySQL_QuerySelectHelper;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import model.runtime.dml.SelectModel;
import model.select.ElementList;
import model.select.SelectAllReturnModel;
import model.select.TableElement;
import model.select.primaryKey.SelectPrimaryKeyList;
import utils.Element;
import utils.SelectForeignKey;
import utils.SelectTableForeignKeys;
import utils.SelectTableUniqueKeys;
import utils.SelectUniqueKey;

public class RuntimeDmlSelectDAO {

    protected final String schemaName;
    protected final String tableName;
    protected final EntityManager em;

    public RuntimeDmlSelectDAO(String schemaName, String tableName, EntityManager em) {
        this.schemaName = schemaName;
        this.tableName = tableName;
        this.em = em;
    }

    //------------------------SELECT STRUCTOR-----------------------------------
    //To do extra only auto_increment
    public SelectAllReturnModel findTableStructure(SelectModel model) {
        SelectAllReturnModel selectReturnModel = new SelectAllReturnModel();
        List<String> columnsName = columnName();
        String sql = MySQL_QuerySelectHelper.TABLE_STRUCTURE;
        System.out.println(sql);
        Query query = em.createNativeQuery(sql).setParameter(1, model.getTableName());
        List<ElementList> listOfRows = rowsOfTable(query, columnsName.size());

        selectReturnModel.setColumnsName(columnsName);
        selectReturnModel.setRows(listOfRows);
        return selectReturnModel;
    }

    private List<String> columnName() {
        List<String> list = new ArrayList<>();
        list.add("Position");
        list.add("Column Name");
        list.add("Column Type");
        list.add("Is NUll");
        list.add("Column Key");
        list.add("Extra");
        list.add("Default");
        return list;
    }

    //-------------------------SELECT ALL IN A TABLE----------------------------
    public SelectAllReturnModel findTableContent(SelectModel model) {

        SelectAllReturnModel selectReturnModel = new SelectAllReturnModel();
        String findTableContent = MySQL_QuerySelectHelper.SELECT_ALL;

        String sql = String.format(findTableContent, model.getSchemaName(), model.getTableName());
        System.out.println(sql);
        Query query = em.createNativeQuery(sql);

        List<String> columnsName = columnsNameList(model);
        List<ElementList> listOfRows = rowsOfTable(query, columnsName.size());

        selectReturnModel.setColumnsName(columnsName);
        selectReturnModel.setRows(listOfRows);

        return selectReturnModel;
    }

    private List<String> columnsNameList(SelectModel model) {
        String show = MySQL_QuerySelectHelper.SHOW_COLUMN_NAME;
        Query q = em.createNativeQuery(show).setParameter(1, model.getTableName()).setParameter(2, model.getSchemaName());
        List<String> list = q.getResultList();
        return list;
    }

    private List<ElementList> rowsOfTable(Query query, int columncount) {
        // Change List<List<String>>  to List<List<ElementModel>> 
        List<ElementList> table = new ArrayList<>();
        List<Object[]> list = query.getResultList();

        for (Object[] rowObject : list) {
            ElementList rowlist = new ElementList();
            rowlist.setElements(new ArrayList<>());

            for (int i = 0; i < columncount; i++) {
                TableElement te = new TableElement();

                if (i < rowObject.length && rowObject[i] != null) { // not null
                    te.setIsNull(false);
                    te.setValue(String.valueOf(rowObject[i]));
                } else { // null
                    te.setIsNull(true);
                    te.setValue(null);
                }
                rowlist.getElements().add(te);

            }
            table.add(rowlist);
        }

        return table;
    }

    //---------------Select AllUnique key --------------------------------------
    public SelectTableUniqueKeys findAllUniqueKey(SelectModel model) {

        SelectTableUniqueKeys selectTableUniquesKey = new SelectTableUniqueKeys();
        List<SelectUniqueKey> uniqueKey = new ArrayList<>();

        List<String> constraintsName = findUniqueConstrains(model);

        System.out.println(constraintsName);

        for (String s : constraintsName) {
            SelectUniqueKey selectUniqueKey = uniqueKey(s , model);
            uniqueKey.add(selectUniqueKey);
        }

        selectTableUniquesKey.setUniqueKey(uniqueKey);

        return selectTableUniquesKey;
    }

    private SelectUniqueKey uniqueKey(String constraintName, SelectModel model) {
        SelectUniqueKey selectUniqueKey = new SelectUniqueKey();
        List<String> columnsName = findConstrainsColumnsName(constraintName,model);
    

//        for (String s : columnsName) {
//            System.out.println(s);
//            columns.add(s);
//        }
        selectUniqueKey.setUniqueKeyName(constraintName);
        selectUniqueKey.setColumnsName(columnsName);
        
        return selectUniqueKey;
    }

    private List<String> findUniqueConstrains(SelectModel model) {
        String sql = MySQL_QuerySelectHelper.FIND_UNIQUE_CONSTRAINS;
        Query query = em.createNativeQuery(sql).setParameter(1, model.getTableName());       
        return query.getResultList();
    }

    private List<String> findConstrainsColumnsName(String constrainsName , SelectModel model) {
        String sql = MySQL_QuerySelectHelper.UNIQUE_COLUMNS;
        Query query = em.createNativeQuery(sql).setParameter(1, constrainsName).setParameter(2, model.getSchemaName());
        return query.getResultList();
    }

    //--------------Select Primary Key -----------------------------------------
    public SelectPrimaryKeyList findPrimaryKey(SelectModel model) {
     
        SelectPrimaryKeyList selectPrimaryKeyList = new SelectPrimaryKeyList();

        List<String> list = findPrimaryKeysName(model);
        selectPrimaryKeyList.setListOfColumnName(list);
       
        return selectPrimaryKeyList;
    }

    private List<String> findPrimaryKeysName(SelectModel model) {
        String sql = MySQL_QuerySelectHelper.FIND_PRIMARY_KEY_COLUMNS_NAME;
        Query query = em.createNativeQuery(sql).setParameter(1, model.getTableName());
        return query.getResultList();
    }

    //-----------------find foreign keys----------------------------------------
    public SelectTableForeignKeys findAllForeignKeys(SelectModel model) {
        SelectTableForeignKeys selectTableForeignKeys = new SelectTableForeignKeys();
        List<SelectForeignKey> listSelectForeignKey = new ArrayList<>();
        List<String> listOfConstrainsName = findAllForeignKeysConstrains(model);

        for (String s : listOfConstrainsName) {
            SelectForeignKey selectForeignKey = findForeignKey(model, s);
            listSelectForeignKey.add(selectForeignKey);
        }

        selectTableForeignKeys.setForeignKeysList(listSelectForeignKey);
        return selectTableForeignKeys;
    }

    private SelectForeignKey findForeignKey(SelectModel model, String constrain) {
        SelectForeignKey selectForeignKey = new SelectForeignKey();
        List<Element> elements = findForeignKeyElement(model, constrain);
        selectForeignKey.setConstrainName(constrain);
        selectForeignKey.setElements(elements);
        return selectForeignKey;
    }

    private List<Element> findForeignKeyElement(SelectModel model, String constrain) {
        List<Element> elements = new ArrayList<>();
        List<Object[]> list = findForeignKeyCaracteristics(model, constrain);

        for (Object[] rowObject : list) {
            Element element = new Element();
            element.setColumnName(rowObject[0].toString());
            element.setRefererencedTable(rowObject[1].toString());
            element.setReferencedColumn(rowObject[2].toString());
            elements.add(element);

        }
        return elements;
    }

    private List<String> findAllForeignKeysConstrains(SelectModel model) {
        String sql = MySQL_QuerySelectHelper.FIND_FOREIGN_KEYS_CONSTRAINS;
        Query query = em.createNativeQuery(sql).setParameter(1, model.getSchemaName()).setParameter(2, model.getTableName());
        return query.getResultList();
    }

    private List<Object[]> findForeignKeyCaracteristics(SelectModel model, String constrain) {
        String sql = MySQL_QuerySelectHelper.FIND_FOREIGN_KEY_CARACTERISTICS;
        Query query = em.createNativeQuery(sql).setParameter(1, model.getSchemaName()).setParameter(2, model.getTableName()).setParameter(3, constrain);
        return query.getResultList();
    }

}

