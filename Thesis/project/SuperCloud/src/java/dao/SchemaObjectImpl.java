package dao;

import entities.SchemaObject;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class SchemaObjectImpl extends GenericDAO<SchemaObject> {
    
    public SchemaObjectImpl(EntityManager em) {
        super(em, SchemaObject.class);
    }
    
    public List<SchemaObject> findTabels(int id) {
        List<SchemaObject> list;
        Query query = em.createQuery("select d from DatabaseTables d join d.databaseId db where db.id =:id").setParameter("id", id);
        list = query.getResultList();
        return list;
    }
    
    public List<SchemaObject> findTabel(int dbId, int tableId) {
        List<SchemaObject> list;
        Query query = em.createQuery("select dt from DatabaseTables dt join dt.databaseId d where d.id =:dbId and dt.id =:tableId").setParameter("dbId", dbId).setParameter("tableId", tableId);
        list = query.getResultList();
        return list;
    }
    
    public List<SchemaObject> findAllSchemaObjectBySchemaId(int id) {
        Query query = em.createQuery("select distinct o from SchemaObject o join o.schemaId s where s.id =:id").setParameter("id", id);
        return query.getResultList();
    }
    
    public List<SchemaObject> findSchemaObjectToSetAccess(int id) {
        Query query = em.createQuery("select distinct s from SchemaObject s \n"
                + "left join fetch s.schemaId sc \n"
                + "left join UserHasAccessToObject us \n"
                + "on us.userHasAccessToObjectPK.schemaObjectsId = s.id \n"
                + "where s.id =:id").setParameter("id", id);
        return query.getResultList();
    }
    
//    public List<SchemaObject> findSchemaObjectWithPremissions(int id) {
//        Query query = em.createQuery("select distinct obj from SchemaObject obj \n"
//                + "join fetch obj.schemaId s \n"
//                + "join fetch s.userId u \n"
//                + "left join fetch obj.userHasAccessToObjectList usl\n"
//                + "where s.id =:id").setParameter("id", id);
//        return query.getResultList();
//    }
    
   
}
