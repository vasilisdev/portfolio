package dao;

import java.util.List;
import entities.User;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class UserDAOImpl extends GenericDAO<User> {
    
    public UserDAOImpl(EntityManager em) {
        super(em, User.class);
    }
    
    public List<User> findUserByUserName(String username) {
        Query query = em.createNamedQuery("User.findByUsername").setParameter("username", username);
        return query.getResultList();
    }
    
    public List<User> findUserByEmail(String email) {
        Query query = em.createNamedQuery("User.findByEmail").setParameter("email", email);
        return query.getResultList();
    }
    
    public List<User> findUserForSetAccessToEntry(int id) {
        List<User> users;
        Query query = em.createQuery("select distinct u from User u left join fetch u.userHasAccessList us where u.id =:id").setParameter("id", id);
        users = query.getResultList();
        return users;
    }
    
    public List<User> findUserForAccessToDatabase(int id) {
        Query query = em.createQuery("select distinct u from User u \n"
                + "left join fetch UserHasAccessToSchema  us \n"
                + "on us.userHasAccessToSchemaPK.userId = u.id \n"
                + "where u.id =:id").setParameter("id", id);
        return query.getResultList();
    }
    
    public List<User> findUserForAccessToObject(int id) {
        Query query = em.createQuery("select distinct u from User u \n"
                + "left join fetch UserHasAccessToObject us \n"
                + "on us.userHasAccessToObjectPK.userId = u.id \n"
                + "where u.id =:id").setParameter("id", id);
        return query.getResultList();
    }
    
    public List<User> findAllUserActiveAndInactive() {
        List<User> users;
        Query query = em.createQuery("Select u from User u");
        users = query.getResultList();
        return users;
    }
    
    public List<User> findUserWithEmailAndUsername(User user) {
        Query query = em.createQuery("Select u from User u where u.username = :username or u.email = :email").setParameter("username", user.getUsername()).setParameter("email", user.getPassword());
        return query.getResultList();
    }
    
}
