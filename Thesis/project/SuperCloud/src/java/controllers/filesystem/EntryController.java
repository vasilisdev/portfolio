package controllers.filesystem;

import dao.EntryDAOImpl;
import dao.UserDAOImpl;
import entities.Entry;
import entities.User;
import exeption.PermissionException;
import filesystem.FileManager;
import filesystem.PermissionManager;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jpautils.EntityManagerTransaction;
import jpautils.EntityManagerTransactionless;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import filesystem.LockEntry;
import filesystem.LockEntryPair;
import filesystem.LockManager;
import java.util.LinkedHashMap;

public class EntryController {

    //--------------------------Move Entry--------------------------------------
    public static Entry move(int user_id, Integer sourceId, Integer destinationId) throws IOException, PermissionException, FileNotFoundException, DuplicateName {
        LockEntryPair lep = null;

        try {
            //hear validation input
//            lep = LockManager.LOCK_MANAGER.lockEntryPair(user_id, sourceId, destinationId);

            return moveInFileSystem(user_id, sourceId, destinationId);
//            moveInDatabase(m);

        } finally {
//            if (lep != null) {
//                LockManager.LOCK_MANAGER.unlockEntryPair(lep);
//            }
        }
    }

    private static Entry moveInFileSystem(int user_id, int sourceId, int destinationId) throws PermissionException, FileNotFoundException, DuplicateName, IOException {

        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            EntryDAOImpl entryDAOImpl = new EntryDAOImpl(e.getEm());
            //find src,src parent , dsn
            List<Entry> sourcelist = entryDAOImpl.find(sourceId);

            //retrive src
            if (sourcelist.size() != 1) {
                throw new FileNotFoundException("File Not Found");
            }
            Entry source = sourcelist.get(0);

            //retrive src parent
            List<Entry> sourceplist = entryDAOImpl.find(source.getParentId().getId());
            if (sourceplist.size() != 1) {
                throw new PermissionException("Access Denied");
            }
            Entry parentSource = sourceplist.get(0);

            //retrive des
            List<Entry> destinationlist = entryDAOImpl.find(destinationId);

            if (destinationlist.size() != 1) {
                throw new PermissionException("Access Denied");
            }

            Entry destination = destinationlist.get(0);
            //------------------------------------------------------------------
            // here we are sure: source, parentSource, destination are not null
            if (!PermissionManager.canMoveEntry(parentSource, destination, user_id)) {
                throw new PermissionException("Access Denied");
            }
            //------------------------------------------------------------------

            FileManager.move(source, destination);
            AdminFileSystemController.moveInDatabase(source, destination, entryDAOImpl);

            return source;
        }
    }

    //---------------------delete-----------------------------------------------
    public static void delete(int userId, int id) throws FileNotFoundException, PermissionException, IOException {
        LockEntry le = null;
//        try {
//            //validetion
//            le = LockManager.LOCK_MANAGER.lockEntry(id, userId);
        deleteInFileSystem(userId, id);
//        } finally {
//            if (le != null) {
//                LockManager.LOCK_MANAGER.unlockEntry(le);
//            }
//
//        }
    }

    //to do : lock and parent
    public static void deleteInFileSystem(int user_id, int id) throws FileNotFoundException, PermissionException, IOException {

        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            EntryDAOImpl entryDAOImpl = new EntryDAOImpl(e.getEm());
            //find src,src parent , dsn

            List<Entry> sourcelist = entryDAOImpl.find(id);
            Entry source;
            //retrive src
            if (sourcelist.size() != 1) {
                throw new FileNotFoundException("entry not found");
            } else {
                source = sourcelist.get(0);
            }

            //retrive src parent
            List<Entry> sourceplist = entryDAOImpl.find(source.getParentId().getId());
            Entry parentSource;
            if (sourceplist.size() != 1) {
                throw new PermissionException("inaccessible entry");
            } else {
                parentSource = sourceplist.get(0);
            }

            if (!PermissionManager.canDeleteEntry(parentSource, user_id)) {
                throw new PermissionException("access denied");
            }

            FileManager.delete(source);
            AdminFileSystemController.deleteInDatabase(source, entryDAOImpl);
        }
    }

    //--------------------------Rename------------------------------------------
    public static Entry rename(int userId, String newFileName, int sourceId) throws FileNotFoundException, PermissionException, DuplicateName, IOException {
        LockEntry le = null;
        try {
            //validetion
            le = LockManager.LOCK_MANAGER.lockEntry(sourceId, userId);
            return renameInFileSystem(userId, newFileName, sourceId);
        } finally {
            if (le != null) {
                LockManager.LOCK_MANAGER.unlockEntry(le);
            }
        }
    }

    //lock parent src , src
    private static Entry renameInFileSystem(int user_id, String newFileName, int sourceId) throws FileNotFoundException, PermissionException, DuplicateName, IOException {

        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            EntryDAOImpl entryDAOImpl = new EntryDAOImpl(e.getEm());
            //find src,src parent , dsn
            List<Entry> sourcelist = entryDAOImpl.find(sourceId);
            Entry source;
            //retrive src
            if (sourcelist.size() != 1) {
                throw new FileNotFoundException("File Not Found");
            }
            source = sourcelist.get(0);

            //retrive src parent
            List<Entry> sourceplist = entryDAOImpl.find(source.getParentId().getId());
            Entry parentSource;
            if (sourceplist.size() != 1) {
                throw new PermissionException("Access Denied");
            }
            parentSource = sourceplist.get(0);

            if (!PermissionManager.canRenameEntry(parentSource, user_id)) {
                throw new PermissionException("Access denied");
            }

            FileManager.rename(source, newFileName);
            AdminFileSystemController.renameInDatabase(source, newFileName, entryDAOImpl);
            return source;
        }
    }

    //-----------------------download-------------------------------------------
    public static String download(int userId, int sourceId) throws FileNotFoundException, PermissionException, IOException {

        LockEntry le = null;

        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            String path;
            EntryDAOImpl entryDAOImpl = new EntryDAOImpl(e.getEntityManager());

            le = LockManager.LOCK_MANAGER.lockEntry(userId, sourceId);

            //find src,src parent , dsn
            List<Entry> sourcelist = entryDAOImpl.find(sourceId);
            Entry source;
            //retrive src
            if (sourcelist.size() != 1) {
                throw new FileNotFoundException("File Not Found");
            }
            source = sourcelist.get(0);

            //retrive src parent
            List<Entry> sourceplist = entryDAOImpl.find(source.getParentId().getId());
            Entry parentSource;
            if (sourceplist.size() != 1) {
                throw new PermissionException("Access Denied");
            }
            parentSource = sourceplist.get(0);

            if (parentSource.getParentId() == null) {
                if (!PermissionManager.canViewDirectory(source, userId)) {
                    throw new PermissionException("Access Denied");
                }
            } else if (!PermissionManager.canViewDirectory(parentSource, userId)) {
                throw new PermissionException("Access Denied");
            }

            return path = FileManager.download(source, entryDAOImpl, userId);
        } finally {
            if (le != null) {
                LockManager.LOCK_MANAGER.unlockEntry(le);
            }
        }

    }

    public static Entry copy(int user_id, int sourceId, int destinationId) throws PermissionException, FileNotFoundException, DuplicateName, IOException {
        LockEntryPair lep = null;
        try {
            //hear validation input
//            lep = LockManager.LOCK_MANAGER.lockEntryPair(user_id, sourceId, destinationId);
             return copyInFileSystem(user_id, sourceId, destinationId);
        } finally {
//            if (lep != null) {
//                LockManager.LOCK_MANAGER.unlockEntryPair(lep);
//            }
        }

    }

    //to do lock src , parent src and destination
    public static Entry copyInFileSystem(int user_id, int sourceId, int destinationId) throws PermissionException, FileNotFoundException, DuplicateName, IOException {
        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            EntryDAOImpl entryDAOImpl = new EntryDAOImpl(e.getEm());
            UserDAOImpl userDAOImpl = new UserDAOImpl(e.getEm());
            //find src,src parent , dsn
            List<Entry> sourcelist = entryDAOImpl.findEntryWithTag(sourceId);
            Entry source;
            //retrive src
            if (sourcelist.size() != 1) {
                throw new FileNotFoundException("File Not Found");
            }
            source = sourcelist.get(0);

            //retrive src parent
            List<Entry> sourceplist = entryDAOImpl.find(source.getParentId().getId());
            Entry parentSource;
            if (sourceplist.size() != 1) {
                throw new PermissionException("Access Denied");
            }
            parentSource = sourceplist.get(0);

            //retrive des
            List<Entry> destinationlist = entryDAOImpl.find(destinationId);
            Entry destination;
            if (destinationlist.size() != 1) {
                throw new PermissionException("Access Denied");
            }
            destination = destinationlist.get(0);

            //------------------------------------------------------------------
            // here we are sure: source, parentSource, destination are not null
            if (!PermissionManager.canCopyEntry(source, parentSource, destination, user_id)) {
                throw new PermissionException("Access Denied");
            }
            List<User> users = userDAOImpl.find(user_id);
            User user = users.get(0);
            //------------------------------------------------------------------
            LinkedHashMap<String, ArrayList<Entry>> map = FileManager.copy(source, parentSource, destination, entryDAOImpl, user);  // + insert into database        

            AdminFileSystemController.copyInDatabase(map, entryDAOImpl, e.getEm());
            return source;
        }
    }

//    
    public static Entry find(int userId, Integer sourceId) throws FileNotFoundException, PermissionException {
        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            EntryDAOImpl entryDAOImpl = new EntryDAOImpl(e.getEm());

            //find src,src parent , dsn
            List<Entry> sourcelist = entryDAOImpl.find(sourceId);
            Entry source;
            //retrive src
            if (sourcelist.size() != 1) {
                throw new FileNotFoundException("File Not Found");
            }
            source = sourcelist.get(0);

            if (source.getParentId() == null) {
                source.setParentId(null);
                return source;
            }

            if (!PermissionManager.canViewDirectory(source, userId)) {
                throw new PermissionException("Access Denied");
            }

            return source;

        }
    }
}

//            for (Map.Entry<String, ArrayList<Entry>> entry : map.entrySet()) {
//                String key = entry.getKey();
//                ArrayList<Entry> value = entry.getValue();
//                for (Entry en : value) {
//                    System.out.println("key : " + key + " value : " + en);
//                }
//            }
//            list.get(0).setParentId(destination);
//
//            for (Entry en : list) {
//                System.out.println(en);
//                for (Tag t : en.getTagList()) {
//                    System.out.println(t);
//                }
//            }
//            if (!source.getIsDirectory()) {
//                copyInDatabaseFile(list.get(0), entryDAOImpl);
//            }
//if (source.getIsDirectory()) {
//                // find all descendants
//                List<Entry> descendants = new ArrayList<>();
//
//                dfsFindDescendants(entryDAOImpl, descendants, source);
//                
//                for(Entry en :descendants){
//                   System.out.println(en.getFullpath());
//                }
//
//                for (Entry ent : descendants) {
//                    String str1 = FileManager.onlyFileNameFromAbsolutePath(ent.getFullpath());
//                    String str2 = FileManager.fullPathOfParentDir(ent.getFullpath()); // this should always return slash ...
//                    ent.setFullpath(ent.getFullpath().replace(str1, newpath_withoutfilename + str2));
//                }
//            }
//
//            source.setParentId(destination);
//            source.setFullpath(newpath_withoutfilename + oldFileName);
//        }
//    }
//
//            String oldFileName = FileManager.onlyFileNameFromAbsolutePath(source.getFullpath());
//            String oldpath_withoutfilename = FileManager.fullPathOfParentDir(source.getFullpath()); // this should always return slash ...
//            String newpath_withoutfilename = destination.getFullpath();
//  System.out.println();
//            for (Entry en : descendants) {
//                System.out.println("Desentents : " + en.getFullpath());
//            }
//        System.out.println();
//        for (Entry en : descendants) {
//            System.out.println(en.getFullpath());
//        }
//           System.out.println("src path : " + source.getFullpath()); //arxio src copyInDatabase
//            System.out.println("src path directory :" + FileManager.fullPathOfParentDir(source.getFullpath())); //directory src
//            System.out.println("des path : " + destination.getFullpath());//destination without /
//            System.out.println("des fullPath: " + FileManager.appandSlash(destination.getFullpath())); //destination with /
//        if (source.getIsDirectory()) {
//            destinationFullPath = appandSlash(fullPathOfParentDir(sourceFullPath)) + newFileName;
//        } else {
//            String dir = appandSlash(fullPathOfParentDir(sourceFullPath));
//            String fileName = onlyFileNameFromAbsolutePath(sourceFullPath);
//            String ext = FilenameUtils.getExtension(fileName);
//            destinationFullPath = dir + newFileName + "." + ext;
//        }
//        System.out.println(newFileName);
//        System.out.println(sourceFullPath);
////        System.out.println(destinationFullPath);
//        System.out.println(directoryName);
//        System.out.println(currnetFileName);
//        System.out.println();
//            List<Entry> descendants = new ArrayList<>();
////            descendants.add(source);
//            dfsFindDescendantsWithPremission(entryDAOImpl, descendants, source, userId);
//
//            List<String> filesListInDir = new ArrayList<>();
//            for (Entry en : descendants) {
//                if (!en.getIsDirectory()) {
//                    filesListInDir.add(en.getFullpath());
//                } 
//            }
//            for (Entry en : Lists.reverse(descendants)) {
//                System.out.println(en.getFullpath());
//            }
//
//            File dir = new File(source.getFullpath());
//            String zipDirName = source.getFullpath().concat(".zip");
//
//          if (source.getParentId() == null) {
//
////                List<Entry> parentlist = entryDAOImpl.find(source.getParentId().getId());
////                Entry parent = parentlist.get(0);
////                e.getEm().detach(source);
////                if (!PermissionManager.canViewDirectory(parent, userId)) {
//                source.setParentId(null);
////                }
//                return source;
//            }
//
//            if (!PermissionManager.canViewDirectory(source, userId)) {
//                throw new PermissionException("Access Denied");
//            }
//
////            System.out.println(source.toString());
//            return source;
//
//    public static void move(int user_id, Integer sourceId, Integer destinationId) throws IOException, PermissionException, FileNotFoundException, DuplicateName {
//        LockEntryPair lep = null;
//
//        try {
//            //hear validation input
////            lep = LockManager.LOCK_MANAGER.lockEntryPair(user_id, sourceId, destinationId);
//
//            MoveOperation m = constructOperation(user_id, sourceId, destinationId);
//
//            moveInFileSystem(user_id, m);
//            moveInDatabase(m);
//
//        } finally {
////            if (lep != null) {
////                LockManager.LOCK_MANAGER.unlockEntryPair(lep);
////            }
//        }
//    }
//
//    private static MoveOperation constructOperation(int user_id, int sourceId, int destinationId) throws PermissionException, FileNotFoundException, DuplicateName, IOException {
//        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
//            EntryDAOImpl entryDAOImpl = new EntryDAOImpl(e.getEm());
//            //find src,src parent , dsn
//            List<Entry> sourcelist = entryDAOImpl.find(sourceId);
//            Entry source;
//            //retrive src
//            if (sourcelist.size() != 1) {
//                throw new FileNotFoundException("File Not Found");
//            }
//            source = sourcelist.get(0);
//
//            //retrive src parent
//            List<Entry> sourceplist = entryDAOImpl.find(source.getParentId().getId());
//            Entry parentSource;
//            if (sourceplist.size() != 1) {
//                throw new PermissionException("Access Denied");
//            }
//            parentSource = sourceplist.get(0);
//
//            //retrive des
//            List<Entry> destinationlist = entryDAOImpl.find(destinationId);
//            Entry destination;
//            if (destinationlist.size() != 1) {
//                throw new PermissionException("Access Denied");
//            } else {
//                destination = destinationlist.get(0);
//            }
//            //------------------------------------------------------------------
//            // here we are sure: source, parentSource, destination are not null
//            if (!PermissionManager.canMoveEntry(parentSource, destination, user_id)) {
//                throw new PermissionException("access denied");
//            }
//
//            MoveOperation m = new MoveOperation(source, destination);
//
//            return m;
//        }
//    }
//class MoveOperation {
//
//    Entry source;
//    Entry destination;
//
//    public MoveOperation(Entry source, Entry destination) {
//        this.source = source;
//        this.destination = destination;
//    }
//private static void moveInFileSystem(int user_id, MoveOperation m) throws DuplicateName, IOException {
//        FileManager.move(m.source, m.destination);
//    }
//
//    private static void moveInDatabase(MoveOperation m) {
//        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
//            EntryDAOImpl entryDAOImpl = new EntryDAOImpl(e.getEm());
//
//            AdminFileSystemController.moveInDatabase(m.source, m.destination, entryDAOImpl);
//        }
//    }
