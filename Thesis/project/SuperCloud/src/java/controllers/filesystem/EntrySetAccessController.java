package controllers.filesystem;

import dao.EntryDAOImpl;
import dao.UserDAOImpl;
import dao.UserHasAccessDAOImpl;
import dto.EntryDTO;
import dto.UserDTO;
import entities.Entry;
import entities.User;
import entities.UserHasAccess;
import exeption.PermissionException;
import exeption.UserException;
import filesystem.PermissionManager;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import jpautils.EntityManagerTransaction;
import jpautils.EntityManagerTransactionless;
import model.EntryPremissionModel;
import model.PremissionAndUserModel;
import model.permission.RemoveAccessToEntryModel;

public class EntrySetAccessController {

    //edit other permission
    public static void editPremissionForOthers(Entry entity, int userId) throws FileNotFoundException, PermissionException {
        
        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            EntryDAOImpl dao = new EntryDAOImpl(e.getEm());
            
            List<Entry> entres = dao.findEntryWithPermissions(entity.getId());
            if (entres.size() != 1) {
                throw new FileNotFoundException("file not found");
            }
            
            if (!PermissionManager.canOwnerViewEntry(entres.get(0), userId)) {
                throw new PermissionException("Access Denied");
            }
            
            Entry newEntiy = entres.get(0);
            newEntiy.setOthersCanExecute(entity.getOthersCanExecute());
            newEntiy.setOthersCanRead(entity.getOthersCanRead());
            newEntiy.setOthersCanWrite(entity.getOthersCanWrite());
            newEntiy.setModifiedDate(new Date());
            
        }
    }

    //premison with entry
    public static EntryPremissionModel getAllPremissionToSpecifcUser(int entryId, int userId) throws FileNotFoundException, PermissionException {
        try (EntityManagerTransactionless less = new EntityManagerTransactionless()) {
            EntryDAOImpl dao = new EntryDAOImpl(less.getEntityManager());
            UserHasAccessDAOImpl udao = new UserHasAccessDAOImpl(less.getEntityManager());
            
            List<Entry> entryList = dao.find(entryId);
            if (entryList.size() != 1) {
                throw new FileNotFoundException("File Not Found");
            }
            
            if (!PermissionManager.canOwnerViewEntry(entryList.get(0), userId)) {
                throw new PermissionException("Access Denied");
            }
            
            List<UserHasAccess> list = udao.findEntryPremission(entryList.get(0).getId());
            
            EntryPremissionModel entryPremissionModel = new EntryPremissionModel();
            List<PremissionAndUserModel> premissionAndUserModels = new ArrayList<>();
            
            entryPremissionModel.setEntryModel(EntryDTO.toModel(entryList.get(0)));
            
            for (UserHasAccess u : list) {
                PremissionAndUserModel model = new PremissionAndUserModel();
                model.setCanExecute(u.getCanExecute());
                model.setCanRead(u.getCanRead());
                model.setCanWrite(u.getCanWrite());
                model.setUserModel(UserDTO.toModel(u.getUser()));
                premissionAndUserModels.add(model);
            }
            entryPremissionModel.setList(premissionAndUserModels);
            
            return entryPremissionModel;
        }
    }

    //----------------------------------Edit Access ----------------------------
    public static void editAccessPermission(UserHasAccess userHasAccess, int userId) throws FileNotFoundException, UserException, PermissionException {
        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            UserHasAccessDAOImpl dao = new UserHasAccessDAOImpl(e.getEm());
            EntryDAOImpl edao = new EntryDAOImpl(e.getEm());
            UserDAOImpl udao = new UserDAOImpl(e.getEm());
            
            List<Entry> entres = edao.find(userHasAccess.getEntry().getId());
            if (entres.size() != 1) {
                throw new FileNotFoundException("File Not Found");
            }
            
            List<User> users = udao.findUserByUserName(userHasAccess.getUser().getUsername());
            if (users.size() != 1) {
                throw new UserException("User Not Found");
            }
            
            if (!PermissionManager.canOwnerViewEntry(entres.get(0), userId)) {
                throw new PermissionException("Access Denied");
            }
            
            List<UserHasAccess> userHassAccess = dao.findPremission(userHasAccess.getEntry().getId(), userHasAccess.getUser().getId());
            if (userHassAccess.size() == 1) {
                editAccess(userHassAccess, userHasAccess, entres.get(0));
            }
        }
    }
    
    public static void editAccess(List<UserHasAccess> userHassAccess, UserHasAccess userHasAccess, Entry entry) {
        
        userHassAccess.get(0).setCanExecute(userHasAccess.getCanExecute());
        userHassAccess.get(0).setCanRead(userHasAccess.getCanRead());
        userHassAccess.get(0).setCanWrite(userHasAccess.getCanWrite());
        entry.setModifiedDate(new Date());
        
    }
    
    public static void addAccessPermission(UserHasAccess userHasAccess, int userId) throws FileNotFoundException, UserException, PermissionException {
        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            UserHasAccessDAOImpl dao = new UserHasAccessDAOImpl(e.getEm());
            EntryDAOImpl edao = new EntryDAOImpl(e.getEm());
            UserDAOImpl udao = new UserDAOImpl(e.getEm());
            
            List<Entry> entres = edao.find(userHasAccess.getEntry().getId());
            if (entres.size() != 1) {
                throw new FileNotFoundException("File Not Found");
            }
            Entry entry = entres.get(0);
            
            System.out.println(entres.get(0));
            List<User> users = udao.findUserByUserName(userHasAccess.getUser().getUsername());
            if (users.size() != 1) {
                throw new UserException("User Not Found");
            }
            
            User user = users.get(0);
            
            if (!PermissionManager.canOwnerViewEntry(entry, userId)) {
                throw new PermissionException("Access Denied");
            }
            
            List<UserHasAccess> userHassAccess = dao.findPremission(entry.getId(),user.getId());
            if (!userHassAccess.isEmpty()) {
                throw new PermissionException("Access Denied");
            }
            
            insertInDatabase(userHasAccess, entry, user);
        }
    }
    
    public static void insertInDatabase(UserHasAccess userHasAccess, Entry entry, User user) throws FileNotFoundException, PermissionException, UserException {

        //Insert Code
        entry.setModifiedDate(new Date());
        userHasAccess.setEntry(entry);
        userHasAccess.setUser(user);
        userHasAccess.getUserHasAccessPK().setEntryId(entry.getId());
        userHasAccess.getUserHasAccessPK().setUserId(user.getId());
        //Update the list of entry and user
        List<UserHasAccess> userList = user.getUserHasAccessList();
        if (userList == null) {
            List<UserHasAccess> newUserList = new ArrayList<>();
            newUserList.add(userHasAccess);
            user.setUserHasAccessList(newUserList);
        } else {
            userList.add(userHasAccess);
            user.setUserHasAccessList(userList);
        }
        List<UserHasAccess> entryList = entry.getUserHasAccessList();
        if (entryList == null) {
            List<UserHasAccess> newEntryList = new ArrayList<>();
            newEntryList.add(userHasAccess);
            entry.setUserHasAccessList(newEntryList);
        } else {
            entryList.add(userHasAccess);
            entry.setUserHasAccessList(userList);
        }
    }
    
    public static void removeAccess(RemoveAccessToEntryModel model, int userId) throws FileNotFoundException, UserException, PermissionException {
        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            UserHasAccessDAOImpl dao = new UserHasAccessDAOImpl(e.getEm());
            EntryDAOImpl edao = new EntryDAOImpl(e.getEm());
            UserDAOImpl udao = new UserDAOImpl(e.getEm());
            
            List<Entry> entres = edao.findEntryWithAccess(model.getEntryId());
            if (entres.size() != 1) {
                throw new FileNotFoundException("File Not Found");
            }
            Entry entry = entres.get(0);
            List<User> users = udao.find(model.getUserId());
            if (users.size() != 1) {
                throw new UserException("User Not Found");
            }
            User user = users.get(0);
            
            if (!PermissionManager.canOwnerViewEntry(entry, userId)) {
                throw new PermissionException("Access Denied");
            }
            
            System.out.println(entry);
            System.out.println(user);
            List<UserHasAccess> userHasAccess = dao.findPremission(entry.getId(), user.getId());
            if (userHasAccess.size() != 1) {
                throw new PermissionException("Premission Not Found");
            }
            
            dao.removePermmissionAccess(userHasAccess.get(0).getEntry().getId(), userHasAccess.get(0).getUser().getId());
        }
    }
    
}
