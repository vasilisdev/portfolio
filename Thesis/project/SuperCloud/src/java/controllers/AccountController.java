package controllers;

import angularsession.SessionMap;
import dao.RoleDAOImpl;
import dao.UserDAOImpl;
import entities.Role;
import entities.User;
import exeption.DuplicateAccount;
import exeption.UserNotFoundException;
import exeption.UserPasswordInvalidException;
import java.util.Date;
import java.util.List;
import jpautils.EntityManagerTransaction;
import jpautils.EntityManagerTransactionless;
import mappers.UserWithTokenMapper;
import model.AuthenticationModel;
import securityutils.TokenFactory;

public class AccountController {

    public static void register(User entity) throws DuplicateAccount {

        try (EntityManagerTransaction e = new EntityManagerTransaction()) {

            UserDAOImpl udao = new UserDAOImpl(e.getEm());
            RoleDAOImpl rDao = new RoleDAOImpl(e.getEm());

            if (!udao.findUserWithEmailAndUsername(entity).isEmpty()) {
                throw new DuplicateAccount("Duplicate username or email");
            }

            List<Role> rolesList = rDao.findAllUsersWithRole();
            rolesList.get(0).getUserList().add(entity);

            entity.setDateCreated(new Date());
            entity.setDateModified(new Date());
            entity.setActive(false);
            entity.setQuota(0);

            udao.create(entity);
          
        }
    }

    public static UserWithTokenMapper login(AuthenticationModel model) throws UserNotFoundException, UserPasswordInvalidException {

        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {

            String username = model.getUsername();
            String password = model.getPassword();
            
            System.out.println(username);

            UserDAOImpl udao = new UserDAOImpl(e.getEntityManager());
            List<User> users = udao.findUserByUserName(username);
            
            System.out.println(users);
            
            if (users.size() == 1) {
                User u = users.get(0);
                if (u.getPassword().equals(password)) { // TODO: change it to SHA2 hash
                    String token = TokenFactory.issueToken(username);
                    
                    UserWithTokenMapper userWithTokenMapper = new UserWithTokenMapper();
                    userWithTokenMapper.setToken(token);
                    userWithTokenMapper.setUser(u);
                    
                    SessionMap.map.put(token, u.getId());
                                        
                    return userWithTokenMapper;
                } else {
                    throw new UserPasswordInvalidException("invalid credential");
                }
            } else {
                throw new UserNotFoundException("User not found");
            }
        }
    }

    //to do when i make two request one after other in the second the entityManager is close
    public static List<User> viewProfile(User u) {
        List<User> users;
        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            UserDAOImpl udao = new UserDAOImpl(e.getEntityManager());
            users = udao.findUserByUserName(u.getUsername());
        }
        return users;
    }
}
