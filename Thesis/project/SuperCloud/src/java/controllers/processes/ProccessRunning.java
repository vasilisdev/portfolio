package controllers.processes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ProccessRunning {

    private ProcessBuilder pb;
    private final String path;

    public ProccessRunning(ProcessBuilder pb, String path) {
        this.pb = pb;
        this.path = path;

    }

    public void runProcess() throws IOException, InterruptedException {

        String output = "";
        String errors = "";
        Process process = pb.start();
        try (InputStream is = process.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                InputStream is2 = process.getErrorStream();
                InputStreamReader isr2 = new InputStreamReader(is2);
                BufferedReader br2 = new BufferedReader(isr2)) {

            String line;
            StringBuilder stdout = new StringBuilder();
            StringBuilder stderr = new StringBuilder();

            while ((line = br.readLine()) != null) {
                stdout.append(line);
                stdout.append(System.getProperty("line.separator"));
            }

            while ((line = br2.readLine()) != null) {
                stderr.append(line);
                stderr.append(System.getProperty("line.separator"));
            }

            output = stdout.toString();
            errors = stderr.toString();

        }
        process.waitFor();
    }

}
