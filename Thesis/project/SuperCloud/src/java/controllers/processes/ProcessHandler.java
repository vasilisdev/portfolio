package controllers.processes;

import dao.ExecutionLogDAOImpl;
import entities.Entry;
import entities.ExecutionLog;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.List;
import jpautils.EntityManagerTransaction;

public class ProcessHandler extends Thread {

    private final ProcessTable processTable;
    private final Entry entry;
    private Process process;
    private int pk;
    private EntityManagerTransaction t;


    public ProcessHandler(ProcessTable q, Entry entry, ExecutionLog executionLog, int pk) {
        this.processTable = q;
        this.entry = entry;
        this.pk = pk;

        processTable.add(this);
    }

    @Override
    public void run() {

        String output = "";
        String errors = "";

        try {

            //To do : reduract heare the out put 
            //To do : one output forError and forOutput
            process = new ProcessBuilder("java", "-jar", entry.getFullpath()).start(); // fork  

            try (EntityManagerTransaction e = new EntityManagerTransaction()) {
                ExecutionLogDAOImpl edao = new ExecutionLogDAOImpl(e.getEm());
                List<ExecutionLog> list = edao.find(pk);
                list.get(0).setPid((int) getPidOfProcess(process));
                list.get(0).setNowRunning(true);
            }

            System.out.println("New Process PID : " + getPidOfProcess(process));

            // go to database and find the path of the executable from entryID                 
            // todo: change with try-with-resources
            // todo: change flag to running = true
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            InputStream is2 = process.getErrorStream();
            InputStreamReader isr2 = new InputStreamReader(is2);
            BufferedReader br2 = new BufferedReader(isr2);

//            OutputStream out = process.getOutputStream();
            String line;

            StringBuilder stdout = new StringBuilder();
            StringBuilder stderr = new StringBuilder();

            while ((line = br.readLine()) != null) {
                stdout.append(line);
                stdout.append(System.getProperty("line.separator"));
            }

            while ((line = br2.readLine()) != null) {
                stderr.append(line);
                stderr.append(System.getProperty("line.separator"));
            }

            output = stdout.toString();
            errors = stderr.toString();

            process.waitFor();

            System.out.println("Stdout : " + stdout.toString());
            System.out.println("Stderror : " + stderr.toString());

        } catch (InterruptedException | IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // todo: store output and errors to database and set running = false

             try (EntityManagerTransaction e = new EntityManagerTransaction()) {
                ExecutionLogDAOImpl edao = new ExecutionLogDAOImpl(e.getEm());
                List<ExecutionLog> list = edao.find(pk);
                list.get(0).setNowRunning(false);
                list.get(0).setStderr(errors);
                list.get(0).setStdout(output);
            }

            processTable.remove(this);
        }
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public Process getRunningProcess() {
        return process;
    }

    public static synchronized long getPidOfProcess(Process p) throws Exception {
        long pid = -1;

        if (p.getClass().getName().equals("java.lang.UNIXProcess")) {
            Field f = p.getClass().getDeclaredField("pid");
            f.setAccessible(true);
            pid = f.getLong(p);
            f.setAccessible(false);
        }
        return pid;
    }
}
