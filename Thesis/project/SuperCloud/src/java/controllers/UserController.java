package controllers;

import dao.UserDAOImpl;
import entities.User;
import exeption.UserException;
import exeption.UserNotFoundException;
import exeption.DuplicateUsername;
import exeption.DuplicateEmail;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import jpautils.EntityManagerTransaction;
import jpautils.EntityManagerTransactionless;
import org.apache.commons.lang3.StringUtils;

public class UserController {
    
    public static List<User> findByUserName(String username) {
        List<User> users;
        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            UserDAOImpl udao = new UserDAOImpl(e.getEntityManager());
            users = udao.findUserByUserName(username);
        }
        return users;
    }
    
    public static List<User> retrieveAllUser() {
        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            UserDAOImpl dao = new UserDAOImpl(e.getEntityManager());
            List<User> users = dao.findAllUserActiveAndInactive();
            return users;
        }
    }

    //To do see again
    public static User retrieveUser(Integer id) throws UserException, UserNotFoundException {
        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            UserDAOImpl dao = new UserDAOImpl(e.getEntityManager());
            List<User> users = dao.find(id);
            if (users.size() == 1) {
                return users.get(0);
            } else {
                throw new UserNotFoundException("User not found");
            }
        }
    }

    //To do check the correct credentials for dublicate username and email
    public static User submitChanges(User user) throws UserNotFoundException, DuplicateUsername, DuplicateEmail {
        
        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            UserDAOImpl udao = new UserDAOImpl(e.getEm());
            List<User> users = udao.find(user.getId());
            if (users.size() != 1) {
                throw new UserNotFoundException("User not found");
            }
            
            
            User owner = users.get(0);       

            if (!StringUtils.isBlank(user.getUsername())) {
                List<User> userUserNameList = udao.findUserByUserName(user.getUsername());
                if ((userUserNameList.isEmpty()) || (userUserNameList.size() == 1 && Objects.equals(userUserNameList.get(0).getId(), owner.getId()))) {
                    owner.setUsername(user.getUsername());
                } else {
                    throw new DuplicateUsername("Duplicate username");
                }
            }
            
            //Change to isNotBlank
            if (!StringUtils.isBlank(user.getEmail())) {
                List<User> userEmailList = udao.findUserByEmail(user.getEmail());
                if ((userEmailList.isEmpty()) || (userEmailList.size() == 1 && Objects.equals(userEmailList.get(0).getId(), owner.getId()))) {
                    owner.setUsername(user.getEmail());
                } else {
                    throw new DuplicateEmail("Duplicate email");
                }
            }
            
            if (!StringUtils.isBlank(user.getName())) {
                owner.setName(user.getName());
            }
            
            if (!StringUtils.isBlank(user.getPassword())) {
                owner.setPassword(user.getPassword());
            }
            
            if (!StringUtils.isBlank(user.getSurname())) {
                owner.setSurname(user.getSurname());
            }
            
            if (!StringUtils.isBlank(user.getPhoneNumber())) {
                owner.setPhoneNumber(user.getPhoneNumber());
            }
            
            owner.setDateModified(new Date());
            e.getEm().flush();
            return owner;
        }
        
    }
    
}
