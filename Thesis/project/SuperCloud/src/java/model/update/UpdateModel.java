package model.update;

import java.util.List;

public class UpdateModel {
    
    private List <String> columnName;
    private List <String> newValues;
    private List <String> oldValues;
    private String tableName;
    private String schemanName;

    public UpdateModel() {
    }

    public List<String> getColumnName() {
        return columnName;
    }

    public void setColumnName(List<String> columnName) {
        this.columnName = columnName;
    }

    public List<String> getNewValues() {
        return newValues;
    }

    public void setNewValues(List<String> newValues) {
        this.newValues = newValues;
    }

    public List<String> getOldValues() {
        return oldValues;
    }

    public void setOldValues(List<String> oldValues) {
        this.oldValues = oldValues;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getSchemanName() {
        return schemanName;
    }

    public void setSchemanName(String schemanName) {
        this.schemanName = schemanName;
    }
}
