package model;

public class UserHasAccessToObjectModel {

    private Integer schemaObjectId;
    private Integer userId;
    Boolean canSelect;
    Boolean canInsert;
    Boolean canUpdate;
    Boolean canDelete;
    Boolean canAlter;
    Boolean canDrop;

    public UserHasAccessToObjectModel() {
    }

    public Integer getSchemaObjectId() {
        return schemaObjectId;
    }

    public void setSchemaObjectId(Integer schemaObjectId) {
        this.schemaObjectId = schemaObjectId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getCanSelect() {
        return canSelect;
    }

    public void setCanSelect(Boolean canSelect) {
        this.canSelect = canSelect;
    }

    public Boolean getCanInsert() {
        return canInsert;
    }

    public void setCanInsert(Boolean canInsert) {
        this.canInsert = canInsert;
    }

    public Boolean getCanUpdate() {
        return canUpdate;
    }

    public void setCanUpdate(Boolean canUpdate) {
        this.canUpdate = canUpdate;
    }

    public Boolean getCanDelete() {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete) {
        this.canDelete = canDelete;
    }

    public Boolean getCanAlter() {
        return canAlter;
    }

    public void setCanAlter(Boolean canAlter) {
        this.canAlter = canAlter;
    }

    public Boolean getCanDrop() {
        return canDrop;
    }

    public void setCanDrop(Boolean canDrop) {
        this.canDrop = canDrop;
    }

}
