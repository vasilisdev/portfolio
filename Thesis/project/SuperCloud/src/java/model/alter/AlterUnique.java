package model.alter;

import java.util.List;

public class AlterUnique {

    private List<String> unique;

    public AlterUnique() {
    }

    public List<String> getUnique() {
        return unique;
    }

    public void setUnique(List<String> unique) {
        this.unique = unique;
    }

}
