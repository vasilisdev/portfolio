package model.alter;

import java.util.List;


public class AlterModelForeignKey {
    
    private List<String> src;
    private List<String> foreignKey;
    private String destinationTable;

    public AlterModelForeignKey() {
    }

    public List<String> getSrc() {
        return src;
    }

    public void setSrc(List<String> src) {
        this.src = src;
    }

    public List<String> getForeignKey() {
        return foreignKey;
    }

    public void setForeignKey(List<String> foreignKey) {
        this.foreignKey = foreignKey;
    }

    public String getDestinationTable() {
        return destinationTable;
    }

    public void setDestinationTable(String destinationTable) {
        this.destinationTable = destinationTable;
    }
    
    
    
}
