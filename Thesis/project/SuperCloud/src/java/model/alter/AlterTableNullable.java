package model.alter;


public class AlterTableNullable {
    private String schemaName;
    private String tableName;
    private Nullabel property;

    public AlterTableNullable() {
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Nullabel getProperty() {
        return property;
    }

    public void setProperty(Nullabel property) {
        this.property = property;
    }
    
    
    
}
