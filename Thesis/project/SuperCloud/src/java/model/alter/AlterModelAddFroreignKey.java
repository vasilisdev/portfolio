package model.alter;

public class AlterModelAddFroreignKey {

    private String schemaName;
    private String tableName;
    private AlterModelForeignKey foreignKey;

    public AlterModelAddFroreignKey() {
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public AlterModelForeignKey getForeignKey() {
        return foreignKey;
    }

    public void setForeignKey(AlterModelForeignKey foreignKey) {
        this.foreignKey = foreignKey;
    }

}
