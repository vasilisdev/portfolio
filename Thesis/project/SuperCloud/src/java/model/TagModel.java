package model;

import java.io.Serializable;

public class TagModel implements Serializable{

    private int id;
    private String tagname;

    public TagModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTagname() {
        return tagname;
    }

    public void setTagname(String tagname) {
        this.tagname = tagname;
    }

    @Override
    public String toString() {
        return "TagModel{" + "id=" + id + ", tagname=" + tagname + '}';
    }
}
