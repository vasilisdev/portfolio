package model;

import java.io.Serializable;

public class DatabaseTablesModel implements Serializable{

    Integer id;
    String tablename;
    SchemaModel databaseId;

    public DatabaseTablesModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public SchemaModel getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(SchemaModel databaseId) {
        this.databaseId = databaseId;
    }

    @Override
    public String toString() {
        return "DatabaseTablesModel{" + "id=" + id + ", tablename=" + tablename + ", databaseId=" + databaseId + '}';
    }

}
