package model;

public class ProcessHelperModel {

    private Boolean isRunning;
    private EntryModel entryModel;

    public ProcessHelperModel() {
    }

    public Boolean getIsRunning() {
        return isRunning;
    }

    public void setIsRunning(Boolean isRunning) {
        this.isRunning = isRunning;
    }

    public EntryModel getEntryModel() {
        return entryModel;
    }

    public void setEntryModel(EntryModel entryModel) {
        this.entryModel = entryModel;
    }

}
