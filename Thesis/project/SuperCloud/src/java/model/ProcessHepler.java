package model;

import entities.Entry;

public class ProcessHepler {

    private Entry entry;
    private Boolean isRunning;

    public ProcessHepler() {
    }

 

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public Boolean isIsRunning() {
        return isRunning;
    }

    public void setIsRunning(Boolean isRunning) {
        this.isRunning = isRunning;
    }

 
}
