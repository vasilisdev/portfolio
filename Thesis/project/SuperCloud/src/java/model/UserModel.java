    package model;

import java.io.Serializable;
import java.util.Date;

public class UserModel implements Serializable {

    private Integer id;
    private String username;
    private String surname;
    private String name;
    private String password;
    private Boolean active;
    private String phoneNumber; 
    private Date dateCreated;
    private Date dateModified;
    private String email;
    private Integer quote;

    public UserModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getQuote() {
        return quote;
    }

    public void setQuote(Integer quote) {
        this.quote = quote;
    }

    @Override
    public String toString() {
        return "UserModel{" + "id=" + id + ", username=" + username + ", surname=" + surname + ", name=" + name + ", password=" + password + ", active=" + active + ", phoneNumber=" + phoneNumber + ", dateCreated=" + dateCreated + ", dateModified=" + dateModified + ", email=" + email + ", quote=" + quote + '}';
    }

}
