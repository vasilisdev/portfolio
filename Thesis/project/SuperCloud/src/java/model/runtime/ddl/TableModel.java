package model.runtime.ddl;

import model.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class TableModel implements Serializable{

    private String schemaName;
    private String tableName;
    private List<ColumnModel> columns;

    public TableModel() {
    
    }
    
    public TableModel(String schemaName, String tableName, List<ColumnModel> columns) {
        this.schemaName = schemaName;
        this.tableName = tableName;
        this.columns = columns;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<ColumnModel> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnModel> columns) {
        this.columns = columns;
    }
}
