package model.runtime.ddl.addColumns;

public class UniqueKeyModel {

    private Boolean isUnique;

    public UniqueKeyModel() {
    }

    public Boolean getIsUnique() {
        return isUnique;
    }

    public void setIsUnique(Boolean isUnique) {
        this.isUnique = isUnique;
    }
}
