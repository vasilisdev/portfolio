package model.runtime.ddl;

import java.util.List;

public class ForegnKeyRelationShip {

    List<String> sourceColumns;     // x, y
    List<String> foreignColumns;    // w, z
    String destinationTable;        //t2

    // foreign key (x,y) references t2(w,z)
    public ForegnKeyRelationShip() {
    }

    public List<String> getSourceColumns() {
        return sourceColumns;
    }

    public void setSourceColumns(List<String> sourceColumns) {
        this.sourceColumns = sourceColumns;
    }

    public List<String> getForeignColumns() {
        return foreignColumns;
    }

    public void setForeignColumns(List<String> foreignColumns) {
        this.foreignColumns = foreignColumns;
    }

    public String getDestinationTable() {
        return destinationTable;
    }

    public void setDestinationTable(String destinationTable) {
        this.destinationTable = destinationTable;
    }
}
