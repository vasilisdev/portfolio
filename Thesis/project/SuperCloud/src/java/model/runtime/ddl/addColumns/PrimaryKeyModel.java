package model.runtime.ddl.addColumns;

public class PrimaryKeyModel {

//    private Boolean isPrimaryKey;
    private Boolean isAutoIncrement;

    public PrimaryKeyModel() {
    }

//    public Boolean getIsPrimaryKey() {
//        return isPrimaryKey;
//    }
//
//    public void setIsPrimaryKey(Boolean isPrimaryKey) {
//        this.isPrimaryKey = isPrimaryKey;
//    }

    public Boolean getIsAutoIncrement() {
        return isAutoIncrement;
    }

    public void setIsAutoIncrement(Boolean isAutoIncrement) {
        this.isAutoIncrement = isAutoIncrement;
    }

}
