package model;

public class UserHasAccessToSchemaModel {
    
    private Integer userId;
    private Integer schemaId;
    private Boolean canModifay;

    public UserHasAccessToSchemaModel() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSchemaId() {
        return schemaId;
    }

    public void setSchemaId(Integer schemaId) {
        this.schemaId = schemaId;
    }

    public Boolean getCanModifay() {
        return canModifay;
    }

    public void setCanModifay(Boolean canModifay) {
        this.canModifay = canModifay;
    }
}
