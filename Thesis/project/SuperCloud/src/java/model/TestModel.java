package model;

import java.io.Serializable;
import java.util.Date;

public class TestModel implements Serializable {

    private String username;

    public TestModel() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
