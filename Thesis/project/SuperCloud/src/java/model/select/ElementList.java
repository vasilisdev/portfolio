package model.select;

import java.util.List;

public class ElementList {

    private List<TableElement> elements;

    public ElementList() {
    }

    public ElementList(List<TableElement> elements) {
        this.elements = elements;
    }

    public List<TableElement> getElements() {
        return elements;
    }

    public void setElements(List<TableElement> elements) {
        this.elements = elements;
    }

}
