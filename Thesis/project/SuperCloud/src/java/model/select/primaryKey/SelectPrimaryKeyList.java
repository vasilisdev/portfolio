
package model.select.primaryKey;

import java.util.List;

public class SelectPrimaryKeyList {
    List<String> listOfColumnName;

    public SelectPrimaryKeyList() {
    }

    public List<String> getListOfColumnName() {
        return listOfColumnName;
    }

    public void setListOfColumnName(List<String> listOfColumnName) {
        this.listOfColumnName = listOfColumnName;
    }
}
