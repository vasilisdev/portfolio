package model.select;

import java.util.List;

public class SelectAllReturnModel {

    private List<String> columnsName;
    private List<ElementList> rows;

    public SelectAllReturnModel() {
    }

    public List<String> getColumnsName() {
        return columnsName;
    }

    public void setColumnsName(List<String> columnsName) {
        this.columnsName = columnsName;
    }

    public List<ElementList> getRows() {
        return rows;
    }


    public void setRows(List<ElementList> rows) {
        this.rows = rows;
    }
}


