package model;

import model.permission.UserHasAccessModel;

public class UserHasAccessWithOwnerId {

    private Integer ownerId;
    private UserHasAccessModel userHasAccessModel;

    public UserHasAccessWithOwnerId() {
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public UserHasAccessModel getUserHasAccessModel() {
        return userHasAccessModel;
    }

    public void setUserHasAccessModel(UserHasAccessModel userHasAccessModel) {
        this.userHasAccessModel = userHasAccessModel;
    }
    
    
    @Override
    public String toString() {
        return "UserHasAccessWithOwnerId{" + "ownerId=" + ownerId + ", userHasAccessModel=" + userHasAccessModel + '}';
    }
    
    
}
