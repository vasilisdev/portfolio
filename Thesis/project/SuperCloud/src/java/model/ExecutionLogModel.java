package model;

import java.util.Date;

public class ExecutionLogModel {

    private Integer id;
    private String stdout;
    private String stderror;
    private Date runAt;
    private Boolean viewed;
    private Boolean nowRunning;
    private Integer pid;

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }
    private EntryModel entryModel;

    public ExecutionLogModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStdout() {
        return stdout;
    }

    public void setStdout(String stdout) {
        this.stdout = stdout;
    }

    public String getStderror() {
        return stderror;
    }

    public void setStderror(String stderror) {
        this.stderror = stderror;
    }

    public Date getRunAt() {
        return runAt;
    }

    public void setRunAt(Date runAt) {
        this.runAt = runAt;
    }

    public Boolean getViewed() {
        return viewed;
    }

    public void setViewed(Boolean viewed) {
        this.viewed = viewed;
    }

    public Boolean getNowRunning() {
        return nowRunning;
    }

    public void setNowRunning(Boolean nowRunning) {
        this.nowRunning = nowRunning;
    }

    public EntryModel getEntryModel() {
        return entryModel;
    }

    public void setEntryModel(EntryModel entryModel) {
        this.entryModel = entryModel;
    }

}
