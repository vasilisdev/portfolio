package model;

import java.util.List;

public class EntryPremissionModel {

    private EntryModel entryModel;
    private List<PremissionAndUserModel> list;

    public EntryPremissionModel() {
    }

    public EntryModel getEntryModel() {
        return entryModel;
    }

    public void setEntryModel(EntryModel entryModel) {
        this.entryModel = entryModel;
    }

    public List<PremissionAndUserModel> getList() {
        return list;
    }

    public void setList(List<PremissionAndUserModel> list) {
        this.list = list;
    }
}
