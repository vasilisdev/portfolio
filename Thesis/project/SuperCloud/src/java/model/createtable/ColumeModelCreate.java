package model.createtable;

public class ColumeModelCreate {

    private String columeName;
    private String type;
    private Boolean isNull; // not null
    private String size;  // default
    private Boolean autoIncrement;
    private String defaultValue;

    public ColumeModelCreate() {
    }

    public String getColumeName() {
        return columeName;
    }

    public void setColumeName(String columeName) {
        this.columeName = columeName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Boolean getIsNull() {
        return isNull;
    }

    public void setIsNull(Boolean isNull) {
        this.isNull = isNull;
    }

    public Boolean getAutoIncrement() {
        return autoIncrement;
    }

    public void setAutoIncrement(Boolean autoIncrement) {
        this.autoIncrement = autoIncrement;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

}
