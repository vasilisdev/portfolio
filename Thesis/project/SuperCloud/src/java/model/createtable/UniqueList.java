package model.createtable;

import java.util.List;

public class UniqueList {

    private List<String> set;

    public UniqueList() {
    }

    public List<String> getSet() {
        return set;
    }

    public void setSet(List<String> set) {
        this.set = set;
    }

}
