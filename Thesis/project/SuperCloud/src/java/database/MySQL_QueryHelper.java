package database;

public class MySQL_QueryHelper implements Mysql_Helper {

    public MySQL_QueryHelper() {

    }

    public static final String QUERY_CREATE_SCHEMA = CREATE_SCHEMA + " `%s`";
    public static final String QUERY_DROP_SCHEMA = DROP_SCHEMA + " `%s`";
    public static final String QUERY_DROP_TABLE = DROP_TABLE + " `%s`";
    public static final String QUERY_ALTER_TABLE_ADD_COLUMN = ALTER_TABLE + " `%s` " + ADD_COLUMN + " %s";
    public static final String QUERY_ALTER_TABLE_DROP_COLUMN = ALTER_TABLE + " `%s` " + DROP_COLUMN + " `%s`";
    public static final String QUERY_ALTER_TABLE_ADD_UNIQUE_KEY = ALTER_TABLE + " `%s` " + ADD_UNIQUE + "(%s)";
    public static final String QUERY_ALTER_TABLE_DROP_UNIQUE_KEY = ALTER_TABLE + " `%s` " + DROP_INDEX + " %s";
    public static final String QUERY_ALTER_TABLE_ADD_FOREIGN_KEY = ALTER_TABLE + " `%s` " + ADD_FOREIGN_KEY + "(%s) " + REFERENCES + " `%s`(%s)";
    public static final String QUERY_ALTER_TABLE_DROP_FORIGN_KEY = ALTER_TABLE + " `%s` " + DROP_FOREIGN_KEY + " `%s`";
    
    public static final String QUERY_ALTER_TABLE_ADD_PRIMARY_KEY = ALTER_TABLE + " `%s` " + ADD_PRIMARY_KEY + "(%s)";
    public static final String QUERY_ALTER_TABLE_DROP_PRIMARY_KEY = ALTER_TABLE + " `%s` " + DROP_PRIMARY_KEY;

    //modifay column
    public static final String QUERY_ALTER_TABLE_MODIFAY_ADD_AUTOINCREMENT = ALTER_TABLE + " `%s` " + MODIFY_COLUMN + " %s " + AUTO_INCREMENT;
    public static final String QUERY_ALTER_TABLE_MODIFAY_DROP_AUTOINCREMENT = ALTER_TABLE + " `%s` " + MODIFY_COLUMN + " %s " + NOT_NULL;
    public static final String QUERY_ALTER_TABLE_MODIFAY_SET_COLUMN_NULL = ALTER_TABLE + " `%s` " + MODIFY_COLUMN + " %s " + NULL;
    public static final String QUERY_ALTER_TABLE_MODIFAY_SET_COLUMN_NOT_NULL = ALTER_TABLE + " `%s` " + MODIFY_COLUMN + " %s " + NOT_NULL;
    public static final String QUERY_ALTER_TABLE_MODIFAY_TYPE_AND_VALUE = ALTER_TABLE + " `%s` " + MODIFY_COLUMN + " %s";

    //create tabele
    public static final String QUERY_CREATE_UNIQUE_KEY = UNIQUE_KEY + " (%s)";
    public static final String QUERY_CREATE_PRIMARY_KEY = PRIMARY_KEY + " (%s)";
    public static final String QUERY_CREATE_FOREIGN_KEY = FOREIGN_KEY + " (%s) " + REFERENCES + " `%s`(%s)";
    public static final String QUERY_CREATE_TABLE = CREATE_TABLE + " `%s` " + "(%s)";

    //dml
    public static final String QUERY_INSERT_INTO_ROW = INSERT_INTO + " `%s` (%s) " + VALUES + "(%s)";
    public static final String QUERY_DELETE_ROW = DELETE_FROM + " `%s` " + WHERE + " %s";
    public static final String QUERY_UPDATE_ROW = UPDATE + " `%s` " + SET + " %s " + WHERE + " %s";
}
