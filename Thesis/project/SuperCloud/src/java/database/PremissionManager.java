package database;

import entities.Schema;
import entities.UserHasAccessToObject;
import entities.UserHasAccessToSchema;

public class PremissionManager {

    public static boolean isOwner(Schema entry, int user_id) {
        if (entry == null) {
            return false;
        }
        if (entry.getUserId().getId() == user_id) {
            return true;
        }
        return false;
    }

    public static boolean canViewSchema(Schema entity, int userId) {
        if (entity == null) {
            return false;
        }

        if (entity.getUserId().getId() == userId) {
            return true;
        }

        for (UserHasAccessToSchema u : entity.getUserHasAccessToSchemaList()) {
            if (u.getUser().getId() == userId) {
                return true;
            }
        }

        return false;
    }

    public static boolean canViewSchemaObject(UserHasAccessToObject entity, int userId) {
        if (entity == null) {
            return false;
        }
        

        if (entity.getUser().getId() == userId) {
            if (entity.getCanAlter()) {
                return true;
            }
            if (entity.getCanDelete()) {
                return true;
            }
            if (entity.getCanDrop()) {
                return true;
            }
            if (entity.getCanInsert()) {
                return true;
            }
            if (entity.getCanSelect()) {
                return true;
            }
            if (entity.getCanUpdate()) {
                return true;
            }
        }

        return false;
    }

}
