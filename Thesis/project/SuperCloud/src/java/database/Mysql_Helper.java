package database;

public interface Mysql_Helper {

    public static final String ALTER_TABLE = "ALTER TABLE";
    public static final String ADD_PRIMARY_KEY = "ADD PRIMARY KEY";
    public static final String ADD_COLUMN = "ADD COLUMN";
    public static final String ADD_UNIQUE = "ADD UNIQUE";
    public static final String ADD_FOREIGN_KEY = "ADD FOREIGN KEY";
    public static final String REFERENCES = "REFERENCES";
    public static final String DROP_FOREIGN_KEY = "DROP FOREIGN KEY";
    public static final String DROP_TABLE = "DROP TABLE";
    public static final String DROP_SCHEMA = "DROP SCHEMA";
    public static final String DROP_COLUMN = "DROP COLUMN";
    public static final String DROP_INDEX = "DROP INDEX";
    public static final String DROP_PRIMARY_KEY = "DROP PRIMARY KEY";
    public static final String UNIQUE_KEY = "UNIQUE KEY";
    public static final String PRIMARY_KEY = "PRIMARY KEY";
    public static final String CREATE_TABLE = "CREATE TABLE";
    public static final String CREATE_SCHEMA = "CREATE SCHEMA";
    public static final String INSERT_INTO = "INSERT INTO";
    public static final String DELETE_FROM = "DELETE FROM";
    public static final String UPDATE = "UPDATE";
    public static final String WHERE = "WHERE";
    public static final String SET = "SET";
    public static final String AND = "AND";
    public static final String SELECT_ALL_FROM = "SELECT * FROM";
    public static final String NOT_NULL = "NOT NULL";
    public static final String NULL = "NULL";
    public static final String AUTO_INCREMENT = "AUTO_INCREMENT";
    public static final String DEFAUTL = "DEFAULT";
    public static final String FOREIGN_KEY = "FOREIGN KEY";
    public static final String MODIFY_COLUMN = "MODIFY COLUMN";
    public static final String VALUES = "VALUES";
}
