
package database;

import entities.Schema;

public class SchemaManager {
    
    public static String createSchemaName(Schema entity, int userId){
//        return "user"+ "_"+Integer.toString(userId)+"_" + entity.getSchemaName();
        return new StringBuilder().append("user_").append(Integer.toString(userId)).append("_").append(entity.getSchemaName()).toString();
    }
    
    public static String createSchemaName(String schemaName, int userId){
        return "user"+ "_"+Integer.toString(userId)+"_" + schemaName;
    }
    
    
}
