package service;

import java.util.Set;
import javax.ws.rs.core.Application;
import org.glassfish.jersey.media.multipart.MultiPartFeature;



@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        resources.add(MultiPartFeature.class);
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method. It is automatically
     * populated with all resources defined in the project. If required, comment
     * out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(resource.AccountResource.class);
        resources.add(resource.AdminResource.class);
        resources.add(resource.EntryResource.class);
        resources.add(resource.ProcessRecource.class);
        resources.add(resource.SchemaRecourse.class);
        resources.add(resource.UserResource.class);
        resources.add(securityutils.JWTTokenNeededFilter.class);

    }

//    @Override
//    public Map<String, Object> getProperties() {
//        Map<String, Object> properties = new HashMap<String, Object>();
//        properties.put("jersey.config.server.provider.packages", "resource");
//        return properties;
//    }

}
