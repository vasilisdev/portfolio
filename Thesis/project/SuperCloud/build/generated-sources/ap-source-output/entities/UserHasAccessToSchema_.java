package entities;

import entities.Schema;
import entities.User;
import entities.UserHasAccessToSchemaPK;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-16T02:10:09")
@StaticMetamodel(UserHasAccessToSchema.class)
public class UserHasAccessToSchema_ { 

    public static volatile SingularAttribute<UserHasAccessToSchema, Schema> schema;
    public static volatile SingularAttribute<UserHasAccessToSchema, UserHasAccessToSchemaPK> userHasAccessToSchemaPK;
    public static volatile SingularAttribute<UserHasAccessToSchema, Boolean> canModify;
    public static volatile SingularAttribute<UserHasAccessToSchema, User> user;

}