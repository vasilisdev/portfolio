package entities;

import entities.Entry;
import entities.ExecutionLog;
import entities.Tag;
import entities.User;
import entities.UserHasAccess;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-16T02:10:09")
@StaticMetamodel(Entry.class)
public class Entry_ { 

    public static volatile SingularAttribute<Entry, Boolean> othersCanExecute;
    public static volatile ListAttribute<Entry, ExecutionLog> executionLogList;
    public static volatile SingularAttribute<Entry, User> ownerId;
    public static volatile SingularAttribute<Entry, Boolean> othersCanRead;
    public static volatile SingularAttribute<Entry, Entry> parentId;
    public static volatile ListAttribute<Entry, Tag> tagList;
    public static volatile ListAttribute<Entry, UserHasAccess> userHasAccessList;
    public static volatile SingularAttribute<Entry, Boolean> othersCanWrite;
    public static volatile SingularAttribute<Entry, Date> createdDate;
    public static volatile SingularAttribute<Entry, Boolean> isDeleted;
    public static volatile SingularAttribute<Entry, String> fullpath;
    public static volatile SingularAttribute<Entry, Date> modifiedDate;
    public static volatile ListAttribute<Entry, Entry> entryList;
    public static volatile SingularAttribute<Entry, Integer> id;
    public static volatile SingularAttribute<Entry, Boolean> isExecutable;
    public static volatile SingularAttribute<Entry, Boolean> isDirectory;

}