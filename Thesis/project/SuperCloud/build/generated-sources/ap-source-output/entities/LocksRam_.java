package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-16T02:10:09")
@StaticMetamodel(LocksRam.class)
public class LocksRam_ { 

    public static volatile SingularAttribute<LocksRam, String> lockId;
    public static volatile SingularAttribute<LocksRam, Boolean> activeLock;
    public static volatile SingularAttribute<LocksRam, Integer> entryIdTarget;
    public static volatile SingularAttribute<LocksRam, Integer> entryId;

}