package entities;

import entities.SchemaObject;
import entities.User;
import entities.UserHasAccessToObjectPK;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-16T02:10:09")
@StaticMetamodel(UserHasAccessToObject.class)
public class UserHasAccessToObject_ { 

    public static volatile SingularAttribute<UserHasAccessToObject, Boolean> canUpdate;
    public static volatile SingularAttribute<UserHasAccessToObject, Boolean> canDrop;
    public static volatile SingularAttribute<UserHasAccessToObject, Boolean> canInsert;
    public static volatile SingularAttribute<UserHasAccessToObject, Boolean> canSelect;
    public static volatile SingularAttribute<UserHasAccessToObject, SchemaObject> schemaObject;
    public static volatile SingularAttribute<UserHasAccessToObject, UserHasAccessToObjectPK> userHasAccessToObjectPK;
    public static volatile SingularAttribute<UserHasAccessToObject, Boolean> canDelete;
    public static volatile SingularAttribute<UserHasAccessToObject, Boolean> canAlter;
    public static volatile SingularAttribute<UserHasAccessToObject, User> user;

}