#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <signal.h>
#include <pthread.h>

#include "hash.h"
#include "lowlevelio.h"
#include "server_parser.h"
#include "server_function.h"
#include "prod_cons.h"

int main(int argc, char** argv) {

    int server_port, server_threadnumber, server_queuesize;
    if (argc != 7) {
        printf("Use this syntax: ./bankserver -p <port> -s <thread pool size> -q <queue size>\n");
        exit(EXIT_FAILURE);
    }

    //
    //    if ((server_port = atoi(argv[2])) <= 0) {
    //        printf("Not correct arguments\n");
    //        exit(EXIT_FAILURE);
    //    } else if ((server_threadnumber = atoi(argv[4])) <= 0) {
    //        printf("Not correct arguments\n");
    //        exit(EXIT_FAILURE);
    //    } else if ((server_queuesize = atoi(argv[6])) <= 0) {
    //        printf("Not correct arguments\n");
    //        exit(EXIT_FAILURE);
    //    } else {
    //        printf("Correct arguments\n");
    //    }

    
    //______________________check arguments_____________________________________
    
    int i, ctr_port = 0, ctr_thread = 0, ctr_queue = 0;
    for (i = 1; i < argc; i += 2) {
        if (strncmp("-p", argv[i], strlen("-p")) == 0) {
            if ((server_port = atoi(argv[i + 1])) <= 0) {
                printf("Not correct arguments in port\n");
                exit(EXIT_FAILURE);
            }
            ctr_port++;
            continue;
        } else if (strncmp("-s", argv[i], strlen("-s")) == 0) {
            if ((server_threadnumber = atoi(argv[i + 1])) <= 0) {
                printf("Not correct arguments thread pool size\n");
                exit(EXIT_FAILURE);
            }
            ctr_thread++;
            continue;
        } else if (strncmp("-q", argv[i], strlen("-q")) == 0) {
            if ((server_queuesize = atoi(argv[i + 1])) <= 0) {
                printf("Not correct arguments queue size\n");
                exit(EXIT_FAILURE);
            }
            ctr_queue++;
            continue;
        } else {
            printf("NOT CORRECT ARGUMETNS\n");
            exit(EXIT_FAILURE);
        }
    }
    
    if (ctr_port != 1 || ctr_thread != 1 || ctr_queue != 1) {
        printf("duplicate command argument %d %d %d\n", ctr_port, ctr_thread, ctr_queue);
        exit(EXIT_FAILURE);
    }

    
            struct sockaddr_in client;
            int client_socket;
            socklen_t clientlen = sizeof (client);
    
    
            //_______________ initialize master socket _________________________________
    
            int master_socket = init_master_socket(server_port);
    
            //__________________________________________________________________________
    
            pid_t pid = getpid(); //take the pid of the server process
            printf("-------------------------------------------\n");
            printf("\nserver_port             is '%d' ", server_port);
            printf("\nserver_queuesize        is '%d' ", server_queuesize);
            printf("\nserver_threads          is '%d' ", server_threadnumber);
            printf("\nserver online! pid      is '%d' ", (int) pid);
            printf("\nmaster_socket           is '%d' ", master_socket);
            printf("\n-------------------------------------------\n");
    
    
            ptrHash infoHash = NULL;
            ptrQueue infoQueue = NULL;
            int bukkit = 17;
            Queue_allocate(&infoQueue, server_queuesize);
            Hash_allocate(&infoHash, bukkit);
            Threadpool_allocate(server_threadnumber, infoQueue, infoHash);
    
    
            printf("\nwaiting for connections to port %d\n", server_port);
    
            while (1) {
                //___________________________WAIT FOR CONNECTION________________________
                //______________________________________________________________________
    
                printf("master socket: %d \n", master_socket);
                if ((client_socket = accept(master_socket, (struct sockaddr *) &client, &clientlen)) < 0) {
                    perror_exit("accept");
                    printf("%d\n", client_socket);
                }
    
                //___________________________ ADD TO QUEUE _____________________________
                //______________________________________________________________________
    
                printf("Accepted connection from client. Descriptor %d placing on queue \n", client_socket);
    
                Queue_place(infoQueue, client_socket);
            }
    
            //___________________________________________________server_shutdown
    
            printf("Server shutting down ...\n");
    
            close_master_socket();
    
            Hash_deallocate(&infoHash);
            Queue_deallocate(&infoQueue);
            Threadpool_deallocate(server_threadnumber);
    
            printf("Server went offline!\n");
    
            exit(EXIT_SUCCESS);
}

