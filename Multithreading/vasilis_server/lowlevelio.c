#include <netinet/in.h>

#include "lowlevelio.h"
// extio.c

ssize_t writeall(int fd, const void *buf, ssize_t nbyte) {//*)
    ssize_t nwritten = 0, n;

    do {
        n = write(fd, &((const char *) buf)[nwritten], nbyte - nwritten);
        if (n == -1) {
            if (errno == EINTR)////write terminate from signal that exist handler
                continue;
            else
                return -1;
        }
        nwritten += n;
    } while (nwritten < nbyte);

    return nwritten;
}

ssize_t readall(int fd, void *buf, ssize_t nbyte) {//**)
    ssize_t nread = 0, n;

    do {
        n = read(fd, &((char *) buf)[nread], nbyte - nread);
        if (n == -1) {
            if (errno == EINTR)//read terminate from signal that exist handler
                continue;
            else
                return -1;
        }
        if (n == 0)//no connection 
            return nread;
        nread += n;
    } while (nread < nbyte);

    return nread;
}


void writeall_numberOfBytesAndDatas(char* line, int fd_pipe, int linesize) {
    int linesize_network = htonl(linesize);
    
    if (writeall(fd_pipe, &linesize_network, sizeof (linesize_network)) == -1) {
        perror("Writing number of byes.Reason: ");
        exit(1);
    }
    if (writeall(fd_pipe, line, linesize) == -1) {
        perror("Writing in pipe.Reason: ");
        exit(1);
    }
}

char* readall_numberOfBytesAndDatas(int fd_pipe) {
    char* buffer;
    int byteNumber;
    int res;
    
    int linesize_network;

    //read and store to byteNumber the number of bytes
    if ((res=readall(fd_pipe, &linesize_network, sizeof (linesize_network))) == -1) {
        perror("Reading the number of bytes.Reason: ");
        return NULL;
    }
    
    byteNumber = ntohl(linesize_network);
    
    if (res == 0) {//case the other socket have close the connection
        return NULL;
    }

    buffer = malloc(byteNumber * sizeof (char) + 1);
    memset(buffer, '\0', byteNumber + 1);

    if (readall(fd_pipe, buffer, byteNumber) == -1) {
        perror("Reading from pipe.Reason: ");
    }
    return buffer;
}

//_________link or other sources that help to complete the project______________

//http://basepath.com/aup/ex/extio_8c-source.html
//extio.c
//*)unix book page 117
//**)unix book page 119