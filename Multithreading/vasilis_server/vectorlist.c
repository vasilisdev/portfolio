#include "vectorlist.h"
//______________________________________________________________________________

// _____________________________LISTA___AKMON___________________________________

struct vector {
    ptrNode destiny; // kovos pou dixni 
    double weight; //baros kombou
    ptrVector next;
};

struct infoVector {
    long int numberVectors;
    ptrVector starList; //point to starList
    ptrVector tailList; //point to end
};

// _______________________________ InfoVector ________________________________
// create

void InfoVector_create(ptrInfoVector* infonodeptr) {
    *infonodeptr = (ptrInfoVector) malloc(sizeof (struct infoVector));
    (*infonodeptr)->numberVectors = 0;
    (*infonodeptr)->starList = NULL;
    (*infonodeptr)->tailList = NULL;
}

// deallocate 

void InfoVector_deallocate(ptrInfoVector* head) {
    if (head == NULL) {
        return;
    }
    if (InfoVector_Empty(*head)) {
        free(*head);
        return;
    }

    ptrVector temp = (*head)->starList;
    ptrVector prev;
    while (temp != NULL) {
        prev = temp;
        temp = temp->next;
        free(prev);
    }
    (*head)->starList = NULL;
    (*head)->tailList = NULL;
    (*head)->numberVectors = 0;
    free(*head);
    *head = NULL;
}

//check empty

int InfoVector_Empty(ptrInfoVector head) {
    if ((head->tailList == NULL) || (head->starList == NULL))
        return 1;
    return 0;
}

//number of vectors

long int InfoVector_getSize(ptrInfoVector head) {
    return head->numberVectors;
}
//insert in end

int InfoVector_Insert(ptrInfoVector* head, ptrNode myDestiny, double myWeight) {

    ptrVector myVector = (ptrVector) malloc(sizeof (struct vector));
    if (myVector == NULL) {
        printf("malloc\n");
        return 1;
    }

    //create a vector with the data 
    (*head)->numberVectors++;
    myVector->weight = myWeight;
    myVector->destiny = myDestiny;
    myVector->next = NULL;
    if (InfoVector_Empty(*head)) {//case empty the list of vector update the last and the start
        (*head)->starList = myVector;
        (*head)->tailList = myVector;
    } else {//other case update the last pointer
        (*head)->tailList->next = myVector;
        (*head)->tailList = myVector;
    }
    return 0;
}

//update the weight of the destination pointer

int InfoVector_Update(ptrInfoVector* head, ptrNode myDestiny, double myWeight) {//insert the node and the weigt
    ptrNode temp2;
    if (head == NULL || myDestiny == NULL) {
        return 1;
    }
    ptrVector temp = (*head)->starList; //pointer to vector
    while (temp != NULL) {//find the destination pointer to make the update
        temp2 = Vector_getDestination(temp); //temp2 pointer to bank account that is in the lists //ptrNode temp2;
        if (temp2 == myDestiny) {
            Vector_setWeight(temp, Vector_getWeight(temp) + myWeight); //update the new weight of the connection
            return 0;
        }
        temp = temp->next;
    }
    return 1; //false 
}

int InfoVector_InsertOrUpdate(ptrInfoVector* head, ptrNode myDestiny, double myWeight) {
    int x;
    x = InfoVector_Update(head, myDestiny, myWeight); //if it return 1 that means that the bank account is not in the list //insert
    if (x == 1) {
        return InfoVector_Insert(head, myDestiny, myWeight); //make
    } else {
        return 0;
    }
}

//print all list

void InfoVector_print(ptrInfoVector head) {
    if (head == NULL) {
        printf("Infovector is empty ! \n");
        return;
    }
    ptrVector temp = head->starList;
    while (temp != NULL) {
        printf(" (%s,amount %f $)  ", Node_getName(temp->destiny), Vector_getWeight(temp));
        temp = temp->next;
    }
}
//return the sum of all the vector

double InfoVector_sum(ptrInfoVector head) {
    double sum = 0;
    if (head == NULL) {
        return 0;
    }
    ptrVector temp = head->starList;
    while (temp != NULL) {
        sum = sum + Vector_getWeight(temp);
        temp = temp->next;
    }
    return sum;
}

//return the node that point 

ptrNode InfoVector_getNode(ptrInfoVector head, ptrNode myDestiny) {
    if (head == NULL || InfoVector_Empty(head) || myDestiny == NULL) {
        return NULL;
    }
    ptrVector temp = head->starList;
    while (temp != NULL) {
        if (temp->destiny == myDestiny) {
            return Vector_getDestination(temp);
        }
        temp = temp->next;
    }
    return NULL;
}

ptrNode InfoVector_getNodeByNameAndSurname(ptrInfoVector head, char* myName) {
    if (head == NULL) {
        return NULL;
    }
    ptrVector temp = head->starList;
    while (temp != NULL) {
        ptrNode temp2 = Vector_getDestination(temp); //temp2 pointer to bank account that is in the lists //ptrNode temp2;
        if ((strcmp(Node_getName(temp2), myName) == 0)) {
            return temp2;
        }
        temp = temp->next;
    }
    return NULL;
}


//return the start of the list vector 

ptrVector InfoVector_getStartList(ptrInfoVector head) {//epistrefi tin arxi tis lisas
    if (head == NULL) {
        return NULL;
    } else {
        return head->starList;
    }
}

// __________________________________ Vector ________________________________
//create vector

void Vector_create(ptrVector* head, double myWeight, ptrNode myDestiny) {
    *head = (ptrVector) malloc(sizeof (struct vector));
    if (head == NULL) {
        printf("failure <malloc\n>");
        return;
    }
    (*head)->weight = myWeight;
    (*head)->destiny = myDestiny;
    (*head)->next = NULL;
}

//deallocate vector

void Vector_deallocate(ptrVector* head) {
    if (*head == NULL) {
        return;
    }
    free(*head);
    head = NULL;
}

//set weight

void Vector_setWeight(ptrVector myVector, double newWeight) {
    if (myVector == NULL) {
        return;
    }
    myVector->weight = newWeight;
}

//get

double Vector_getWeight(ptrVector myVector) {
    if (myVector == NULL) {
        return 0;
    }
    return myVector->weight;
}

//return a pointer to account bank account that the vector has

ptrNode Vector_getDestination(ptrVector myVector) {
    if (myVector == NULL) {
        return NULL;
    }
    return myVector->destiny;
}
