#include "server_function.h"
#include <pthread.h>

int master_socket;
pthread_t *thread_ids;

typedef struct ThreadParams * ptrThreadParams;

typedef struct ThreadParams {
    int id;
    ptrQueue queue;
    ptrHash hash;
} ThreadParams;


void* Threadpool_service(void * args) {
    ptrThreadParams params = (ptrThreadParams) args;//cast too thread params
    int id = params->id;
    ptrQueue infoQueue = params->queue;
    ptrHash infoHash = params->hash;

    while (1) {
        printf("Thread %d blocked on Queue_obtain \n", id);
        int client_socket = Queue_obtain(infoQueue);
        printf("Thread %d unblocked on Queue_obtain, obtained %d \n", id, client_socket);

        char* message;
        while ((message = readall_numberOfBytesAndDatas(client_socket)) != NULL) {//NULL no more connection
            printf("received from clinet : '%s' \n", message);
            server_parser(infoHash, message, client_socket);
            free(message);
        }
        close(client_socket);
    }
    free(params);
    return NULL;
}

void perror_exit(char *message) {
    perror(message);
    exit(EXIT_FAILURE);
}

int init_master_socket(int port) {
    static struct sockaddr_in server;
    struct sockaddr *serverptr = (struct sockaddr *) &server;

    
    
    //____________________________CREATE_SOCKET_________________________________
    //__________________________________________________________________________
    if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror_exit("socket");
    } else {
        printf("Socket created successfully! \n");
    }
    
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_family = AF_INET; /* Internet domain */
    server.sin_port = htons(port); /* The given port */

    //______________________________BIND_SOCKET_________________________________
    //__________________________________________________________________________
    if (bind(master_socket, serverptr, sizeof (server)) < 0) {
        if (errno == EADDRINUSE) {
            printf("Address is in use: port %d \n", port);
            perror_exit("bind");
        }
    }

    //___________________________ENABLE LISTENING_______________________________
    //__________________________________________________________________________
    if (listen(master_socket, 50) < 0) {
        perror_exit("listen");
    }
    return master_socket;
}

void close_master_socket() {
    close(master_socket);
}

void Threadpool_allocate(int pool_size, ptrQueue queue, ptrHash hash) {
    int i;
    thread_ids = malloc(pool_size * sizeof (pthread_t));
    for (i = 0; i < pool_size; i++) {
        ptrThreadParams params = malloc(sizeof (ThreadParams));
        params->id = i + 1;
        params->queue = queue;
        params->hash = hash;
        pthread_create(&thread_ids[i], NULL, Threadpool_service, params);
    }
}

void Threadpool_deallocate(int pool_size) {
    int i;
    for (i = 0; i < pool_size; i++) {
        pthread_join(thread_ids[i], NULL);
    }
    free(thread_ids);
}