#include "prod_cons.h"

struct queue_t {
    int *data;
    int start;
    int end;
    int count;
    int size;
};

pthread_mutex_t mtx;
pthread_cond_t cond_nonempty;
pthread_cond_t cond_nonfull;

void Queue_allocate(ptrQueue* infoQueue, int size) {

    (*infoQueue) = malloc(sizeof (struct queue_t));
    (*infoQueue)->start = 0;
    (*infoQueue)->end = -1;
    (*infoQueue)->count = 0;
    (*infoQueue)->size = size;

    (*infoQueue)->data = malloc(sizeof (int)*size);

    pthread_mutex_init(&mtx, 0);
    pthread_cond_init(&cond_nonempty, 0);
    pthread_cond_init(&cond_nonfull, 0);
}

void Queue_deallocate(ptrQueue* infoQueue) {
    pthread_cond_destroy(&cond_nonempty);
    pthread_cond_destroy(&cond_nonfull);
    pthread_mutex_destroy(&mtx);

    free((*infoQueue)->data);
    free((*infoQueue));
    *infoQueue = NULL;
}

void Queue_place(ptrQueue infoQueue, int data) {
    pthread_mutex_lock(&mtx);
    while (infoQueue->count == infoQueue->size) {//queue full
        pthread_cond_wait(&cond_nonfull, &mtx); // wait to came consumer 
    }
    infoQueue->end = (infoQueue->end + 1) % infoQueue->size;
    infoQueue->data[infoQueue->end] = data;
    infoQueue->count++;

    printf("Queue_place: %d \n", data);

    pthread_cond_signal(&cond_nonempty);

    pthread_mutex_unlock(&mtx);
}

int Queue_obtain(ptrQueue infoQueue) {
    int data;
    pthread_mutex_lock(&mtx);
    while (infoQueue->count == 0) {
        pthread_cond_wait(&cond_nonempty, &mtx); //wait to came produsers
    }
    data = infoQueue->data[infoQueue->start];
    infoQueue->start = (infoQueue->start + 1) % infoQueue->size;
    infoQueue->count--;

    printf("Queue_obtain: %d \n", data);

    pthread_cond_signal(&cond_nonfull);

    pthread_mutex_unlock(&mtx);

    return data;
}