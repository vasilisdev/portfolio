#ifndef __NODELIST_H__
#define __NODELIST_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "node.h"
#include "lowlevelio.h"

typedef struct listNode* ptrListNode;
typedef struct infoList* ptrInfoList;


// _____________________________________________________________________InfoList
void InfoList_create(ptrInfoList* listInfoHead);

void InfoList_deallocate(ptrInfoList* listInfoHead);

int InfoList_Empty(ptrInfoList listInfoHead);

void InfoList_print(ptrInfoList listInfoHead);

void InfoList_lock(ptrInfoList listInfoHead);

void  InfoList_unlock(ptrInfoList listInfoHead);

ptrListNode InfoList_SearchIfTheNodeIsInList(ptrInfoList listInfoHead, char* name);

int InfoList_Insert(ptrInfoList* listInfoHead, char* myName, double myAmount);

long int InfoList_getSize(ptrInfoList listInfoHead);

// _____________________________________________________________________ListNode
void ListNode_create(ptrListNode* myListNode, char* myName, double myAmount);

void ListNode_deallocate(ptrListNode* myListNode);

ptrNode ListNode_getDestination(ptrListNode myListNode);

#endif

