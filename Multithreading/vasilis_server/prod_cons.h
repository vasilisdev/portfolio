#ifndef __PROD_CONS_H__
#define __PROD_CONS_H__

#include <stdio.h>   
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>


typedef struct queue_t* ptrQueue;

void Queue_allocate(ptrQueue* infoQueue, int size);

void Queue_deallocate(ptrQueue* infoQueue);

void Queue_place(ptrQueue infoQueue, int data);

int Queue_obtain(ptrQueue infoQueue);

#endif /* PROD_CONS_H */

// from www.mario-konrad.ch

