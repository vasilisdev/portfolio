#ifndef __HASH_H__
#define __HASH_H__

#include <string.h>
#include <string.h>
#include <stdlib.h>
#include <error.h>
#include <unistd.h>


#include "lowlevelio.h"
#include "vectorlist.h"
#include "nodelist.h"
#include "node.h"


typedef struct hash* ptrHash;

//__________________________________________________________________________hash
void Hash_allocate(ptrHash* infoHash, long int buckitNumber);

void Hash_deallocate(ptrHash* infohash);

int Hash_Insert(ptrHash* infoHash, double amount, char * myName, int delay,ptrInfoList info );

int Hash_checkAccount(ptrHash* infoHash, char* myName);

int Hash_checkMuliAccount(ptrHash* infoHash, char* line);

int Hash_AddTran(ptrHash* infoHash, char * myName1, char * myName2, double myAmount, int delay);

double Hash_LookUp(ptrHash infoHash, char * myName);

void Hash_Print(ptrHash infohash);

long int Hash_function(char* name);

ptrInfoList Hash_getInfoList(ptrHash infoHash, int pos);

int* Hash_creatArray(char * line, int sizeOfArray, ptrHash infoHash);

int* Hash_creatArrayForTransfert(char * line, int sizeOfArray, ptrHash infoHash);

int Hash_sortArray(int * arrayOfBucketId, int sizeOfArray);

long int Hash_size(ptrHash infoHash);

#endif /* HASH_H */

