#ifndef __NODE_H__
#define __NODE_H__ 

#include <stdlib.h>
#include <string.h>
#include "vectorlist.h"
#include "node.h"
#include "lowlevelio.h"


typedef struct node* ptrNode;
typedef struct infoVector* ptrInfoVector;

// _____________________________________________________________________________

void Node_create(ptrNode* nodeAccount,char* myName,  double myAmount) ;

void Node_deallocate(ptrNode * nodeAccount);

void Node_print(ptrNode myNode);

//_____________________________________________________________________set + get
double Node_getFirstAmount(ptrNode nodeAccount);

char* Node_getName(ptrNode nodeAccount);

double Node_incomingAndFirestAmount(ptrNode nodeAccount);

double Node_amountInBankAccount(ptrNode nodeAccount);

ptrInfoVector Node_getIncomingVectors(ptrNode nodeAccount);

ptrInfoVector Node_getoutgoingVectors(ptrNode nodeAccount);



#endif