#include "server_parser.h"

int isnumber(char * word) {
    int length = strlen(word);
    int i;
    for (i = 0; i < length; i++) {
        if (!isdigit(word[i])) {
            return 1;
        }
    }
    return 0;
}

int isdouble(char * word) {
    char * end;

    strtod(word, &end);

    if (*end == '\0') {
        return 0;
    } else {
        return 1;
    }
}

void server_parser(ptrHash infoHash, char* line, int fd_com) {

    char templine[5000], secondtempline[5000], thirdtempline[5000], checkMultiAccount[5000], buffer[5000];

    memset(templine, '\0', sizeof (templine));
    memset(secondtempline, '\0', sizeof (secondtempline));
    memset(thirdtempline, '\0', sizeof (thirdtempline));
    memset(checkMultiAccount, '\0', sizeof (checkMultiAccount));
    memset(buffer, '\0', sizeof (buffer));

    strcpy(templine, line); //parser to find the command
    strcpy(secondtempline, line); //parser for find the bukkit
    strcpy(thirdtempline, line); //parser for take result
    strcpy(checkMultiAccount, line); //parser for checking the account if exist

    printf("\ntherdline : (%s)\n", thirdtempline);

    char* word, *word1;
    char name[100];
    char name_des[100];
    double amount, allAccountAmount;
    int delay, ret, i = 0, numberOfAccounts = 0, uniqueAccounts;
    int bufferSize = sizeof (buffer);
    char* save, *save1;
    int* arrayOfBuckit;

    //________________parse and command_________________________________________
    word = strtok_r(templine, " ", &save);
    if (word == NULL) {
        error_argoument(buffer, bufferSize, fd_com);
        return;
    }

    if (!strncmp(word, "add_account", strlen("add_account"))) {
        //__________________check for correct argument__________________________
        word = strtok_r(NULL, " ", &save); //check the amount 
        if (word == NULL) {
            error_argoument(buffer, bufferSize, fd_com);
            return;
        }
        if (isdouble(word) == 0) {
            amount = atof(word);
            if (amount < 0) {
                error_argoument(buffer, bufferSize, fd_com);
                return;
            }
        } else {
            error_argoument(buffer, bufferSize, fd_com);
            return;
        }

        word = strtok_r(NULL, " ", &save);
        if (word == NULL) {
            error_argoument(buffer, bufferSize, fd_com);
            return;
        }
        strcpy(name, word);

        word = strtok_r(NULL, " ", &save);
        if (word == NULL) {
            delay = 0;
        } else {
            if (isnumber(word) == 0) {
                delay = atoi(word);
                if (delay < 0) {
                    error_argoument(buffer, bufferSize, fd_com);
                    return;
                }
            } else {
                error_argoument(buffer, bufferSize, fd_com);
                return;
            }
        }

        //___________find the bukkit to lock____________________________________

        long int key = Hash_function(name);
        int mode = key % Hash_size(infoHash);
        ptrInfoList info = Hash_getInfoList(infoHash, mode);

        //________________lock the bukkit_______________________________________
        printf("befor lockin in add_account\n");
        InfoList_lock(info);
        //______________________________________________________________________

        //__________________protocol____________________________________________
        ret = Hash_Insert(&infoHash, amount, name, delay, info);
        memset(buffer, '\0', sizeof (buffer));
        if (ret == 0) { // true
            sprintf(buffer, "Success.Account creation (%s : %f) [%d]", name, amount, delay);
            writeall_numberOfBytesAndDatas(buffer, fd_com, strlen(buffer));
        } else { // false
            sprintf(buffer, "Error.Account creation failed (%s : %f))", name, amount);
            writeall_numberOfBytesAndDatas(buffer, fd_com, strlen(buffer));
        }
        //_________________unlock the bukkit____________________________________
        InfoList_unlock(info); //unlock the bukkit
        printf("after unlocking in the add_account\n");
        //______________________________________________________________________

    } else if (!strncmp(word, "add_transfer", strlen("add_transfer"))) {
        word = strtok_r(NULL, " ", &save);
        if (word == NULL) {
            error_argoument(buffer, bufferSize, fd_com);
            return;
        }
        if (isdouble(word) == 0) {
            amount = atof(word);
            if (amount < 0) {
                error_argoument(buffer, bufferSize, fd_com);
                return;
            }
        } else {
            error_argoument(buffer, bufferSize, fd_com);
            return;
        }
        //________________name________________ _________________________________
        word = strtok_r(NULL, " ", &save);
        if (word == NULL) {
            error_argoument(buffer, bufferSize, fd_com);
            return;
        }
        strcpy(name, word);

        //________________name_des______________________________________________
        word = strtok_r(NULL, " ", &save);
        if (word == NULL) {
            error_argoument(buffer, bufferSize, fd_com);
            return;
        }
        strcpy(name_des, word);

        //______________________________________________________________________
        word = strtok_r(NULL, " ", &save);
        if (word == NULL) {
            delay = 0;
        } else {
            if (isnumber(word) == 0) {
                delay = atoi(word);
                if (delay < 0) {
                    error_argoument(buffer, bufferSize, fd_com);
                    return;
                }
            } else {
                error_argoument(buffer, bufferSize, fd_com);
                return;
            }
        }

        //_____________________find bukkit______________________________________
        long int key1 = Hash_function(name);
        long int key2 = Hash_function(name_des);
        int mode1 = key1 % Hash_size(infoHash);
        int mode2 = key2 % Hash_size(infoHash);

        //___________________locking ___________________________________________
        printf("befor lockint in add_transfert\n");
        if (mode1 == mode2) {
            //printf("mode1 = %d    ==   mode2 = %d\n", mode1, mode2);
            ptrInfoList info = Hash_getInfoList(infoHash, mode1);
            InfoList_lock(info);
        } else {
            if (mode1 < mode2) {
              //  printf("mod1 = %d    < mode1 = %d \n", mode1, mode2);
                ptrInfoList info1 = Hash_getInfoList(infoHash, mode1);
                InfoList_lock(info1);
                ptrInfoList info2 = Hash_getInfoList(infoHash, mode2);
                InfoList_lock(info2);
            } else {
               // printf("mod1 = %d    > mode1 = %d \n", mode1, mode2);
                ptrInfoList info2 = Hash_getInfoList(infoHash, mode2);
                InfoList_lock(info2);
                ptrInfoList info1 = Hash_getInfoList(infoHash, mode1);
                InfoList_lock(info1);
            }
        }
    

        //__________________________protocol____________________________________
        //______________________________________________________________________
        ret = Hash_AddTran(&infoHash, name, name_des, amount, delay);
        memset(buffer, '\0', sizeof (buffer));
        if (ret == 0) { // true
            sprintf(buffer, "Success.Transfer addition (%s: %s: %f[:%d])", name, name_des, amount, delay);
            writeall_numberOfBytesAndDatas(buffer, fd_com, strlen(buffer));
        } else { // false
            sprintf(buffer, "Error.Transfer addition failed (%s: %s: %f[:%d])", name, name_des, amount, delay);
            writeall_numberOfBytesAndDatas(buffer, fd_com, strlen(buffer));
        }
        //_______________________________________________________________________
   

        if (mode1 == mode2) {
            ptrInfoList info = Hash_getInfoList(infoHash, mode1);
            InfoList_unlock(info);
        } else {
            if (mode1 < mode2) {
                ptrInfoList info1 = Hash_getInfoList(infoHash, mode1);
                InfoList_unlock(info1);
                ptrInfoList info2 = Hash_getInfoList(infoHash, mode2);
                InfoList_unlock(info2);
            } else {
                ptrInfoList info2 = Hash_getInfoList(infoHash, mode2);
                InfoList_unlock(info2);
                ptrInfoList info1 = Hash_getInfoList(infoHash, mode1);
                InfoList_unlock(info1);
            }
        }
        printf("after unlocking in add_transfert\n");

    } else if (!strncmp(word, "print_balance", strlen("print_balance"))) {
        word = strtok_r(NULL, " ", &save);
        if (word == NULL) {
            error_argoument(buffer, bufferSize, fd_com);
            return;
        }
        strcpy(name, word);

        //__________________find bukkit ________________________________________
        long int key = Hash_function(name);
        int mode = key % Hash_size(infoHash);
        ptrInfoList info = Hash_getInfoList(infoHash, mode);

        //__________________lock bukkit ________________________________________
        printf("befor locking in print_balance\n");
        InfoList_lock(info);
        //____________________check channel and take amount_____________________

        ret = Hash_checkAccount(&infoHash, name);
        if (ret == 1) {//false
            sprintf(buffer, "Error.Balance (%s)", name);
            writeall_numberOfBytesAndDatas(buffer, fd_com, strlen(buffer));
        } else {//true
            double bankAmount = Hash_LookUp(infoHash, name);
            sprintf(buffer, "Success.Balance (%s :%f)", name, bankAmount);
            writeall_numberOfBytesAndDatas(buffer, fd_com, strlen(buffer));
        }

        //______________________unlock _________________________________________ 
        InfoList_unlock(info);
        printf("afert unlocking in print_balance\n");

    } else if (!strncmp(word, "add_multi_transfer", strlen("add_multi_transfer"))) {
        //____________________parser____________________________________________
        word = strtok_r(NULL, " ", &save); //parse amount 
        if (word == NULL) {
            error_argoument(buffer, bufferSize, fd_com);
            return;
        }
        amount = atof(word);
        if (amount <= 0) {
            error_argoument(buffer, bufferSize, fd_com);
            return;
        }

        word = strtok_r(NULL, " ", &save); //parse name 
        if (word == NULL) {
            error_argoument(buffer, bufferSize, fd_com);
            return;
        }

        numberOfAccounts++; //number of words in the line 
        while ((word = strtok_r(NULL, " ", &save)) != NULL) { //parse des + time
            if (atoi(word) == 0) {
                numberOfAccounts++;
                delay = 0;
            } else {
                if (isdouble(word) == 0) {
                    amount = atof(word);
                    if (amount < 0) {
                        error_argoument(buffer, bufferSize, fd_com);
                        return;
                    }
                } else {
                    error_argoument(buffer, bufferSize, fd_com);
                    return;
                }
            }
        }
        //_________case not exist destination __________________________________

        if (numberOfAccounts < 2) {
            error_argoument(buffer, bufferSize, fd_com);
            return;
        }


        //_______________create array and sort _________________________________

        arrayOfBuckit = Hash_creatArrayForTransfert(secondtempline, numberOfAccounts, infoHash);
        uniqueAccounts = insertion_Sort(arrayOfBuckit, numberOfAccounts);
        //print_Array(arrayOfBuckit, uniqueAccounts);

        //______________lock the bukkit that need_______________________________
        printf("befor loking in add_multi_transfert\n");
        for (i = 0; i < uniqueAccounts; i++) {
            //printf("add_multi_transfert lock arrayOfBukit[%d]\n", arrayOfBuckit[i]);
            InfoList_lock(Hash_getInfoList(infoHash, arrayOfBuckit[i]));
        }

        int error_transfert = 0;
        word1 = strtok_r(thirdtempline, " ", &save1); //skip command
        word1 = strtok_r(NULL, " ", &save1); //skip amount
        word1 = strtok_r(NULL, " ", &save1); //skip name

        memset(name, '\0', sizeof (name));
        strcpy(name, word1);

        //____________create array for true or false____________________________
        char bufferSuccess[200];
        memset(bufferSuccess, '\0', sizeof (bufferSuccess));
        sprintf(bufferSuccess, "Success.Multi-Transfer addition (src %s:%f[:%d])", name, amount, delay);


        char bufferFaile[200];
        memset(bufferFaile, '\0', sizeof (bufferFaile));
        sprintf(bufferFaile, "Error.Multi-Transfer addition failed (src %s:%f[:%d])", name, amount, delay);


        //_______________check account and amount_______________________________
        if (Hash_checkAccount(&infoHash, name) == 1) {
            error_transfert = 1;
        }


        //_______________find amount of name src _______________________________
        int mode = Hash_function(name) % Hash_size(infoHash);
        ptrInfoList srcBukkit = Hash_getInfoList(infoHash, mode);
        ptrListNode forN = InfoList_SearchIfTheNodeIsInList(srcBukkit, name);
        ptrNode n = ListNode_getDestination(forN);


        allAccountAmount = amount * (numberOfAccounts - 1);
        double bankAccountAmountOfSrc = Node_amountInBankAccount(n);

        if (bankAccountAmountOfSrc < allAccountAmount) {
            error_transfert = 1;
        }

        //________________check all account if exist____________________________

        if (Hash_checkMuliAccount(&infoHash, checkMultiAccount) == 1) {
            error_transfert = 1;
        }
        //______________________________________________________________________
        if (error_transfert == 0) {
            while ((word1 = strtok_r(NULL, " ", &save1)) != NULL) {
                if (atoi(word1) != 0) {
                    break;
                }
                memset(name_des, '\0', sizeof (name_des));
                strcpy(name_des, word1);

                printf("name_src :%s, name_des : %s, amount: %f,delay :%d\n", name, name_des, amount, delay);
                ret = Hash_AddTran(&infoHash, name, name_des, amount, delay);
                if (ret == 1) {
                    error_transfert = 1;
                    break;
                }
            }
        }
        // printf("the error_transfert :%d \n", error_transfert);
        if (error_transfert == 1) {
            writeall_numberOfBytesAndDatas(bufferFaile, fd_com, strlen(bufferFaile));
        } else {
            writeall_numberOfBytesAndDatas(bufferSuccess, fd_com, strlen(bufferSuccess));
        }


        for (i = 0; i < uniqueAccounts; i++) {
            InfoList_unlock(Hash_getInfoList(infoHash, arrayOfBuckit[i]));
        }
        printf("after unlocking add_multi_balance\n");
        free(arrayOfBuckit);
    } else if (!strncmp(word, "print_multi_balance", strlen("print_multi_balance"))) {

        //___parse in tamplane to find the the number of account________________

        numberOfAccounts = 0;
        while ((word = strtok_r(NULL, " ", &save)) != NULL) {
            memset(name_des, '\0', sizeof (name_des));
            strcpy(name_des, word);
            numberOfAccounts++;
        }

        //__________only print_multi_balance error______________________________
        if (numberOfAccounts == 0) {
            error_argoument(buffer, bufferSize, fd_com);
            return;
        }

        //________________array with number that means bukkit___________________

        arrayOfBuckit = Hash_creatArray(secondtempline, numberOfAccounts, infoHash);

        //________________sort and remove duplicate ____________________________

        uniqueAccounts = insertion_Sort(arrayOfBuckit, numberOfAccounts);
        //printArray(arrayOfBuckit, uniqueAccounts);
        
        printf("befor blocking the print_multi_balance\n");
        
        //______________________lock the bukkit_________________________________
        for (i = 0; i < uniqueAccounts; i++) {
            InfoList_lock(Hash_getInfoList(infoHash, arrayOfBuckit[i]));
        }

        char * successmsg = "Success: Multi-balance ";
        char * errormsg = "Error: Multi-balance";
        char successBuffer[5000];
        char errorBuffer[5000];
        char message[5000];
        memset(successBuffer, '\0', sizeof (successBuffer));
        memset(errorBuffer, '\0', sizeof (errorBuffer));
        memset(message, '\0', sizeof (message));

        int error_exists = 0;
        strcpy(successBuffer, "");
        strcpy(errorBuffer, "");

        char* word1 = strtok_r(thirdtempline, " ", &save1); //skip the command
        while ((word1 = strtok_r(NULL, " ", &save1)) != NULL) {
            memset(name_des, '\0', sizeof (name_des));
            strcpy(name_des, word1);

            //____________________check channel and take amount_________________
            ret = Hash_checkAccount(&infoHash, name_des);
            if (ret == 1) {//the node do not exist
                error_exists = 1;
            } else if (error_exists == 0) {
                double bankAmount = Hash_LookUp(infoHash, name_des);
                sprintf(buffer, " (%s:%f) ", name_des, bankAmount);
                strcat(successBuffer, buffer);
            }

            sprintf(buffer, " %s ", name_des);
            strcat(errorBuffer, buffer);
        }

        if (error_exists == 0) {
            strcpy(message, successmsg);
            strcat(message, successBuffer);
        } else {
            strcpy(message, errormsg);
            strcat(message, errorBuffer);
        }

        writeall_numberOfBytesAndDatas(message, fd_com, strlen(message));
        print_Array(arrayOfBuckit, uniqueAccounts);

        //___________________unlock the bukkit__________________________________
        for (i = 0; i < uniqueAccounts; i++) {
            InfoList_unlock(Hash_getInfoList(infoHash, arrayOfBuckit[i]));
        }
        printf("after unlocking the print_multi_balance\n");
        free(arrayOfBuckit);
    } else if (!strncmp(word, "list", strlen("list"))) {//de bug
        Hash_Print(infoHash);
    } else {
        error_argoument(buffer, bufferSize, fd_com);
    }
}

void error_argoument(char* buffer, int bufferSize, int fd_com) {
    memset(buffer, '\0', sizeof (bufferSize));
    char* errorargoumet = "Error.Unknown command";
    sprintf(buffer, "%s", errorargoumet);
    writeall_numberOfBytesAndDatas(buffer, fd_com, strlen(buffer));
}

