#ifndef __CLIENT_PARSER_H__
#define __CLIENT_PARSER_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "lowlevelio.h"



void client_parser(char* line,FILE *fp, int fd_com);

#endif /* CLIENT_PARSER_H */

