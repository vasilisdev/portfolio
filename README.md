1)Thesis. Developed a web application for support basic cloud service
https://bitbucket.org/vasilisdev/portofolio/src/master/Thesis/πτυχιακή-εργασία.pdf

2)Co-Developer of an Airbnb type web application.
https://bitbucket.org/vasilisdev/portofolio/src/master/Airbnb/readme.pdf

3)Co-Developer of a desktop JavaApp and an Android application for monitoring cellphones sensors data changes. Communicate base over MQTT protocol.  
https://bitbucket.org/vasilisdev/portofolio/src/master/Android%20and%20JavaAPP/Andorid/README.md
https://bitbucket.org/vasilisdev/portofolio/src/master/Android%20and%20JavaAPP/JavaApp/README.md

4)Developed a client server web application for support bank transaction.
https://bitbucket.org/vasilisdev/portofolio/src/master/Multithreading/Project1-S16-K24-3.pdf