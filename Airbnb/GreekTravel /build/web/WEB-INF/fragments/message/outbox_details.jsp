<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<link href="${pageContext.request.contextPath}/css/message.css" rel="stylesheet" type="text/css" />

<header>
    <h1>Messages</h1>
</header>


    <div class="mailbox">
        <div class="nav">
            <a href="${pageContext.request.contextPath}/user/message/send_message">compose</a>
            <a href="${pageContext.request.contextPath}/user/message/inbox">inbox</a>
            <a href="${pageContext.request.contextPath}/user/message/outbox" class="active">outbox</a>
        </div>

        <div class="messages">
            <!--<table>-->
            <!--<tr>-->
            <div class="message" style="border-radius: 50px;">
                <span class="sender">to</span>
                <span class="title">topic</span>
                <span class="date">date</span>
                <hr/>
                <hr/>
            </div>

            <!--</tr>-->
            <!--</table>-->
            <c:forEach var="obj" items="${messages}" varStatus="index">
                <form action="${pageContext.request.contextPath}/user/message/delete" method="POST">
                    <div class="message">
                        <span class="delete">
                            <input type="hidden"   name="deleteId"  value="${obj.id}">
                            <input src="/GreekTravel/images/delete.png" class="deleteImg"  type="image" />
                        </span> 
                        <!--<input type="checkbox" />-->
                        <input type ="hidden" value ="${obj.id}" name="id">
                        <span class="sender">${obj.receiverId.nickname}</span>

                        <span class="date"><fmt:formatDate value="${obj.submittedDate}" pattern="MMM d, yyyy" /></span>

                        <a href="${pageContext.request.contextPath}/user/message/message_details?id=${obj.id}" >

                            <span class="title">${obj.topic}</span></a>
                    </div>
                </form>
            </c:forEach>


        </div>
    </div>

</form>

<div id="paginations">

    <ul> 
        <li>
            <c:if test="${currentPage != 1}">    
                <a href="${pageContext.request.contextPath}/user/message/outbox?currentPage=${1}">&laquo;</a>
            </c:if>
        </li>

        <li>
            <c:if test="${currentPage != 1}">    
                <a href="${pageContext.request.contextPath}/user/message/outbox?currentPage=${currentPage - 1}">&lt;</a>
            </c:if>
        </li>

        <c:forEach begin="1" end="${numberOfPages}" var="i">
            <c:choose>
                <c:when test="currentPage eq i">
                    <li>
                        <a href="${pageContext.request.contextPath}/user/message/outbox?currentPage=${i}"></a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li>
                        <a href="${pageContext.request.contextPath}/user/message/outbox?currentPage=${i}">${i}</a>
                    </li>
                </c:otherwise>
            </c:choose>

        </c:forEach>
        <li>
            <c:if test="${currentPage lt numberOfPages}">
                <a href="${pageContext.request.contextPath}/user/message/outbox?currentPage=${currentPage + 1}">&gt;</a>
            </c:if>
        </li>
        <li>
            <c:if test="${currentPage lt numberOfPages}">
                <a href="${pageContext.request.contextPath}/user/message/outbox?currentPage=${numberOfPages}">&raquo;</a>
            </c:if>
        </li>
    </ul>
    <div class="clear">&nbsp;</div>
</div>