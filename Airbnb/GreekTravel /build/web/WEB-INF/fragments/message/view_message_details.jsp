<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<link href="${pageContext.request.contextPath}/css/message.css" rel="stylesheet" type="text/css" />

<header>
    <h1>Messages</h1>
</header>


<form name="admin_view_user" action="${pageContext.request.contextPath}/user/message/reply" method="POST">

    <div class="mailbox">
        <div class="nav">
            <a href="${pageContext.request.contextPath}/user/message/send_message">compose</a>
            <a href="${pageContext.request.contextPath}/user/message/inbox">inbox</a>
            <a href="${pageContext.request.contextPath}/user/message/outbox">outbox</a>
        </div>

        <div class="messages">
            <!--<table>-->
            <!--<tr>-->
            <div class="message" style="border-radius: 50px;">
                <span class="left">from</span>
                <span class="center">${sender}</span>
                <br>
                <span class="left">to</span>
                <span class="center">${receiver}</span>
                <br>
                <span class="left">date</span>
                <span class="center">${date}</span>
                <br>
                <span class="left">topic</span>
                <span class="center">${title}</span>
            </div>


            <div class="message">
                ${message}
            </div>


            <input name="messageId" type="hidden" value="${messageId}"> 

            <div>
                <input class="commitbutton" type="submit" alt="buttonReplay" name="buttonReplay" id="buttonEdit" value="Reply"/>
            </div>       
        </div>
    </div>



</form>
