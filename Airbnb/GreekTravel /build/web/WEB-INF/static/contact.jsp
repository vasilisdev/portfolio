<%@page contentType="text/html" pageEncoding="UTF-8"%>

<head>
    <jsp:include page="/WEB-INF/fragments/common/head.jsp"/>
    <link href="${pageContext.request.contextPath}/css/layout_1.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="wrap"> 
        <jsp:include page="/WEB-INF/fragments/common/topbar.jsp"/>
        <jsp:include page="/WEB-INF/fragments/common/header.jsp"/>
        <jsp:include page="/WEB-INF/fragments/static/contact_details.jsp"/>
        <jsp:include page="/WEB-INF/fragments/common/footer.jsp"/>
    </div>
</body>