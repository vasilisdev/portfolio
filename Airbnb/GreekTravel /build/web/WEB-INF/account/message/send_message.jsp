<head>
    <jsp:include page="/WEB-INF/fragments/common/head.jsp"/>
</head>

<body>
    <div id="wrap">            
        <jsp:include page="/WEB-INF/fragments/common/topbar.jsp"/>

        <jsp:include page="/WEB-INF/fragments/common/header.jsp"/>

        <div id="content">
            <div id="home_main">                    
                <jsp:include page="/WEB-INF/fragments/message/send_message.jsp"/>  
            </div>
            
                        <!--<div id="sidebar" style="min-height: 380px">-->
                <%--<jsp:include page="/WEB-INF/fragments/common/sidebar.jsp"/>--%>                                                                                      
            <!--</div>-->

        </div> 
        <jsp:include page="/WEB-INF/fragments/common/footer.jsp"/>
    </div>
</body>

