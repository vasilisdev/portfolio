package entities;

import entities.Availability;
import entities.Booking;
import entities.Country;
import entities.Location;
import entities.Message;
import entities.Photo;
import entities.RoomType;
import entities.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-10-10T16:29:58")
@StaticMetamodel(Room.class)
public class Room_ { 

    public static volatile SingularAttribute<Room, Boolean> parking;
    public static volatile SingularAttribute<Room, Boolean> pets;
    public static volatile ListAttribute<Room, Photo> photoList;
    public static volatile SingularAttribute<Room, Boolean> tv;
    public static volatile SingularAttribute<Room, Integer> wcNumber;
    public static volatile SingularAttribute<Room, String> description;
    public static volatile SingularAttribute<Room, Country> countryId;
    public static volatile SingularAttribute<Room, Boolean> elevator;
    public static volatile ListAttribute<Room, User> userList;
    public static volatile SingularAttribute<Room, Integer> minimumDays;
    public static volatile SingularAttribute<Room, Boolean> smoking;
    public static volatile SingularAttribute<Room, Integer> bedroomNumber;
    public static volatile SingularAttribute<Room, Integer> id;
    public static volatile SingularAttribute<Room, Boolean> heating;
    public static volatile SingularAttribute<Room, Boolean> kitchen;
    public static volatile SingularAttribute<Room, Integer> bedNumber;
    public static volatile SingularAttribute<Room, Integer> floor;
    public static volatile SingularAttribute<Room, Boolean> events;
    public static volatile SingularAttribute<Room, Double> area;
    public static volatile SingularAttribute<Room, Boolean> wifi;
    public static volatile SingularAttribute<Room, Boolean> refridgerator;
    public static volatile SingularAttribute<Room, Boolean> livingRoom;
    public static volatile SingularAttribute<Room, Boolean> aircondition;
    public static volatile ListAttribute<Room, Location> locationList;
    public static volatile ListAttribute<Room, RoomType> roomTypeList;
    public static volatile ListAttribute<Room, Message> messageList;
    public static volatile ListAttribute<Room, Booking> bookingList;
    public static volatile SingularAttribute<Room, Double> costPerDay;
    public static volatile SingularAttribute<Room, Integer> maxPeople;
    public static volatile SingularAttribute<Room, Double> costPerPerson;
    public static volatile ListAttribute<Room, Availability> availabilityList;

}