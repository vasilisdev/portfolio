package entities;

import entities.Room;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-10-10T16:29:58")
@StaticMetamodel(Availability.class)
public class Availability_ { 

    public static volatile SingularAttribute<Availability, Date> dateTo;
    public static volatile SingularAttribute<Availability, Integer> id;
    public static volatile SingularAttribute<Availability, Date> dateFrom;
    public static volatile SingularAttribute<Availability, Room> roomId;

}