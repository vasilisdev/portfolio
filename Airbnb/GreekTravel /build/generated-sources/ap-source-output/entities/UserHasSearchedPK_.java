package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-10-10T16:29:58")
@StaticMetamodel(UserHasSearchedPK.class)
public class UserHasSearchedPK_ { 

    public static volatile SingularAttribute<UserHasSearchedPK, Integer> id;
    public static volatile SingularAttribute<UserHasSearchedPK, Integer> roomId;

}