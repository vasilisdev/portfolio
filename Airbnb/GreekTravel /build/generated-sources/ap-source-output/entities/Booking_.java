package entities;

import entities.Room;
import entities.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-10-10T16:29:58")
@StaticMetamodel(Booking.class)
public class Booking_ { 

    public static volatile SingularAttribute<Booking, String> ownerComment;
    public static volatile SingularAttribute<Booking, Date> dateTo;
    public static volatile SingularAttribute<Booking, Integer> id;
    public static volatile SingularAttribute<Booking, Date> dateFrom;
    public static volatile SingularAttribute<Booking, Integer> ownerRating;
    public static volatile SingularAttribute<Booking, Integer> roomRating;
    public static volatile SingularAttribute<Booking, User> userId;
    public static volatile SingularAttribute<Booking, String> roomComment;
    public static volatile SingularAttribute<Booking, Room> roomId;

}