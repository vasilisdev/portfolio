package entities;

import entities.Booking;
import entities.Message;
import entities.Photo;
import entities.Role;
import entities.Room;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-10-10T16:29:58")
@StaticMetamodel(User.class)
public class User_ { 

    public static volatile ListAttribute<User, Photo> photoList;
    public static volatile SingularAttribute<User, Integer> active;
    public static volatile ListAttribute<User, Message> messageList1;
    public static volatile ListAttribute<User, Role> roleList;
    public static volatile SingularAttribute<User, String> firstName;
    public static volatile SingularAttribute<User, String> password;
    public static volatile SingularAttribute<User, String> phoneNumber;
    public static volatile ListAttribute<User, Message> messageList;
    public static volatile SingularAttribute<User, String> surname;
    public static volatile ListAttribute<User, Booking> bookingList;
    public static volatile SingularAttribute<User, String> nickname;
    public static volatile SingularAttribute<User, Integer> id;
    public static volatile SingularAttribute<User, String> email;
    public static volatile ListAttribute<User, Room> roomList;

}