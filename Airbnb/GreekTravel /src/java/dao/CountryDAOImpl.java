/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Country;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import jpautils.EntityManagerHelper;

public class CountryDAOImpl implements CountryDAO {

    @Override
    public List<Country> list() {
        EntityManager em = EntityManagerHelper.getEntityManager();
        Query query = em.createNamedQuery("Country.findAll");
        List<Country> countryes = query.getResultList();
        return countryes;
    }

    @Override
    public void create(Country c) {
        EntityManager em = EntityManagerHelper.getEntityManager();

        try {
            EntityTransaction entityTrasacrion = em.getTransaction();
            entityTrasacrion.begin();
            em.persist(c);
            entityTrasacrion.commit();
        } catch (RuntimeException e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    @Override
    public Country findCountryById(int id) {
        EntityManager em = EntityManagerHelper.getEntityManager();
        Object singleResult;
        Country c;

        try {
            EntityTransaction entityTransaction = em.getTransaction();
            entityTransaction.begin();
            Query q = em.createNamedQuery("Country.findById").setParameter("id", id);
            entityTransaction.commit();
            singleResult = q.getSingleResult();
        } catch (NoResultException e) {
            EntityManagerHelper.closeEntityManager();
            return null;
        } catch (RuntimeException e) {
            throw e;
        }

        c = (Country) singleResult;
        EntityManagerHelper.closeEntityManager();
        return c;
    }

    @Override
    public Country findCountryByName(String name) {
        EntityManager em = EntityManagerHelper.getEntityManager();
        Country c = null;
        try {
            EntityTransaction entityTransaction = em.getTransaction();
            entityTransaction.begin();
            Query q = em.createNamedQuery("Country.findByName").setParameter("name", name);
            c = (Country) q.getSingleResult();
            entityTransaction.commit();
        } catch (NoResultException e) {
            c = null;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
        return c;
    }
}
