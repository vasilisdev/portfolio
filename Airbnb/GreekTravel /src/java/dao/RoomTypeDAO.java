package dao;

import entities.RoomType;
import java.util.List;

/**
 *
 * @author psilos
 */
public interface RoomTypeDAO {
    public List <RoomType> list();
    public void create(RoomType rt);
   
}
