package dao;

import entities.UserHasSearched;
import java.util.List;


public interface UserHasSearchedDAO {
    public void create(UserHasSearched u);
    public List<Integer> findByUserId(int userid);
    
   public Boolean checkDuplicateInput(int userid, int roomid);
//    public String remove(int id);
}
