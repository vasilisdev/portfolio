package dao;

import entities.RoomType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import jpautils.EntityManagerHelper;

public class RoomTypeDAOImpl implements RoomTypeDAO {

    @Override
    public List<RoomType> list() {
        EntityManager em = EntityManagerHelper.getEntityManager();
        Query query = em.createNamedQuery("RoomType.findAll");
        List<RoomType> roomType = query.getResultList();
        return roomType;
    }

    @Override
    public void create(RoomType rt) {
        EntityManager em = EntityManagerHelper.getEntityManager();
        try {
            EntityTransaction entityTrasacrion = em.getTransaction();
            entityTrasacrion.begin();
            em.persist(rt);
            entityTrasacrion.commit();
        } catch (RuntimeException e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

}
