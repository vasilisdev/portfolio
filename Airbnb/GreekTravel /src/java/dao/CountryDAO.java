/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Country;
import java.util.List;

/**
 *
 * @author psilos
 */
public interface CountryDAO {

    List<Country> list();

    public void create(Country c);

    public Country findCountryById(int id);
    
    public Country findCountryByName(String name);
}
