package controllers.rooms;

import dao.BookingDAO;
import dao.BookingDAOImpl;
import dao.RoomDAO;
import dao.RoomDAOImpl;
import dao.UserDAO;
import dao.UserDAOImpl;
import dao.UserHasRatedDAO;
import dao.UserHasRatedDAOImpl;
import dao.UserHasSearchedDAOImpl;
import entities.Role;
import entities.Room;
import entities.User;
import entities.UserHasRated;
import entities.UserHasSearched;
import entities.UserHasSearchedPK;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import validation.PositiveIntegerValidator;

//validate input
public class ViewSearchRoomDetails extends controllers.Controller {

    private final String target_success = "/WEB-INF/account/owner/room_details.jsp";
    private final String target_error = "/WEB-INF/error/endoftheworld.jsp";

    private RoomDAO rd = new RoomDAOImpl();
    private UserDAO ud = new UserDAOImpl();
    private BookingDAO bd = new BookingDAOImpl();
    private UserHasRatedDAO uhRateddao = new UserHasRatedDAOImpl();
    private UserHasSearchedDAOImpl uhSearchedao = new UserHasSearchedDAOImpl();
    private UserHasRated uhr =  null;

    private User sessionuser = null;
    private String id = null;
    private Integer avgOfTheRoom ;
    private Integer reviewsOfTheRoom ;
    private List<Integer> ownerOfTheRoomAvg = null;

    private List<Room> list = null;
    private User ownerOfTheRoom = null;

    @Override
    protected boolean validate_input(Map<String, String[]> map) {
        
        PositiveIntegerValidator idValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (idValidator.validate(map, "Room_id")) {
            System.out.println("Correct roomId :" + correctMap.get("Room_id"));
        }
        System.out.println("input");
        return errorMap.isEmpty();
    }

    @Override
    protected boolean validate_logic() {
        System.out.println("logic");
        return true;
    }

    @Override
    protected boolean try_execute() {
        try {

            id = correctMap.get("Room_id");
            int roomId = Integer.parseInt(id);

            HttpSession session = request.getSession();
            sessionuser = (User) session.getAttribute("sessionuser");

            ownerOfTheRoom = ud.findUserByRoomId(roomId);
            if(ownerOfTheRoom == null){
                return false;
            }

            if (sessionuser == null) {
                request.setAttribute("isguest", true);//gest
            } else {

                if (sessionuser.getId().equals(ownerOfTheRoom.getId())) {  // if sessionUser is room's owner edit option
                    request.setAttribute("roomsOwner", true);
                } else {
                    request.setAttribute("book", true);

                    List<Role> roleList = ownerOfTheRoom.getRoleList();
                    for (Role r : roleList) {
                        if (r.getId() == 2) { // owner
                            request.setAttribute("isowner", true);
                        }
                        if (r.getId() == 3) { // visitor
                            request.setAttribute("isvisitor", true);
                        }
                    }
                }
            }
            
            //edw xriazete elenxos an ena apo ta stixia tou pinaka einia null
            list = rd.findRoomById(roomId);
            avgOfTheRoom = bd.findAvgRating(roomId); 
            reviewsOfTheRoom = bd.findReviews(roomId);
            int ownerid = ownerOfTheRoom.getId();
            ownerOfTheRoomAvg = bd.findAvgRatingOfOwner(ownerid);

            //configure UserHasSearched table for recommandation
            if (sessionuser != null) {

                Boolean checkDuplicateInput = uhSearchedao.checkDuplicateInput(sessionuser.getId(), roomId);
                if( checkDuplicateInput == (false)){
                    return true;
                }
                
                uhr = uhRateddao.findUserHasRatedById(sessionuser.getId());
                if(uhr == null){ //efoswn kanw kainourgia register dn xreiazetai
                    return true;
                }
                
                if (uhr.getHasRated() != true) {
                    UserHasSearched uhs = new UserHasSearched();
                    UserHasSearchedPK uhsPK = new UserHasSearchedPK();
                    uhsPK.setId(sessionuser.getId());
                    uhsPK.setRoomId(roomId);
                    uhs.setUserHasSearchedPK(uhsPK);
                    uhSearchedao.create(uhs);
                }
            }

            return true;

        } catch (Exception e) {
            System.out.println("Error in execute" + e.getMessage());
            return false;
        }

    }

    @Override
    protected void on_success() {
        request.setAttribute("avgOfTheRoom", avgOfTheRoom);
        request.setAttribute("reviewsOfTheRoom", reviewsOfTheRoom);
        request.setAttribute("ownerOfTheRoomAvg", ownerOfTheRoomAvg);
        request.setAttribute("User", ownerOfTheRoom);
        request.setAttribute("list", list);

        redirect(target_success);
    }

    @Override
    protected void on_error() {
        redirect(target_error);
    }

}

