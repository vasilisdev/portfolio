package controllers.rooms;

import dao.CountryDAO;
import dao.CountryDAOImpl;
import dao.RoomDAO;
import dao.RoomDAOImpl;
import entities.Availability;
import entities.Booking;
import entities.Country;
import entities.Location;
import entities.Message;
import entities.Photo;
import entities.Room;
import entities.RoomType;
import entities.User;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import validation.AddressValidator;
import validation.CountryNameValidator;
import validation.DateValidator;
import validation.DoublePositiveNegativeValidator;
import validation.DoublePositiveUperToZero;
import validation.PositiveIntegerValidator;
import validation.TextValidator;
import validation.TrueFalseValidator;
import validation.TypeValidator;

public class CreateRoomController extends controllers.Controller {

    private final String target_error = "/WEB-INF/account/owner/create.jsp";
    private final String target_success = "/WEB-INF/error/action_result.jsp";

    private RoomDAO rdao = new RoomDAOImpl();
    private CountryDAO cd = new CountryDAOImpl();

    private Country c = null;

    private User sessionuser = null;
    private int minimumDays = 0;
    private int wcNumber = 0;
    private int bedNum = 0;
    private int bedroomNum = 0;
    private Integer floor = 0;
    private int maxPeople = 0;
    private double costPerDay = 0;
    private double costPerPerson = 0;
    private double area = 0;
    private double latitude = 0;
    private double longitude = 0;
    private Boolean wifi = null;
    private Boolean rfr = null;
    private Boolean parking = null;
    private Boolean kitchen = null;
    private Boolean elevator = null;
    private Boolean ac = null;
    private Boolean heating = null;
    private Boolean tv = null;
    private Boolean livingRoom = null;
    private Boolean smoking = null;
    private Boolean pets = null;
    private Boolean events = null;

    @Override
    protected boolean validate_input(Map<String, String[]> map) {

        DoublePositiveNegativeValidator doublev = new DoublePositiveNegativeValidator(errorMap, correctMap);
        DoublePositiveUperToZero dpv = new DoublePositiveUperToZero(errorMap, correctMap);
        TypeValidator tpv = new TypeValidator(errorMap, correctMap);
        CountryNameValidator cnv = new CountryNameValidator(errorMap, correctMap);
        PositiveIntegerValidator piv = new PositiveIntegerValidator(errorMap, correctMap);
        DateValidator dv = new DateValidator(errorMap, correctMap);
        AddressValidator av = new AddressValidator(errorMap, correctMap);
        TrueFalseValidator tfv = new TrueFalseValidator(errorMap, correctMap);
        TextValidator textv = new TextValidator(errorMap, correctMap);

        //Date format
        dv.validate(map, "dateFrom");
        dv.validate(map, "dateTo");

        //positiv integer
        piv.validate(map, "floor");
        piv.validate(map, "minimumDays");
        piv.validate(map, "wcNumber");
        piv.validate(map, "bedNumber");
        piv.validate(map, "bedroomNumber");
        piv.validate(map, "maxPeople");

        //True false
        tfv.validate(map, "kitchen");
        tfv.validate(map, "heating");
        tfv.validate(map, "tv");
        tfv.validate(map, "livingRoom");
        tfv.validate(map, "kitchen");
        tfv.validate(map, "elevator");
        tfv.validate(map, "aircondition");
        tfv.validate(map, "wifi");
        tfv.validate(map, "refridgerator");
        tfv.validate(map, "parking");
        tfv.validate(map, "smoking");
        tfv.validate(map, "pets");
        tfv.validate(map, "events");

        //Country name validate
        cnv.validate(map, "country");
        cnv.validate(map, "city");
        cnv.validate(map, "distrinct");

        //addres Validate
        av.validate(map, "address");
        av.validate(map, "postcode");

        //double_positive
        dpv.validate(map, "costPerDay");
        dpv.validate(map, "costPerPerson");
        dpv.validate(map, "area");
        //double_positive_and_negative
        doublev.validate(map, "latitude");
        doublev.validate(map, "longitude");

        tpv.validate(map, "type");

        //textValidator
        textv.validate(map, "comment");
        textv.validate(map, "description");

        //To do validate an to size einia megalitero
        if (map.get("photo")[0].isEmpty() || map.get("photo")[0] == null) {
            errorMap.addError("photo", "missing");
        } else {
            if (map.get("photo")[0].length() >= 4000) {
                errorMap.addError("photo", "Add onother photo big size");
            } else {
                correctMap.put("photo", map.get("photo")[0]);
            }
        }
        
        if(errorMap.isEmpty()){
            System.out.println("empty");
            return true;
        }else{
            System.out.println("full");
            return false;
        }
      
    }

    @Override
    protected boolean validate_logic() {

        String countryName = correctMap.get("country");
        c = cd.findCountryByName(countryName);
        if (c == null) {
            errorMap.addError("country", "Country do not exist");
        }
     
        return errorMap.isEmpty();
    }

    @Override
    protected boolean try_execute() {
         System.out.println("Meta apo to validation logic");
        try {

            area = Double.parseDouble(correctMap.get("area"));
            latitude = Double.parseDouble(correctMap.get("latitude"));
            longitude = Double.parseDouble(correctMap.get("longitude"));
            costPerDay = Double.parseDouble(correctMap.get("costPerDay"));
            costPerPerson = Double.parseDouble(correctMap.get("costPerPerson"));

            minimumDays = Integer.parseInt(correctMap.get("minimumDays"));
            floor = Integer.parseInt(correctMap.get("floor"));
            bedNum = Integer.parseInt(correctMap.get("bedNumber"));
            bedroomNum = Integer.parseInt(correctMap.get("bedroomNumber"));
            wcNumber = Integer.parseInt(correctMap.get("wcNumber"));
            maxPeople = Integer.parseInt(correctMap.get("maxPeople"));

            events = Boolean.valueOf(correctMap.get("events"));
            livingRoom = Boolean.valueOf(correctMap.get("livingRoom"));
            smoking = Boolean.valueOf(correctMap.get("smoking"));
            pets = Boolean.valueOf(correctMap.get("pets"));
            ac = Boolean.valueOf(correctMap.get("aircondition"));
            heating = Boolean.valueOf(correctMap.get("heating"));
            tv = Boolean.valueOf(correctMap.get("tv"));
            parking = Boolean.valueOf(correctMap.get("parking"));
            kitchen = Boolean.valueOf(correctMap.get("kitchen"));
            elevator = Boolean.valueOf(correctMap.get("elevator"));
            rfr = Boolean.valueOf(correctMap.get("refridgerator"));
            wifi = Boolean.valueOf(correctMap.get("wifi"));

            //------- configure photo
            List<Photo> photoList = new ArrayList<>();
            Photo p = new Photo();
            p.setPhotographUrl(correctMap.get("photo"));
            photoList.add(p);

            //------- configure type
            List<RoomType> roomTypeList = new ArrayList<>();
            switch (correctMap.get("type")) {
                case "Private room":
                    RoomType rt1 = new RoomType(1);
                    roomTypeList.add(rt1);
                    break;
                case "Shared room":
                    RoomType rt2 = new RoomType(2);
                    roomTypeList.add(rt2);
                    break;
                case "House":
                    RoomType rt3 = new RoomType(3);
                    roomTypeList.add(rt3);
                    break;
                case "Villa":
                    RoomType rt4 = new RoomType(4);
                    roomTypeList.add(rt4);
                    break;
                default:
                    break;
            }
            //????????????????????/nomizo mono to id thes 
            //------- configure user
            List<User> userList = new ArrayList<>();
            User u = new User();
            HttpSession session = request.getSession();
            sessionuser = (User) session.getAttribute("sessionuser");
            u.setActive(sessionuser.getActive());
            u.setEmail(sessionuser.getEmail());
            u.setFirstName(sessionuser.getFirstName());
            u.setNickname(sessionuser.getNickname());
            u.setSurname(sessionuser.getSurname());
            u.setPassword(sessionuser.getPassword());
            u.setPhoneNumber(sessionuser.getPhoneNumber());
            u.setId(sessionuser.getId());
            userList.add(u);

            //Nomizw ayta den xriazonte i oxi null to minima 
            List<Message> messageList = new ArrayList<>();
            Message m = new Message();
            m = null;
            messageList.add(m);
            List<Booking> bookingList = new ArrayList<>();
            Booking b = new Booking();
            b = null;
            bookingList.add(b);

            //------- configure room
            List<Room> roomList = new ArrayList<>();// nomizw oute ayto xriazete kai to pio katw pou bazis to add stin lista
            Room r = new Room();
            r.setAircondition(ac);
            r.setArea(area);
            r.setBedNumber(bedNum);
            r.setBedroomNumber(bedroomNum);
            r.setCostPerDay(costPerDay);
            r.setCostPerPerson(costPerPerson);
            r.setDescription(correctMap.get("description"));
            r.setDescription(correctMap.get("comment"));

            r.setElevator(elevator);
            r.setEvents(events);
            r.setFloor(floor);
            r.setHeating(heating);
            r.setKitchen(kitchen);
            r.setLivingRoom(livingRoom);
            r.setMaxPeople(maxPeople);
            r.setMinimumDays(minimumDays);
            r.setParking(parking);
            r.setPets(pets);
            r.setRefridgerator(rfr);
            r.setSmoking(smoking);
            r.setTv(tv);
            r.setWcNumber(wcNumber);
            r.setWifi(wifi);

            r.setBookingList(bookingList);
            r.setMessageList(messageList);
            r.setPhotoList(photoList);
            r.setRoomTypeList(roomTypeList);
            r.setUserList(userList);

            //------- configure country
            Country newc = new Country();
            newc.setId(c.getId());
            r.setCountryId(newc);
            roomList.add(r);

            //------- configure location
            List<Location> locationList = new ArrayList<>(); // to idi kai ayto edw den nomizw oti to xrisimopis
            Location l = new Location();
            l.setAddress(correctMap.get("address"));
            l.setCity(correctMap.get("city"));
            l.setDistrinct(correctMap.get("distrinct"));
            l.setLatitude(latitude);
            l.setLongitude(longitude);
            l.setAccessComments(correctMap.get("comment"));
            l.setPostcode(correctMap.get("postcode"));
            locationList.add(l); //+++++++++++++++++++++++++++=kai edw 

            //------- configure availability
            List<Availability> availabilityList = new ArrayList<>(); //oute ayto den to xrisimopis
            Availability av = new Availability();
            DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
            Date startDate = (Date) formatter.parse(correctMap.get("dateFrom"));
            Date endDate = (Date) formatter.parse(correctMap.get("dateTo"));
            av.setDateFrom(startDate);
            av.setDateTo(endDate);
            availabilityList.add(av);///oute ayto

            rdao.create(r, av, l);

            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    protected void on_success() {
        request.setAttribute("createRoom", true);
        redirect(target_success);
    }

    @Override
    protected void on_error() {
        redirect(target_error);
    }
}
