package controllers.rooms;

import entities.User;
import java.util.Map;
import javax.servlet.http.HttpSession;
public class BookingController extends controllers.Controller{
    
    private final String target_error ="/WEB-INF/error/endoftheworld.jsp";
    private final String target_success ="/WEB-INF/fragments/Booking.jsp";
    
    private String roomId = null;
    private String ownerId = null;
    private User sessionuser = null;
    

    @Override
    protected boolean validate_input(Map<String, String[]> map) {
        return true;
    }

    @Override
    protected boolean validate_logic() {
        return true;
    }

    @Override
    protected boolean try_execute() {
    
        try {
            ownerId = map.get("ownerId")[0];
            roomId = map.get("roomId")[0];
            
            HttpSession session = request.getSession();
            sessionuser = (User) session.getAttribute("sessionuser");
            return true;
        } catch (Exception e) {

            return false;
        }
    }

    @Override
    protected void on_success() {
        request.setAttribute("roomId", roomId);
        request.setAttribute("ownerId", ownerId);
        request.setAttribute("sessionuser", sessionuser);
        redirect(target_success);
    }

    @Override
    protected void on_error() {
        redirect(target_error);
    }
}
