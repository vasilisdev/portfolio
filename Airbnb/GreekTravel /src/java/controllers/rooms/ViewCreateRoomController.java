package controllers.rooms;


import java.util.Map;
    
public class ViewCreateRoomController extends controllers.Controller {
    
    private final String target_error = "/WEB-INF/error/endoftheworld.jsp";
    private final String target_success = "/WEB-INF/account/owner/create.jsp";

    @Override
    protected boolean validate_input(Map<String, String[]> map) {
        return errorMap.isEmpty();
    }

    @Override
    protected boolean validate_logic() {
        return true;
    }

    @Override
    protected boolean try_execute() {
    
        try {
            
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    protected void on_success() {
        redirect(target_success);
    }

    @Override
    protected void on_error() {
        redirect(target_error);
    }
}
