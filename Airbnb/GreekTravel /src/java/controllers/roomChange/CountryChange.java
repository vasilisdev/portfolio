package controllers.roomChange;

import dao.CountryDAO;
import dao.CountryDAOImpl;
import dao.RoomDAO;
import dao.RoomDAOImpl;
import dao.UserDAO;
import dao.UserDAOImpl;
import entities.Country;
import entities.Room;
import entities.User;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import validation.CountryNameValidator;
import validation.PositiveIntegerValidator;

public class CountryChange extends controllers.Controller {

    private final String target = "/WEB-INF/report/roomupdate.jsp";
    private final String target_error = "/WEB-INF/account/owner/edit.jsp";

    private RoomDAO rd = new RoomDAOImpl();
    private CountryDAO cd = new CountryDAOImpl();
    private UserDAO ud = new UserDAOImpl();

    private Country c = null;
    private User u = null;
    int id;

    @Override
    protected boolean validate_input(Map<String, String[]> map) {
        CountryNameValidator countryName = new CountryNameValidator(errorMap, correctMap);
        if (countryName.validate(map, "country")) {
            System.out.println("County Validat : " + correctMap.get("country"));
        }

        PositiveIntegerValidator idValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (idValidator.validate(map, "id")) {
            System.out.println("Correct id :" + correctMap.get("id"));
        }

        try {
            id = Integer.parseInt(correctMap.get("id"));
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            return false;
        }

        return errorMap.isEmpty();
    }

    @Override
    protected boolean validate_logic() {

        String countryName = correctMap.get("country");
        c = cd.findCountryByName(countryName);
        if (c == null) {
            errorMap.addError("country", "Country do not exist");
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        Boolean editAvailability = rd.editAvailability(id, date);
        if (editAvailability) {
            return true;
        } else {
            errorMap.addError("countryA", "You can't edit your room unless it is available");
        }
        return errorMap.isEmpty();
    }

    @Override
    protected boolean try_execute() {
        Room r = new Room();
        try {

            Country c = new Country();
            c.setName(correctMap.get("country"));
            r.setCountryId(c);
            r.setId(Integer.parseInt(correctMap.get("id")));
            boolean ok = rd.updateCountry(r);
            return ok;
        } catch (Exception e) {
            System.out.println("Exeption country " + e.getMessage());
            return false;
        }

    }

    @Override
    protected void on_success() {
        redirect(target);
    }

    @Override
    protected void on_error() {
        Room roomThatWillByChange = (Room) rd.findAllCharacteristicsById(id);
        request.setAttribute("roomThatWillByChange", roomThatWillByChange);
        redirect(target_error);
    }

}
