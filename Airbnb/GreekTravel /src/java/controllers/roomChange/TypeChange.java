package controllers.roomChange;

import dao.RoomDAO;
import dao.RoomDAOImpl;
import dao.UserDAO;
import dao.UserDAOImpl;
import entities.Room;
import entities.RoomType;
import entities.User;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import validation.PositiveIntegerValidator;
import validation.TypeValidator;

public class TypeChange extends controllers.Controller {

    private final String target = "/WEB-INF/report/roomupdate.jsp";
    private final String target_error = "/WEB-INF/account/owner/edit.jsp";

    private RoomDAO rd = new RoomDAOImpl();
   
    int id;

    @Override
    protected boolean validate_input(Map<String, String[]> map) {

        PositiveIntegerValidator idValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (idValidator.validate(map, "id")) {
            System.out.println("Correct id : " + correctMap.get("id"));
        }

        try {
            id = Integer.parseInt(correctMap.get("id"));
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            return false;
        }

        TypeValidator typeValidator = new TypeValidator(errorMap, correctMap);
        if (typeValidator.validate(map, "type")) {
            System.out.println("Correct type : " + correctMap.get("type"));
        }

        return errorMap.isEmpty();
    }

    @Override
    protected boolean validate_logic() {
        //pernw to id

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();

        Boolean editAvailability = rd.editAvailability(id, date);
        if (editAvailability) {
            return true;
        } else {
            errorMap.addError("type", "You can't edit your room unless it is available");
        }

        return errorMap.isEmpty();
    }

    @Override
    protected boolean try_execute() {
        Room r = new Room();

        try {

            List<RoomType> list = new ArrayList<>();
            RoomType roomType = new RoomType();
            roomType.setType(correctMap.get("type"));
            list.add(roomType);

            r.setRoomTypeList(list);
            r.setId(id);

            return rd.updateTypeRoom(r);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    protected void on_success() {
        redirect(target);
    }

    @Override
    protected void on_error() {
        Room roomThatWillByChange = (Room) rd.findAllCharacteristicsById(id);
        request.setAttribute("roomThatWillByChange", roomThatWillByChange);
        redirect(target_error);
    }

}
