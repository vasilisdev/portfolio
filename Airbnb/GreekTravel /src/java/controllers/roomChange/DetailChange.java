package controllers.roomChange;

import dao.RoomDAO;
import dao.RoomDAOImpl;
import entities.Room;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import validation.DoublePositiveUperToZero;
import validation.PositiveIntegerValidator;
import validation.TrueFalseValidator;


public class DetailChange extends controllers.Controller {

    private final String target = "/WEB-INF/report/roomupdate.jsp";
    private final String target_error = "/WEB-INF/account/owner/edit.jsp";
 
    private RoomDAO rd = new RoomDAOImpl();
    private int id;

    @Override
    protected boolean validate_input(Map<String, String[]> map) {

        PositiveIntegerValidator idValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (idValidator.validate(map, "id")) {
            System.out.println("Correct id :" + correctMap.get("id"));
        }

        try {
            id = Integer.parseInt(correctMap.get("id"));
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            return false;
        }

        //________________________________________________________________double
        DoublePositiveUperToZero aeraValidation = new DoublePositiveUperToZero(errorMap, correctMap);
        if (aeraValidation.validate(map, "area")) {
            System.out.println("Validate area : " + correctMap.get("area"));
        }

        DoublePositiveUperToZero costPerDayValidator = new DoublePositiveUperToZero(errorMap, correctMap);
        if (costPerDayValidator.validate(map, "costPerDay")) {
            System.out.println("Validate costPerDay : " + correctMap.get("costPerDay"));
        }

        DoublePositiveUperToZero costPerPersonValidator = new DoublePositiveUperToZero(errorMap, correctMap);
        if (costPerPersonValidator.validate(map, "costPerPerson")) {
            System.out.println("Validate costPerPerson : " + correctMap.get("costPerPerson"));
        }
        //_______________________________________________________________integer
        PositiveIntegerValidator bedNumberValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (bedNumberValidator.validate(map, "bedNumber")) {
            System.out.println("Correct bedNumber : " + correctMap.get("bedNumber"));
        }

        PositiveIntegerValidator bedroomNumberValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (bedroomNumberValidator.validate(map, "bedroomNumber")) {
            System.out.println("Correct bedroomNumberValidator : " + correctMap.get("bedroomNumber"));
        }

        PositiveIntegerValidator wcNumberValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (wcNumberValidator.validate(map, "wcNumber")) {
            System.out.println("Correct wcNumber : " + correctMap.get("wcNumber"));
        }

        PositiveIntegerValidator minimumDaysValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (minimumDaysValidator.validate(map, "minimumDays")) {
            System.out.println("Correct minimumDays : " + correctMap.get("minimumDays"));
        }
        PositiveIntegerValidator maxPeopleValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (maxPeopleValidator.validate(map, "maxPeople")) {
            System.out.println("Correct maxPeople : " + correctMap.get("maxPeople"));
        }

        //_____________________________________________________________boolean
        TrueFalseValidator wifiValidator = new TrueFalseValidator(errorMap, correctMap);
        if (wifiValidator.validate(map, "wifi")) {
            System.out.println("Correct wifi : " + correctMap.get("wifi"));
        }

        TrueFalseValidator parkingValidator = new TrueFalseValidator(errorMap, correctMap);
        if (parkingValidator.validate(map, "parking")) {
            System.out.println("Correct parking : " + correctMap.get("parking"));
        }

        TrueFalseValidator elevatorValidator = new TrueFalseValidator(errorMap, correctMap);
        if (elevatorValidator.validate(map, "elevator")) {
            System.out.println("Correct elevator : " + correctMap.get("elevator"));
        }

        TrueFalseValidator kitchenValidator = new TrueFalseValidator(errorMap, correctMap);
        if (kitchenValidator.validate(map, "kitchen")) {
            System.out.println("Correct kitchen : " + correctMap.get("kitchen"));
        }

        TrueFalseValidator airconfitionValidator = new TrueFalseValidator(errorMap, correctMap);
        if (airconfitionValidator.validate(map, "aircondition")) {
            System.out.println("Correct aircondition : " + correctMap.get("aircondition"));
        }

        TrueFalseValidator heatingValidator = new TrueFalseValidator(errorMap, correctMap);
        if (heatingValidator.validate(map, "heating")) {
            System.out.println("Correct heating : " + correctMap.get("heating"));
        }

        TrueFalseValidator tvValidator = new TrueFalseValidator(errorMap, correctMap);
        if (tvValidator.validate(map, "tv")) {
            System.out.println("Correct tv : " + correctMap.get("tv"));
        }

        TrueFalseValidator livingRoomValidator = new TrueFalseValidator(errorMap, correctMap);
        if (livingRoomValidator.validate(map, "livingRoom")) {
            System.out.println("Correct livingRoom : " + correctMap.get("livingRoom"));
        }

        TrueFalseValidator smokingValidator = new TrueFalseValidator(errorMap, correctMap);
        if (smokingValidator.validate(map, "smoking")) {
            System.out.println("Correct smoking : " + correctMap.get("smoking"));
        }

        TrueFalseValidator petsValidator = new TrueFalseValidator(errorMap, correctMap);
        if (petsValidator.validate(map, "pets")) {
            System.out.println("Correct pets : " + correctMap.get("pets"));
        }

        TrueFalseValidator eventsValidator = new TrueFalseValidator(errorMap, correctMap);
        if (eventsValidator.validate(map, "events")) {
            System.out.println("Correct events : " + correctMap.get("events"));
        }

        TrueFalseValidator refridgeratorValidator = new TrueFalseValidator(errorMap, correctMap);
        if (refridgeratorValidator.validate(map, "refridgerator")) {
            System.out.println("Correct refridgerator : " + correctMap.get("refridgerator"));
        }

        if (map.get("description")[0].isEmpty() || map.get("description")[0] == null) {
            correctMap.put("description", "No description recored");
        } else {
            if (map.get("description")[0].length() >= 500) {
                errorMap.addError("description", "To many charachter");
            } else {
                correctMap.put("description", map.get("description")[0].trim());
            }
        }

        if ( (map.get("floor") == null || map.get("floor")[0].isEmpty()) || map.get("floor")[0] == null) {
            correctMap.put("floor", "1");
        } else {
            PositiveIntegerValidator floorValidator = new PositiveIntegerValidator(errorMap, correctMap);
            if (floorValidator.validate(map, "floor")) {
                System.out.println("Correct floor : " + correctMap.get("floor"));
            }
        }

       // System.out.println("Description : " + correctMap.get("description"));

        return errorMap.isEmpty();
    }

    @Override
    protected boolean validate_logic() {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();

        Boolean editAvailability = rd.editAvailability(id, date);
        if (editAvailability) {
            return true;
        } else {
            errorMap.addError("detail", "You can't edit your room unless it is available");
        }
        return errorMap.isEmpty();

    }

    @Override
    protected boolean try_execute() {
        Room r = new Room();
        try {

            //System.out.println("1 elevetor "+  correctMap.get("elevator"));
            r.setId(Integer.parseInt(correctMap.get("id")));
            //boolean
            r.setWifi(Boolean.parseBoolean(correctMap.get("wifi")));
            r.setParking(Boolean.parseBoolean(correctMap.get("parking")));
            r.setElevator(Boolean.parseBoolean(correctMap.get("elevator")));
            r.setKitchen(Boolean.parseBoolean(correctMap.get("kitchen")));
            r.setAircondition(Boolean.parseBoolean(correctMap.get("aircondition")));
            r.setHeating(Boolean.parseBoolean(correctMap.get("heating")));
            r.setTv(Boolean.parseBoolean(correctMap.get("tv")));
            r.setLivingRoom(Boolean.parseBoolean(correctMap.get("livingRoom")));
            r.setSmoking(Boolean.parseBoolean(correctMap.get("smoking")));
            r.setPets(Boolean.parseBoolean(correctMap.get("pets")));
            r.setEvents(Boolean.parseBoolean(correctMap.get("events")));
            r.setRefridgerator(Boolean.parseBoolean(correctMap.get("refridgerator")));

            //double
            r.setArea(Double.parseDouble(correctMap.get("area")));
            r.setCostPerDay(Double.parseDouble(correctMap.get("costPerDay")));
            r.setCostPerPerson(Double.parseDouble(correctMap.get("costPerPerson")));

            //int
            r.setBedNumber(Integer.parseInt(correctMap.get("bedNumber")));
            r.setBedroomNumber(Integer.parseInt(correctMap.get("bedroomNumber")));
            r.setWcNumber(Integer.parseInt(correctMap.get("wcNumber")));
            r.setMinimumDays(Integer.parseInt(correctMap.get("minimumDays")));
            r.setMaxPeople(Integer.parseInt(correctMap.get("maxPeople")));

            //SOS  apotinxani o parser gt re gamoto 
            r.setFloor(Integer.parseInt(correctMap.get("floor")));
            r.setDescription(correctMap.get("description"));

            //System.out.println("2 elevetor "+ r.getElevator());
            boolean ok = rd.updateDetails(r);

            return ok;
        } catch (Exception e) {
            System.out.println("Exeption " + e.getMessage());
            return false;
        }

    }

    @Override
    protected void on_success() {
        redirect(target);
    }

    @Override
    protected void on_error() {
        Room roomThatWillByChange = (Room) rd.findAllCharacteristicsById(id);
        request.setAttribute("roomThatWillByChange", roomThatWillByChange);
        redirect(target_error);
    }

}
