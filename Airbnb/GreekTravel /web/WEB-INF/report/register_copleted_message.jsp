<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<body>

    <div id="w">
        <div id="content1">
            <div class="notify successbox">
                <h1>Success!</h1>
                <span class="alerticon"><img src="http://s22.postimg.org/i5iji9hv1/check.png" alt="success"/></span>
                <p>You have successfully registered to our site</p>
                <p>If you requested owner priviledges, please wait for Admin verification.</p>  
            </div>
        </div>
    </div>
</body>