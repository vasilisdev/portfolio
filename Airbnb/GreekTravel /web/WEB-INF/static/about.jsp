<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <head>
        <jsp:include page="/WEB-INF/fragments/common/head.jsp"/>
    </head>
    <body>
        <div id="wrap"> 
            <jsp:include page="/WEB-INF/fragments/common/topbar.jsp"/>
            <jsp:include page="/WEB-INF/fragments/common/header.jsp"/> 
            <div id="aboutus">
                <jsp:include page="/WEB-INF/fragments/static/developers_details.jsp"/>                                              
            </div>
                <jsp:include page="/WEB-INF/fragments/common/footer.jsp"/>
        </div>
    </body>
