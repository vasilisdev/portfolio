

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<h1>5 Room Villa at PlaceName</h1>
<div id="single_item_details">
    <div id="leftcolumn"><img src="${pageContext.request.contextPath}/images/imageholder_detailspage.jpg" alt="" width="220" height="220" class="previewimg" /> </div>
    <div id="rightcolumn">
        <h2>New Condo house, modern light &amp; garden. 50 Acres</h2>
        <p class="user"><img src="${pageContext.request.contextPath}/images/usericon.gif" alt="" /> Posted by <a href="#">Chris002</a></p>
        <p>Ref# :CO1 <br />
            Posted On : Thursday 05th of February 2009</p>
        <p>&nbsp;</p>
        <p class="price">Offered at: $225,000</p>
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Save This</a></li>
                <li><a href="#tabs-2">Send This</a></li>
                <li><a href="#tabs-3">Report This</a></li>
            </ul>
            <div id="tabs-1" class="hiddentab">
                <p><img src="${pageContext.request.contextPath}/images/fav.gif" alt="" width="18" height="13" />&nbsp;<a href="#">To My Favorites</a></p>
                <p><img src="${pageContext.request.contextPath}/images/emailalert.gif" alt="" width="18" height="15" />&nbsp;<a href="#">To Email Alerts</a></p>
                <p><img src="${pageContext.request.contextPath}/images/sms.gif" alt="" width="18" height="16" />&nbsp;<a href="#">To SMS Alerts</a></p>
            </div>
            <div id="tabs-2" class="hiddentab">
                <p><img src="${pageContext.request.contextPath}/images/emailalert.gif" alt="" width="18" height="15" />&nbsp;<a href="#">By Email</a></p>
                <p><img src="${pageContext.request.contextPath}/images/sms.gif" alt="" width="18" height="16" />&nbsp;<a href="#">By SMS</a></p>
            </div>
            <div id="tabs-3" class="hiddentab">
                <p><img src="${pageContext.request.contextPath}/images/emailalert.gif" alt="" width="18" height="15" />&nbsp;<a href="#">Report Spam</a></p>
            </div>
        </div>
    </div>
    <div class="clear">&nbsp;</div>
</div>

