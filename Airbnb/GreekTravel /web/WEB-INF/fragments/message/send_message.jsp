<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div id="search">
    <div class="tab">
        <h2>Send a new message</h2>
    </div>
    <div class="container">
        <form action="${pageContext.request.contextPath}/user/message/send_message" method="POST">
            <table class="search_form" style="width:100%; border:none;">


                <tr>
                    <td width="10%" class="label">Recipient</td>
                    <td colspan="3">
                        <label>
                            <input type="text" name="recipient" class="text longfield" value="${recipient}" />
                        </label>

                        <c:forEach items="${errorMap.recipient}" var="obj">
                            <p class="fielderror">${obj}</p>
                        </c:forEach>
                    </td>
                </tr>


                <tr>
                    <td width="10%" class="label">Title</td>
                    <td colspan="3">
                        <label>
                            <input type="text" name="title" id="title" class="text longfield" value="${topic}"  />
                        </label>
                        <c:forEach items="${errorMap.title}" var="obj">
                            <p class="fielderror">${obj}</p>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <td width="10%" class="label">Message</td>
                    <td colspan ="3"> 
                        <textarea name="message" id="message" rows="20" class="text longfield"></textarea>
                        <c:forEach items="${errorMap.message}" var="obj">
                            <p class="fielderror">${obj}</p>
                        </c:forEach>
                    </td>
                </tr>
                <input type ="hidden" value="${roomId}" name="roomId" >

                <tr>
                    <td class="label">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2" class="label">
                        <label>
                            <input class="commitbutton" type="submit" alt="createmessage" name="button2" id="createmessage" value="                 Submit                    " />
                        </label>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>