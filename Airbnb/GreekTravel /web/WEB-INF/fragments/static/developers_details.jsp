<%@page contentType="text/html" pageEncoding="UTF-8"%>
<h1>About us</h1>

<ul class="vasilis_listing">
    <li>
        
        <div class="listinfo"> <img src="${pageContext.request.contextPath}/images/andreas.jpg" alt="" class="vasilis_listingimage " />
            <h2>Andreas Ntoules</h2>
            <h3>Live : Athens<br/>Works : Athens<br/>Tel : 6953443642 <br/> Birthday : 21/12/92<br/> Status : Front-end<br/> Email : ultras_7@di.uoa.gr<br/>Education : Department of Informatics and Telecommunications</h3>
            <div class="clear">&nbsp;</div>
        </div>

    </li>
    <li>
        <div class="listinfo"> <img src="${pageContext.request.contextPath}/images/vasilis.jpg" alt="" class="vasilis_listingimage " />
            <h2>Basilis Chronis</h2>
            <h3>Live : Athens<br/>Works : Athens<br/>Tel : 6953443232 <br/> Birthday : 10/09/1989<br/> Status : Back-end<br/> Email : mma_fighter@di.uoa.gr<br/>Education : Department of Informatics and Telecommunications</h3>

            <div class="clear">&nbsp;</div>
        </div>
    </li>
    <li>
        <div class="listinfo"> <img src="${pageContext.request.contextPath}/images/petros.jpg" alt="" class="vasilis_listingimage " />
            <h2>Petros Chatzigeorgiou</h2>
            <h3>Live : Athens<br/>Works : Athens<br/>Tel : 6953443342 <br/> Birthday : 17/10/1988<br/> Status : Supervisor<br/> Email : starcraft_2@di.uoa.gr<br/>Education : Department of Informatics and Telecommunications</h3>

            <div class="clear">&nbsp;</div>
        </div>
    </li>
</ul>

